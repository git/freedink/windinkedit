#include "Globals.h"
#include "windinkedit-dmod.h"
#include "windinkedit-map.h"
#include "windinkedit-sequence.h"
#include "windinkedit-sprite.h"


/*************/
/**  Class  **/
/**         **/
/*************/

static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditSprite* self = (WindinkeditSprite*)instance;
  self->x		= 0;
  self->y		= 0;
  self->sequence	= 0;
  self->frame		= 0;
  self->type		= 0;
  self->size		= 0;
  self->brain		= 0;
  self->speed		= 0;
  self->base_walk	= 0;
  self->base_idle	= 0;
  self->base_attack	= 0;
  self->timing		= 0;
  self->depth		= 0;
  self->hardness	= 0;
  self->trim_left	= 0;
  self->trim_top	= 0;
  self->trim_right	= 0;
  self->trim_bottom	= 0;
  self->warp_enabled	= 0;
  self->warp_screen	= 0;
  self->warp_x		= 0;
  self->warp_y		= 0;
  self->touch_sequence	= 0;
  self->base_death	= 0;
  self->hitpoints	= 0;
  self->defense		= 0;
  self->sound		= 0;
  self->vision		= 0;
  self->nohit		= 0;
  self->touch_damage	= 0;
  self->experience	= 0;
  memset(self->script, 0, MAX_SCRIPT_NAME_LENGTH);
}

GType windinkedit_sprite_get_type (void)
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditSpriteClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      NULL,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditSprite),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditSpriteType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/

WindinkeditSprite* windinkedit_sprite_new()
{
  WindinkeditSprite* sprite = WINDINKEDIT_SPRITE(g_object_new(WINDINKEDIT_TYPE_SPRITE, NULL));
  return sprite;
}

/**
 * Deserialization
 *
 * Creates a sprite from a sprite structure used to read the file from
 * the map file
 **/
WindinkeditSprite* windinkedit_sprite_new_from(struct DUMMY_SPRITE* sprite_data)
{
  WindinkeditSprite* copy_sprite = WINDINKEDIT_SPRITE(g_object_new(WINDINKEDIT_TYPE_SPRITE, NULL));

  // substract SIDEBAR_WIDTH ("screen lock" sidebar in the game)
  copy_sprite->x		= sprite_data->x - SIDEBAR_WIDTH;
  copy_sprite->y		= sprite_data->y;
  copy_sprite->sequence		= sprite_data->sequence;
  copy_sprite->frame		= sprite_data->frame;
  copy_sprite->type		= sprite_data->type;
  copy_sprite->size		= sprite_data->size;
  copy_sprite->brain		= sprite_data->brain;
  copy_sprite->speed		= sprite_data->speed;
  copy_sprite->base_walk	= sprite_data->base_walk;
  copy_sprite->base_idle	= sprite_data->base_idle;
  copy_sprite->base_attack	= sprite_data->base_attack;
  copy_sprite->timing		= sprite_data->timing;
  copy_sprite->depth		= sprite_data->depth;
  copy_sprite->hardness		= sprite_data->hardness;
  copy_sprite->trim_left	= sprite_data->trim_left;
  copy_sprite->trim_top		= sprite_data->trim_top;
  copy_sprite->trim_right	= sprite_data->trim_right;
  copy_sprite->trim_bottom	= sprite_data->trim_bottom;
  copy_sprite->warp_enabled	= sprite_data->warp_enabled;
  copy_sprite->warp_screen	= sprite_data->warp_screen;
  copy_sprite->warp_x		= sprite_data->warp_x;
  copy_sprite->warp_y		= sprite_data->warp_y;
  copy_sprite->touch_sequence	= sprite_data->touch_sequence;
  copy_sprite->base_death	= sprite_data->base_death;
  copy_sprite->hitpoints	= sprite_data->hitpoints;
  copy_sprite->defense		= sprite_data->defense;
  copy_sprite->sound		= sprite_data->sound;
  copy_sprite->vision		= sprite_data->vision;
  copy_sprite->nohit		= sprite_data->nohit;
  copy_sprite->touch_damage	= sprite_data->touch_damage;
  copy_sprite->experience	= sprite_data->experience;
  memcpy(copy_sprite->script, sprite_data->script, MAX_SCRIPT_NAME_LENGTH);

  return copy_sprite;
}

/**
 * Serialization
 */
void windinkedit_sprite_formatOutput(WindinkeditSprite* self, struct DUMMY_SPRITE* sprite_data)
{
  memset(&sprite_data, 0 , sizeof(DUMMY_SPRITE));

  // found a sprite, now fill in the dummy_struct
  // add SIDEBAR_WIDTH ("screen lock" sidebar in the game)
  sprite_data->x			= self->x + SIDEBAR_WIDTH;
  sprite_data->y			= self->y;
  sprite_data->sequence		= self->sequence;
  sprite_data->frame		= self->frame;
  sprite_data->type		= self->type;
  sprite_data->size		= self->size;
  sprite_data->brain		= self->brain;
  sprite_data->speed		= self->speed;
  sprite_data->base_walk		= self->base_walk;
  sprite_data->base_idle		= self->base_idle;
  sprite_data->base_attack	= self->base_attack;
  sprite_data->timing		= self->timing;
  sprite_data->depth		= self->depth;
  sprite_data->hardness		= self->hardness;
  sprite_data->trim_left		= self->trim_left;
  sprite_data->trim_top		= self->trim_top;
  sprite_data->trim_right	= self->trim_right;
  sprite_data->trim_bottom	= self->trim_bottom;
  sprite_data->warp_enabled	= self->warp_enabled;
  sprite_data->warp_screen	= self->warp_screen;
  sprite_data->warp_x		= self->warp_x;
  sprite_data->warp_y		= self->warp_y;
  sprite_data->touch_sequence	= self->touch_sequence;
  sprite_data->base_death	= self->base_death;
  sprite_data->hitpoints		= self->hitpoints;
  sprite_data->defense		= self->defense;
  sprite_data->sound		= self->sound;
  sprite_data->vision		= self->vision;
  sprite_data->nohit		= self->nohit;
  sprite_data->touch_damage	= self->touch_damage;
  sprite_data->experience	= self->experience;
  
  sprite_data->sprite_exist	= 1;	// sprite exists
  
  memcpy(sprite_data->script, self->script, MAX_SCRIPT_NAME_LENGTH);
}

/**
 * Copy constructor
 */
WindinkeditSprite* windinkedit_sprite_copy(WindinkeditSprite* self)
{
  WindinkeditSprite* copy_sprite = WINDINKEDIT_SPRITE(g_object_new(WINDINKEDIT_TYPE_SPRITE, NULL));
  copy_sprite->x		= self->x;
  copy_sprite->y		= self->y;
  copy_sprite->sequence		= self->sequence;
  copy_sprite->frame		= self->frame;
  copy_sprite->type		= self->type;
  copy_sprite->size		= self->size;
  copy_sprite->brain		= self->brain;
  copy_sprite->speed		= self->speed;
  copy_sprite->base_walk	= self->base_walk;
  copy_sprite->base_idle	= self->base_idle;
  copy_sprite->base_attack	= self->base_attack;
  copy_sprite->timing		= self->timing;
  copy_sprite->depth		= self->depth;
  copy_sprite->hardness		= self->hardness;
  copy_sprite->trim_left	= self->trim_left;
  copy_sprite->trim_top		= self->trim_top;
  copy_sprite->trim_right	= self->trim_right;
  copy_sprite->trim_bottom	= self->trim_bottom;
  copy_sprite->warp_enabled	= self->warp_enabled;
  copy_sprite->warp_screen	= self->warp_screen;
  copy_sprite->warp_x		= self->warp_x;
  copy_sprite->warp_y		= self->warp_y;
  copy_sprite->touch_sequence	= self->touch_sequence;
  copy_sprite->base_death	= self->base_death;
  copy_sprite->hitpoints	= self->hitpoints;
  copy_sprite->defense		= self->defense;
  copy_sprite->sound		= self->sound;
  copy_sprite->vision		= self->vision;
  copy_sprite->nohit		= self->nohit;
  copy_sprite->touch_damage	= self->touch_damage;
  copy_sprite->experience	= self->experience;
  memcpy(copy_sprite->script, self->script, MAX_SCRIPT_NAME_LENGTH);

  return copy_sprite;
}

int windinkedit_sprite_getImageBounds(WindinkeditSprite* self, SDL_Rect* bounds)
{
  WindinkeditSequence* cur_seq = windinkedit_dmod_get_sequence(current_dmod, self->sequence);
	int cur_frame = self->frame - 1;

	// make sure the sequence is valid
	if (cur_seq == NULL)
		return /*FALSE*/0;

	// fill in the destination rect
	if (windinkedit_sequence_getBounds(cur_seq, cur_frame, self->size, bounds) == 0/*false*/)
		return /*FALSE*/0;

	bounds->x += self->x;
	bounds->y += self->y;

	return /*TRUE*/1;
}

void windinkedit_sprite_setVision(WindinkeditSprite* self, int new_vision)
{
  self->vision = new_vision;
}

int windinkedit_sprite_getVision(WindinkeditSprite* self)
{
  return self->vision;
}
