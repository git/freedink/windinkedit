/**
 * Graphics sequence / animation - defined in a dink.ini line

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef WINDINKEDIT_SEQUENCE_H
#define WINDINKEDIT_SEQUENCE_H

#include <glib-object.h>
#include "Globals.h"
struct SDL_Surface;

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_SEQUENCE         (windinkedit_sequence_get_type ())
#define WINDINKEDIT_SEQUENCE(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_SEQUENCE, WindinkeditSequence))
#define WINDINKEDIT_SEQUENCE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_SEQUENCE, WindinkeditSequenceClass))
#define WINDINKEDIT_IS_SEQUENCE(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_SEQUENCE))
#define WINDINKEDIT_IS_SEQUENCE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_SEQUENCE))
#define WINDINKEDIT_SEQUENCE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_SEQUENCE, WindinkeditSequenceClass))

#ifndef _WINDINKEDIT_TYPEDEF_SEQUENCE
#define _WINDINKEDIT_TYPEDEF_SEQUENCE
typedef struct _WindinkeditSequence WindinkeditSequence;
#endif
typedef struct _WindinkeditSequenceClass WindinkeditSequenceClass;
typedef struct _WindinkeditSequencePrivate WindinkeditSequencePrivate;

/* Avoid including full .h headers just for pointers */
#ifndef _WINDINKEDIT_TYPEDEF_SPRITE
#define _WINDINKEDIT_TYPEDEF_SPRITE
typedef struct _WindinkeditSprite WindinkeditSprite;
#endif


#define MAX_SEQUENCES		1000
#define MAX_FRAMES			50
#define MAX_FRAME_INFOS		500

struct FRAME_INFO
{
  int frame_delay;
  int center_x;
  int center_y;
  int left_boundary;
  int top_boundary;
  int right_boundary;
  int bottom_boundary;
};
typedef struct FRAME_INFO FRAME_INFO;

struct _WindinkeditSequence {
  GObject parent;
  
  // for cases where the frame info is different
  FRAME_INFO* frame_info[MAX_FRAMES];
  
  int set_frame_delay[MAX_FRAMES];
  int set_frame_special[MAX_FRAMES];
  int set_frame_frame_seq[MAX_FRAMES];
  int set_frame_frame_frame[MAX_FRAMES];
  
  char graphics_path[PATH_MAX];
  
  // stuff that can be different for every frame
  short frame_delay;
  int center_x;
  int center_y;
  int left_boundary;
  int top_boundary;
  int right_boundary;
  int bottom_boundary;
  int size;
  int type;
  int frame_width[MAX_FRAMES];
  int frame_height[MAX_FRAMES];
  int/*bool*/ now;
  
  // same for every frame
  SDL_Surface* frame_image[MAX_FRAMES];
  
  /*< private >*/
  WindinkeditSequencePrivate* _priv;
};

struct _WindinkeditSequenceClass {
  GObjectClass parent;
};

/* used by WINDINKEDIT_TYPE_SEQUENCE */
GType windinkedit_sequence_get_type (void);

/* API. */
WindinkeditSequence*
windinkedit_sequence_new(WindinkeditDmod* context,
			 int sequence_num, char* graphics_path, int frame_delay, int type, int center_x, int center_y, 
			 int left_boundary, int top_boundary, int right_boundary, int bottom_boundary, int/*bool*/ now);

int/*bool*/ windinkedit_sequence_addFrameInfo(WindinkeditSequence* self, int frame_num, int center_x, int center_y, int left, int top, int right, int bottom);
FILE* windinkedit_sequence_extractBitmap(WindinkeditSequence* self, char* bitmap_file);
int/*bool*/ windinkedit_sequence_loadFrame(WindinkeditSequence* self, int cur_frame);
int/*bool*/ windinkedit_sequence_loadFrameFromFile(WindinkeditSequence* self, int cur_frame, char* bmp_file);

int/*bool*/ windinkedit_sequence_getBounds(WindinkeditSequence* self, int frame, int size, SDL_Rect* rect);
int/*bool*/ windinkedit_sequence_getCenter(WindinkeditSequence* self, int frame, int size, int* center_x, int* center_y);

int windinkedit_sequence_render(WindinkeditSequence* self, int x, int y, int cur_frame);
int windinkedit_sequence_clipRender(WindinkeditSequence* self, WindinkeditSprite* cur_sprite, int sprite_num, int/*bool*/ draw_info, SDL_Surface *surface, int offset_x, int offset_y);
int windinkedit_sequence_drawSpriteInfo(WindinkeditSequence* self, int x_offset, int y_offset, WindinkeditSprite* cur_sprite, int sprite_num);
int windinkedit_sequence_clipRenderHardness(WindinkeditSequence* self, int x_offset, int y_offset, WindinkeditSprite* cur_sprite, struct rect* clip_rect);

G_END_DECLS

#endif
