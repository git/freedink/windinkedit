/**
 * Graphics memory management

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef WINDINKEDIT_GRAPHICS_MANAGER_H
#define WINDINKEDIT_GRAPHICS_MANAGER_H

#include <glib-object.h>
#include "SDL.h"

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_GRAPHICS_MANAGER         (windinkedit_graphics_manager_get_type ())
#define WINDINKEDIT_GRAPHICS_MANAGER(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_GRAPHICS_MANAGER, WindinkeditGraphicsManager))
#define WINDINKEDIT_GRAPHICS_MANAGER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_GRAPHICS_MANAGER, WindinkeditGraphicsManagerClass))
#define WINDINKEDIT_IS_GRAPHICS_MANAGER(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_GRAPHICS_MANAGER))
#define WINDINKEDIT_IS_GRAPHICS_MANAGER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_GRAPHICS_MANAGER))
#define WINDINKEDIT_GRAPHICS_MANAGER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_GRAPHICS_MANAGER, WindinkeditGraphicsManagerClass))

#ifndef _WINDINKEDIT_TYPEDEF_GRAPHICS_MANAGER
#define _WINDINKEDIT_TYPEDEF_GRAPHICS_MANAGER
typedef struct _WindinkeditGraphicsManager WindinkeditGraphicsManager;
#endif
typedef struct _WindinkeditGraphicsManagerClass WindinkeditGraphicsManagerClass;
typedef struct _WindinkeditGraphicsManagerPrivate WindinkeditGraphicsManagerPrivate;

typedef struct GRAPHICQUE GRAPHICQUE;
struct GRAPHICQUE
{
  struct GRAPHICQUE* next;
  struct GRAPHICQUE* prev;
  SDL_Surface** surface;
};
  
struct _WindinkeditGraphicsManager {
  GObject parent;
  /*< private >*/
  WindinkeditGraphicsManagerPrivate* _priv;
};

struct _WindinkeditGraphicsManagerClass {
  GObjectClass parent;
};

/* used by WINDINKEDIT_TYPE_GRAPHICS_MANAGER */
GType windinkedit_graphics_manager_get_type(void);

/* API. */
GRAPHICQUE* windinkedit_graphics_manager_addGraphic(WindinkeditGraphicsManager* self, SDL_Surface** surface);
int windinkedit_graphics_manager_moveLocation(WindinkeditGraphicsManager* self, GRAPHICQUE* location);
int windinkedit_graphics_manager_removeAllVideoMemGraphics(WindinkeditGraphicsManager* self);
int windinkedit_graphics_manager_removeGraphics(WindinkeditGraphicsManager* self, int amount);
SDL_Surface *windinkedit_graphics_manager_allocateSurface(WindinkeditGraphicsManager* self, int width, int height);
int windinkedit_graphics_manager_checkMemory(WindinkeditGraphicsManager* self);
void windinkedit_graphics_manager_empty(WindinkeditGraphicsManager* self);
  
G_END_DECLS

#endif
