#ifndef _WINDINKEDIT_SPRITE_H_
#define _WINDINKEDIT_SPRITE_H_

#include <glib-object.h>
struct SDL_Rect;

G_BEGIN_DECLS


#define WINDINKEDIT_TYPE_SPRITE         (windinkedit_sprite_get_type ())
#define WINDINKEDIT_SPRITE(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_SPRITE, WindinkeditSprite))
#define WINDINKEDIT_SPRITE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_SPRITE, WindinkeditSpriteClass))
#define WINDINKEDIT_IS_SPRITE(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_SPRITE))
#define WINDINKEDIT_IS_SPRITE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_SPRITE))
#define WINDINKEDIT_SPRITE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_SPRITE, WindinkeditSpriteClass))

#ifndef _WINDINKEDIT_TYPEDEF_SPRITE
#define _WINDINKEDIT_TYPEDEF_SPRITE
typedef struct _WindinkeditSprite WindinkeditSprite;
#endif
typedef struct _WindinkeditSpriteClass WindinkeditSpriteClass;
typedef struct _WindinkeditSpritePrivate WindinkeditSpritePrivate;


#define MAX_SCRIPT_NAME_LENGTH	13

// needs to be of size 220
struct DUMMY_SPRITE
{
  int x;
  int y;
  int sequence;
  int frame;
  int type;
  int size;
  char/*bool*/ sprite_exist; /* keep this 1 byte size, used in serialization */
  int rotation;	// doesn't work
  int special;
  int brain;
  char script[MAX_SCRIPT_NAME_LENGTH];
  char hit[MAX_SCRIPT_NAME_LENGTH];
  char die[MAX_SCRIPT_NAME_LENGTH];
  char talk[MAX_SCRIPT_NAME_LENGTH];
  int speed;
  int base_walk;
  int base_idle;
  int base_attack;
  int base_hit;	// might work
  int timing;
  int depth;
  int hardness;
  int trim_left;
  int trim_top;
  int trim_right;
  int trim_bottom;
  int warp_enabled;
  int warp_screen;
  int warp_x;
  int warp_y;
  int touch_sequence;
  int base_death;
  int gold_dropped;	// doesn't work
  int hitpoints;
  int strength;	// doesn't work
  int defense;
  int experience;
  int sound;
  int vision;
  int nohit;
  int touch_damage;
  
  int garbage[5];	// unknown value
};

struct _WindinkeditSprite {
  GObject parent;
  
  int x; // DinkEdit-compatible x,y coordinates (screen starts
  // at (20,0) because of the Dink game sidebar
  int y;
  int sequence;
  int frame;
  short type;
  short size;
  int special;
  short brain;
  char script[MAX_SCRIPT_NAME_LENGTH];
  short speed;
  short base_walk;
  short base_idle;
  short base_attack;
  // int base_hit;
  short timing;
  int depth;
  short hardness;
  short trim_left;
  short trim_top;
  short trim_right;
  short trim_bottom;
  short warp_enabled;
  short warp_screen;
  short warp_x;
  short warp_y;
  short touch_sequence;
  short base_death;
  short hitpoints;
  short defense;
  short experience;
  short sound;
  short nohit;
  short touch_damage;
  /* private: */
  short vision;
};

struct _WindinkeditSpriteClass {
  GObjectClass parent;
};


/* used by WINDINKEDIT_TYPE_SPRITE */
GType windinkedit_sprite_get_type(void);

WindinkeditSprite* windinkedit_sprite_new();
WindinkeditSprite* windinkedit_sprite_new_from(struct DUMMY_SPRITE* sprite_data);
WindinkeditSprite* windinkedit_sprite_copy(WindinkeditSprite* self);
void windinkedit_sprite_formatOutput(WindinkeditSprite* self, struct DUMMY_SPRITE* sprite_data);
int windinkedit_sprite_getImageBounds(WindinkeditSprite* self, SDL_Rect* bounds);
void windinkedit_sprite_setVision(WindinkeditSprite* self, int new_vision);
int windinkedit_sprite_getVision(WindinkeditSprite* self);

G_END_DECLS

#endif
