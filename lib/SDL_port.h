#ifndef _SDL_PORT_H
#define _SDL_PORT_H

struct SDL_Rect;
struct SDL_Surface;
struct rect;

#ifdef __cplusplus
extern "C"
{
#endif
  
  extern void CopyRect2(SDL_Rect* dest, struct rect* src);
  extern void Blt(SDL_Surface* dest, struct rect* dest_rect,
		  SDL_Surface* src, struct rect* src_rect);
  extern void BltStretch(SDL_Surface* dest, struct rect* dest_rect,
			 SDL_Surface* src, struct rect* src_rect);
  extern void fill_rect(SDL_Surface* surface, struct rect* box, int color);
  extern SDL_Surface* SubSurface(SDL_Surface* surface, SDL_Rect* subrect);
  
#ifdef __cplusplus
}
#endif

#endif
