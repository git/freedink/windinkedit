#ifndef _WINDINKEDIT_TILE_H
#define _WINDINKEDIT_TILE_H

#include <glib-object.h>

#include "Globals.h"
struct SDL_Surface;
struct rect;

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_TILE         (windinkedit_tile_get_type ())
#define WINDINKEDIT_TILE(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_TILE, WindinkeditTile))
#define WINDINKEDIT_TILE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_TILE, WindinkeditTileClass))
#define WINDINKEDIT_IS_TILE(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_TILE))
#define WINDINKEDIT_IS_TILE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_TILE))
#define WINDINKEDIT_TILE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_TILE, WindinkeditTileClass))

#ifndef _WINDINKEDIT_TYPEDEF_TILE
#define _WINDINKEDIT_TYPEDEF_TILE
typedef struct _WindinkeditTile WindinkeditTile;
#endif
typedef struct _WindinkeditTileClass WindinkeditTileClass;
typedef struct _WindinkeditTilePrivate WindinkeditTilePrivate;

struct GRAPHICQUE;

#define MAX_TILE_BMPS		41
#define SCREEN_TILE_WIDTH	12
#define SCREEN_TILE_HEIGHT	8
#define TILEWIDTH			50
#define TILEHEIGHT			50

struct _WindinkeditTile {
  GObject parent;

  // stores the tile hardness id that is associated with each square
  int tile_hardness[SCREEN_TILE_HEIGHT][SCREEN_TILE_WIDTH];
    
  /*< private >*/
  WindinkeditTilePrivate* _priv;
};

struct _WindinkeditTileClass {
  GObjectClass parent;
};


/* used by WINDINKEDIT_TYPE_TILE */
GType windinkedit_tile_get_type(void);

WindinkeditTile* windinkedit_tile_new(int file_location);
int windinkedit_tile_loadSurface(WindinkeditTile* self);
int windinkedit_tile_render(WindinkeditTile* self, int tileX, int tileY, SDL_Surface *surface, SDL_Rect* dest_rect);
int windinkedit_tile_renderAll(WindinkeditTile* self, int x, int y, int width, int height);

G_END_DECLS

#endif
