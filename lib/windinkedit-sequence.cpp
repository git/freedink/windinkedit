/**
 * Graphics sequence / animation - defined in a dink.ini line

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "windinkedit-sequence.h"

#include <glib/gprintf.h>
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_port.h"
#include "rect.h"

#include "Colors.h"
#include "Common.h"
#include "gfx_fonts.h"
#include "windinkedit-dmod.h"
#include "windinkedit-gc.h"
#include "windinkedit-graphics-manager.h"
#include "windinkedit-map.h"
#include "windinkedit-sprite.h"

G_BEGIN_DECLS

#define UNKNOWN_GRAPHIC		0
#define DMOD_GRAPHIC		1
#define DINK_GRAPHIC		2
#define GRAPHIC_NOT_FOUND	3

#define SPRITE_TYPE_INVISIBLE		2


/*************/
/**  Class  **/
/**         **/
/*************/

static GObjectClass *parent_class = NULL;

enum {
  WINDINKEDIT_SEQUENCE_CONSTRUCT_DMOD = 1,
};


/** Private fields **/

#define selfp (self->_priv)

struct _WindinkeditSequencePrivate {
  WindinkeditDmod* dmod;
  int sequence_num;
  GRAPHICQUE* graphic_que_location[MAX_FRAMES];	// so we don't run out of memory
  Uint32 color_key; // used to set color key
  int graphic_location;
};



static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditSequence* self = (WindinkeditSequence*)instance;
  self->_priv = G_TYPE_INSTANCE_GET_PRIVATE(self, WINDINKEDIT_TYPE_SEQUENCE, WindinkeditSequencePrivate);
}

/**
 * Run after properties are set
 */
static void
___constructed(GObject* object)
{
  WindinkeditSequence* self = WINDINKEDIT_SEQUENCE(object);
  if (parent_class->constructed)
    parent_class->constructed(object);
}



static void
___get_property(GObject      *object,
		guint         property_id,
		GValue       *value,
		GParamSpec   *pspec)
{
  WindinkeditSequence *self = (WindinkeditSequence *) object;
  
  switch (property_id) {
  case WINDINKEDIT_SEQUENCE_CONSTRUCT_DMOD:
    g_value_set_object(value, selfp->dmod);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

static void
___set_property (GObject      *object,
		 guint         property_id,
		 const GValue *value,
		 GParamSpec   *pspec)
{
  WindinkeditSequence *self = (WindinkeditSequence *) object;
  
  switch (property_id) {
  case WINDINKEDIT_SEQUENCE_CONSTRUCT_DMOD:
    g_return_if_fail(selfp->dmod == NULL);
    selfp->dmod = WINDINKEDIT_DMOD(g_value_get_object(value));
    /* dmod is passed by reference */
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

/**
 * Clean-up object (after dispose)
 */
static void
___finalize(GObject *obj)
{
  WindinkeditSequence *self = (WindinkeditSequence *)obj;

  // free the memory for all of the sequence's frames
  for (int i = 0; i < MAX_FRAMES; i++)
    {
      if (self->frame_image[i])
	{
	  SDL_FreeSurface(self->frame_image[i]);
	  self->frame_image[i] = NULL;
	}
      
      if (self->frame_info[i])
	{
	  delete self->frame_info[i];
	  self->frame_info[i] = NULL;
	}
    }

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->finalize(obj);
}


static void
___class_init(gpointer g_class,
	      gpointer g_class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
  WindinkeditSequenceClass *klass = WINDINKEDIT_SEQUENCE_CLASS (g_class);
  GParamSpec *pspec;

  gobject_class->constructed = ___constructed;
  gobject_class->set_property = ___set_property;
  gobject_class->get_property = ___get_property;
  gobject_class->finalize = ___finalize;

  g_type_class_add_private(g_class, sizeof(WindinkeditSequencePrivate));
  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));

  pspec = g_param_spec_object("dmod",
			      "D-Mod that contains this sequence",
			      "Allow to get D-Mod properties such as its path",
			      WINDINKEDIT_TYPE_DMOD /* type */,
			      (GParamFlags)(G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));
  g_object_class_install_property(gobject_class,
				  WINDINKEDIT_SEQUENCE_CONSTRUCT_DMOD,
				  pspec);
}

GType windinkedit_sequence_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditSequenceClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      (GClassInitFunc) ___class_init,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditSequence),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditSequenceType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/

WindinkeditSequence*
windinkedit_sequence_new(WindinkeditDmod* context, int sequence_num, char* graphics_path,
			 int frame_delay, int type, int center_x, int center_y, 
			 int left_boundary, int top_boundary, int right_boundary, int bottom_boundary, int/*bool*/ now)
{
  WindinkeditSequence *self = WINDINKEDIT_SEQUENCE(g_object_new(WINDINKEDIT_TYPE_SEQUENCE, "dmod", context, NULL));
  
  strcpy(self->graphics_path, graphics_path);
  self->frame_delay = frame_delay;
  self->center_x = center_x;
  self->center_y = center_y;
  self->left_boundary = left_boundary;
  self->top_boundary = top_boundary;
  self->right_boundary = right_boundary;
  self->bottom_boundary = bottom_boundary;
  selfp->sequence_num = sequence_num - 1;
  self->type = type;
  self->now = now;
  
  // set color key
  int color;
  if (type == BLACK)
    color = color_black;
  else
    color = color_white;
  
  memset(self->set_frame_delay, 0, sizeof(self->set_frame_delay));
  memset(self->set_frame_special, 0, sizeof(self->set_frame_special));
  memset(self->set_frame_frame_seq, 0, sizeof(self->set_frame_frame_seq));
  memset(self->set_frame_frame_frame, 0, sizeof(self->set_frame_frame_frame));
  
  selfp->color_key = color;
  selfp->color_key = color;
  
  selfp->graphic_location = UNKNOWN_GRAPHIC;
  
  memset(self->frame_image, 0, MAX_FRAMES * sizeof(int));
  memset(self->frame_info,  0, MAX_FRAMES * sizeof(int));

  return self;
}

int/*bool*/ windinkedit_sequence_addFrameInfo(WindinkeditSequence* self, int frame_num, int center_x, int center_y, int left, int top, int right, int bottom)
{
	if (frame_num < 0 || frame_num >= MAX_FRAMES)
		return 0/*false*/;

	if (self->frame_info[frame_num] == NULL)
	{
		// create a new frame info
		self->frame_info[frame_num] = new FRAME_INFO;
	}
	FRAME_INFO* new_frame_info = self->frame_info[frame_num];
	
	// fill out the info for a special frame
	
	new_frame_info->center_x = center_x;
	new_frame_info->center_y = center_y;
	new_frame_info->left_boundary = left;
	new_frame_info->top_boundary = top;
	new_frame_info->right_boundary = right;
	new_frame_info->bottom_boundary = bottom;

	return 1/*true*/;
}

// returns the location in the .ff file of the bitmap
FILE* windinkedit_sequence_extractBitmap(WindinkeditSequence* self, char* bitmap_file)
{
	FILE* fastfile;
	char filename[13];
	int num_files;
	int file_begin_location;
	// UNUSED:	int file_end_location = 0;
	int i;

	char* outFile = bitmap_file;
	char ffFile[100] = "dir.ff";

	// clip the filename off then end and append dir.ff to the path
	int string_length = strlen(bitmap_file);
	for (i = string_length - 1; i >= 0; i--)
	{ 
		if (bitmap_file[i] == '/')
		{
			outFile = bitmap_file + i + 1;
			strncpy(ffFile, bitmap_file, i);
			ffFile[i] = 0;
			strcat(ffFile, "/dir.ff");
			break;
		}
	}

	// open the fastfile
	if ((fastfile = fopen(ffFile, "rb")) == NULL)
		return NULL;

	// get the number of .bmp files
	if (fread(&num_files, sizeof(int), 1, fastfile) <= 0)
	{
		fclose(fastfile);
		return NULL;
	}

	for (i = 0; i < num_files; i++)
	{
		// get the file location in the fastfile
		if (fread(&file_begin_location, sizeof(int), 1, fastfile) <= 0)
		{
			fclose(fastfile);
			return NULL;
		}

		// get the filename
		if (fread(filename, sizeof(filename), 1, fastfile) <= 0)
		{
			fclose(fastfile);
			return NULL;
		}

		// check if filename matches (case insensitive)
		if (strcasecmp(filename, outFile) == 0)
		{
			break;
		}
	}

	// return if nothing was found
	if (i == num_files)
	{
		fclose(fastfile);
		return NULL;
	}

	// seek to the location of the .bmp file
	fseek(fastfile, file_begin_location, SEEK_SET);

	return fastfile;
}

int/*bool*/ windinkedit_sequence_getBounds(WindinkeditSequence* self, int frame, int size, SDL_Rect* rect)
{
  if (frame < 0 || frame >= MAX_FRAMES)
    return 0/*false*/;
  
  // make sure the frame is valid and has been loaded
  if (self->frame_image[frame] == NULL)
    {
      if (windinkedit_sequence_loadFrame(self, frame) == 0/*false*/)
	return 0/*false*/;	
      return 0/*false*/;
    }
  
  /* Sadly the placement of resized sprites doesn't make sense in the
     original game engine. It's reproduced here for compatibility */
  double size_ratio = size / 100.0;
  int x_compat = self->frame_width [frame] * (size_ratio - 1) / 2;
  int y_compat = self->frame_height[frame] * (size_ratio - 1) / 2;

  int center_x = -1, center_y = -1;
  windinkedit_sequence_getCenter(self, frame, size, &center_x, &center_y);
  rect->x = - center_x - x_compat;
  rect->y = - center_y - y_compat;

  rect->w = self->frame_width [frame] * size_ratio;
  rect->h = self->frame_height[frame] * size_ratio;

  return 1/*true*/;
}

/**
 * Get the depth center. 'size' isn't currently used, yet maybe it
 * should be taken into account for the computation - that is, if a
 * sprite is 200% of the original size, should we return the pixel
 * coordinates of the center of the sprite in its original size or in
 * its resized size?
 */
int/*bool*/ windinkedit_sequence_getCenter(WindinkeditSequence* self, int frame, int size, int* x, int* y)
{
  if (frame < 0 || frame >= MAX_FRAMES)
    return 0/*false*/;
  
  // make sure the frame is valid and has been loaded
  if (self->frame_image[frame] == NULL)
    return 0/*false*/;
  
  if (self->frame_info[frame] == NULL)
    {
      if (self->center_x != 0 && self->center_y != 0)
	{
	  *x = self->center_x;
	  *y = self->center_y;
	}
      // check if hardbox default should be used
      else if (self->type == NOTANIM)
	{
	  *x = (self->frame_width [frame] - self->frame_width [frame] / 2) + self->frame_width [frame] / 6;
	  *y = (self->frame_height[frame] - self->frame_height[frame] / 4) - self->frame_height[frame] / 30;
	}
      else
	{
	  // use the first frame's information for computing the center
	  if (self->frame_image[0] == NULL)
	    {
	      if (windinkedit_sequence_loadFrame(self, 0) == 0/*false*/)
		return 0/*false*/;
	    }
	  
	  // permanently change the centers for the sequence
	  *x = self->center_x = (self->frame_width [0] - self->frame_width [0] / 2) + self->frame_width [0] / 6;
	  *y = self->center_y = (self->frame_height[0] - self->frame_height[0] / 4) - self->frame_height[0] / 30;
	}
    }
  else
    {
      *x = self->frame_info[frame]->center_x;
      *y = self->frame_info[frame]->center_y;
    }
  return 1/*true*/;
}

int/*bool*/ windinkedit_sequence_loadFrameFromFile(WindinkeditSequence* self, int cur_frame, char* bmp_file)
{
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return 0/*false*/;

	FILE* stream;

	// backslash to slash (works on more OSes)
	char *pc = bmp_file;
	while (*pc != '\0')
	  {
	    if (*pc == '\\')
	      *pc = '/';
	    pc++;
	  }
	  

	if ((stream = fopen(bmp_file, "rb")) != NULL)
	{
		self->frame_image[cur_frame] = IMG_Load(bmp_file);
		fclose(stream);
	}
	// direct load failed, try extracting the file from the "dir.ff"
	else if ( (stream = windinkedit_sequence_extractBitmap(self, bmp_file)) != NULL)
	{
	  self->frame_image[cur_frame] = IMG_Load_RW(SDL_RWFromFP(stream, 0), 1);
		fclose(stream);
	}

	if (self->frame_image[cur_frame] == NULL)
		return 0/*false*/;

	// Convert to display for efficiency and also get the same
	// color references everywhere */
	SDL_Surface *old = self->frame_image[cur_frame];
	self->frame_image[cur_frame] = SDL_DisplayFormat(self->frame_image[cur_frame]);
	SDL_FreeSurface(old);
	// TODO: get rid of frame_width/frame_height arrays
	self->frame_width[cur_frame] = self->frame_image[cur_frame]->w;
	self->frame_height[cur_frame] = self->frame_image[cur_frame]->h;

	// now set the color key for source blitting
	SDL_SetColorKey(self->frame_image[cur_frame], SDL_SRCCOLORKEY, selfp->color_key);

	// add the frame to the graphics manager
	selfp->graphic_que_location[cur_frame] = windinkedit_graphics_manager_addGraphic(graphics_manager, &self->frame_image[cur_frame]);

	return 1/*true*/;
}

int/*bool*/ windinkedit_sequence_loadFrame(WindinkeditSequence* self, int cur_frame)
{
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return 0/*false*/;

	char bmp_file[PATH_MAX];

	if (selfp->graphic_location == GRAPHIC_NOT_FOUND)
	{
		// graphic cannot be loaded
		return 0/*false*/;
	}
	else if (selfp->graphic_location == UNKNOWN_GRAPHIC)
	{
		// try the dmod directory first
		sprintf(bmp_file, "%s%s%.2d.bmp", windinkedit_dmod_get_dmoddir(selfp->dmod),
			self->graphics_path, 1);
		if (windinkedit_sequence_loadFrameFromFile(self, 0, bmp_file) == 1/*true*/)
		{
			selfp->graphic_location = DMOD_GRAPHIC;
		}
		else
		{
		  g_sprintf(bmp_file, "%s%s%.2d.bmp", windinkedit_dmod_get_refdir(selfp->dmod),
			    self->graphics_path, 1);
			if (windinkedit_sequence_loadFrameFromFile(self, 0, bmp_file) == 1/*true*/)
			{
				selfp->graphic_location = DINK_GRAPHIC;
			}
			else
			{
				selfp->graphic_location = GRAPHIC_NOT_FOUND;
				return 0/*false*/;
			}
		}
	}

	if (selfp->graphic_location == DMOD_GRAPHIC)
	{
		sprintf(bmp_file, "%s%s%.2d.bmp",
			windinkedit_dmod_get_dmoddir(selfp->dmod),
			self->graphics_path, cur_frame + 1);
		return windinkedit_sequence_loadFrameFromFile(self, cur_frame, bmp_file);
	}
	else if (selfp->graphic_location == DINK_GRAPHIC)
	{
	  sprintf(bmp_file, "%s%s%.2d.bmp", windinkedit_dmod_get_refdir(selfp->dmod), self->graphics_path, cur_frame + 1);
		return windinkedit_sequence_loadFrameFromFile(self, cur_frame, bmp_file);
	}

	return 1/*true*/;
}

// used for drawing sprites in the sprite selector
int windinkedit_sequence_render(WindinkeditSequence* self, int x, int y, int cur_frame)
{
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return 0/*false*/;

	if (self->frame_image[cur_frame] == NULL)
	  if (windinkedit_sequence_loadFrame(self, cur_frame) == 0/*false*/)
			return 0/*false*/;

	struct rect dest_rect;   // the destination rectangle                          
	// fill in the destination rect

	dest_rect.left   = x;
	dest_rect.top    = y;
	dest_rect.right  = x + SPRITE_SELECTOR_BMP_WIDTH;
	dest_rect.bottom = y + SPRITE_SELECTOR_BMP_HEIGHT;

	Blt(lpddsback, &dest_rect, self->frame_image[cur_frame], NULL);

	// notify the graphics manager that the graphic was just used
	windinkedit_graphics_manager_moveLocation(graphics_manager, selfp->graphic_que_location[cur_frame]);

	return 1/*true*/;
}

// clips sprites to screens, offsets are screen locations
int windinkedit_sequence_clipRender(WindinkeditSequence* self, WindinkeditSprite* cur_sprite, int sprite_num, int/*bool*/ draw_info, SDL_Surface* surface, int offset_x, int offset_y)
{
  SDL_Rect clip_rect;
  SDL_GetClipRect(surface, &clip_rect);

  int cur_frame = cur_sprite->frame - 1;
  if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
    return 0/*false*/;

  self->size = cur_sprite->size;
  int x = cur_sprite->x;
  int y = cur_sprite->y;
  
  if (self->frame_image[cur_frame] == NULL)
    {
      if (windinkedit_sequence_loadFrame(self, cur_frame) == 0/*false*/)
	return 0/*false*/;		
    }

  SDL_Rect dstrect;

  if (windinkedit_sequence_getBounds(self, cur_frame, cur_sprite->size, &dstrect) == 0/*false*/)
    return 0/*false*/;

  dstrect.x += x;
  dstrect.y += y;

  double size_ratio = double(cur_sprite->size) / 100.0;
  SDL_Rect srcrect;

  // to trim or not to trim
  if (cur_sprite->trim_right == 0)
    {
      // fill in the source rect
      srcrect.x = 0;
      srcrect.y = 0;
      srcrect.w = self->frame_width [cur_frame];
      srcrect.h = self->frame_height[cur_frame];
    }
  else
    {
      // Trim the sprite.
      srcrect.x = cur_sprite->trim_left;
      srcrect.y = cur_sprite->trim_top;
      srcrect.w = cur_sprite->trim_right  - cur_sprite->trim_left;
      srcrect.h = cur_sprite->trim_bottom - cur_sprite->trim_top;

      // This doesn't work so well for resized sprites, but that the
      // way the original engine works. Ever heard of bug-for-bug
      // reimplementation? ;)
      dstrect.x += cur_sprite->trim_left;
      dstrect.y += cur_sprite->trim_top;
      dstrect.w -= self->frame_width [cur_frame] - cur_sprite->trim_right;
      dstrect.h -= self->frame_height[cur_frame] - cur_sprite->trim_bottom;
    }


  /* Clipping is done by SDL */


  dstrect.x += offset_x;
  dstrect.y += offset_y;

  // blt to destination surface
  if (cur_sprite->type == SPRITE_TYPE_INVISIBLE)
    {
      // draw sprite interlaced
      double ratio = 2.0 * double(srcrect.h) / double(dstrect.h);
      double src_row = 0.0;
      
      SDL_Rect srcrect2, dstrect2;
      srcrect2.x = srcrect.x;
      srcrect2.w = srcrect.w;
      for (int dest_row = 0; dest_row < dstrect.h; dest_row += 2, src_row += ratio)
	{
	  /* SDL_BlitSurface(3): "The final blit rectangle is saved in
	     dstrect after all clipping is performed (srcrect is not
	     modified)." => we need to rewrite it after each blit. */
	  dstrect2.x = dstrect.x;
	  dstrect2.w = dstrect.w;

	  srcrect2.y = int(src_row) + srcrect.y;
	  srcrect2.h = 1;
	  dstrect2.y = dstrect.y + dest_row;
	  dstrect2.h = 1;
	  
	  SDL_BlitSurface(self->frame_image[cur_frame], &srcrect2, surface, &dstrect2);
	}
    }
  else
    {
      // draw solid sprite
      SDL_BlitSurface(self->frame_image[cur_frame], &srcrect, surface, &dstrect);
    }
  
  // notify the graphics manager that the graphic was just used
  windinkedit_graphics_manager_moveLocation(graphics_manager, selfp->graphic_que_location[cur_frame]);
  
  //should I draw it now?
  if (draw_info)
    windinkedit_sequence_drawSpriteInfo(self, 0,// x_offset
					0, // y_offset
					cur_sprite, sprite_num+1);
  
  return 1/*true*/;
}

int windinkedit_sequence_drawSpriteInfo(WindinkeditSequence* self, int x_offset, int y_offset, WindinkeditSprite* cur_sprite, int sprite_num)
{
	int cur_frame = cur_sprite->frame - 1;
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return 0/*false*/;

	self->size = cur_sprite->size;
	// UNUSED: int x = cur_sprite->x + x_offset;
	// UNUSED: int y = cur_sprite->y + y_offset; 

	if (self->frame_image[cur_frame] == NULL)
	{
		return 0/*false*/;
	}

	int center_x2, center_y2;
	
	// fill in the destination rect
	if (windinkedit_sequence_getCenter(self, cur_frame, self->size, &center_x2, &center_y2) == 0/*false*/)
		return 0/*false*/;

	center_x2 = x_offset + cur_sprite->x - center_x2;
	center_y2 = y_offset + cur_sprite->y - center_y2;

	char buffer[50];

	struct rect fill_box;
	fill_box.left = center_x2 - 5;
	fill_box.right = center_x2 + 100;
	fill_box.top = center_y2 - 5;
	fill_box.bottom = center_y2 + 50;

	// UNUSED: struct rect clip_box = {0, 0, current_map->window_width, current_map->window_height};

	//if (fixBounds(fill_box, clip_box) == false) //damn gary!
	//	return false;

	//WC's fix second part of making draw info alittle less sloppy
	//if the text box is out of bounds, kill it.
// 	if (fill_box.bottom > windinkedit_dmod_get_map(selfp->dmod)->window_height ||
// 		fill_box.right > windinkedit_dmod_get_map(selfp->dmod)->window_width ||
// 		fill_box.top < 0 ||
// 		fill_box.left < 0)
// 		return 0/*false*/;

	//there
	drawFilledBox(fill_box, 0, 0/*false*/, lpddsback);

	// draw script name
	sprintf(buffer, "%s", cur_sprite->script);
	Draw_Text_GDI(buffer, fill_box.left + 5, fill_box.top + 35, color_sprite_info_text, lpddsback);

	// draw script brain	
	sprintf(buffer, "Brain: %d", cur_sprite->brain);
	Draw_Text_GDI(buffer, fill_box.left + 5, fill_box.top + 18, color_sprite_info_text, lpddsback);

	//draw sprite number
	sprintf(buffer, "Sprite: %d", sprite_num);
	Draw_Text_GDI(buffer, fill_box.left + 5, fill_box.top + 2, color_sprite_info_text, lpddsback);

	return 1/*true*/;
}


// draws the sprite hardness'
int windinkedit_sequence_clipRenderHardness(WindinkeditSequence* self, int x_offset, int y_offset, WindinkeditSprite* cur_sprite, struct rect* clip_rect)
{
	self->size = cur_sprite->size;

	int cur_frame = cur_sprite->frame - 1;
	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return 0/*false*/;


	double mysize = (double(self->size) / 100.00);

	int my_width = self->frame_width[cur_frame];
	int my_height = self->frame_height[cur_frame];

	int x_buff = (my_width / 2) - ((my_width * mysize) / 2);
	int y_buff = (my_height / 2) - ((my_height * mysize) / 2);

	if (cur_frame < 0 || cur_frame >= MAX_FRAMES)
		return 0/*false*/;

	if (self->frame_image[cur_frame] == NULL)
		return 0/*false*/;

	int x = cur_sprite->x + x_offset;
	int y = cur_sprite->y + y_offset;

	struct rect dest_rect;

	// fill in the destination rect
	if (self->frame_info[cur_frame] != NULL)
	{
		dest_rect.left		= (x + (self->frame_info[cur_frame]->left_boundary * mysize));
		dest_rect.top		= (y + (self->frame_info[cur_frame]->top_boundary * mysize));
		dest_rect.right		= (x + (self->frame_info[cur_frame]->right_boundary * mysize));
		dest_rect.bottom	= (y + (self->frame_info[cur_frame]->bottom_boundary * mysize));
	}
	else
	{
		// check if hardbox default should be used
		if (((self->center_x == 0) && (self->center_y == 0))
		    || ((self->type != NOTANIM)
			&& (self->left_boundary == 0) && (self->top_boundary == 0)
			&& (self->right_boundary == 0) && (self->bottom_boundary == 0)))
		{
			dest_rect.left		= (x - (self->frame_width[cur_frame] * mysize) / 4);
			dest_rect.top		= (y - ((self->frame_height[cur_frame] * mysize)) / 10);
			dest_rect.right		= (x + (self->frame_width[cur_frame]* mysize) / 4);
			dest_rect.bottom	= (y + ((self->frame_height[cur_frame] * mysize)) / 10);
		}
		else
		{
			dest_rect.left		= (x + (self->left_boundary * mysize));
			dest_rect.top		= (y + (self->top_boundary * mysize));
			dest_rect.right		= (x + (self->right_boundary * mysize));
			dest_rect.bottom	= (y + (self->bottom_boundary * mysize));
		}
	}

	// now clip sprites according to what screen they are on
	int x_max_right = SCREEN_WIDTH + x_offset;
	int y_max_right = SCREEN_HEIGHT + y_offset;

	if (fixBounds(dest_rect, clip_rect) == 0/*false*/)
		return 0/*false*/;

	int hardfillcolor = -1;
	// set the hardbox color based on whether it is a warp or not
	if (cur_sprite->warp_enabled)
	{
		// red
		hardfillcolor = color_warp_sprite_hardness;
	}
	else
	{
		// normal hardness color
		hardfillcolor = color_sprite_hardness;
	}

	// blt to destination surface
	if (cur_sprite->hardness == 1)
	{
		drawBox(&dest_rect, hardfillcolor, 1);
	}
	else
	{
		fill_rect(lpddsback, &dest_rect, hardfillcolor);
	}
	return 1/*true*/;
}

G_END_DECLS
