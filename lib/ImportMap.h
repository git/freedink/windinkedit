#ifndef _IMPORT_MAP_H_
#define _IMPORT_MAP_H_

#include <glib.h>
#include "Globals.h"

void removeImportMap();

class ImportMap
{
public:
	ImportMap();

	int loadMap(gchar *pathname);
	int importScreen(gchar *pathname, int cur_screen, int new_screen);
	bool render();
	void tileClicked();

	int screen_order[NUM_SCREENS];
	int midi_num[NUM_SCREENS];
	int indoor[NUM_SCREENS];

private:
	int source_tile;
	int dest_tile;
	gchar *src_dmod_path;
};

#endif
