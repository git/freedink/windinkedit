/**
 * Minimap preview

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "windinkedit-minimap.h"

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_port.h"
#include "rect.h"

#include "Colors.h"
#include "Common.h"
#include "Engine.h"
#include "Globals.h"
#include "MainFrame.h"
#include "windinkedit-dmod.h"
#include "windinkedit-graphics-manager.h"
#include "windinkedit-map.h"
#include "windinkedit-screen.h"


/*************/
/**  Class  **/
/**         **/
/*************/

static GObjectClass *parent_class = NULL;

enum {
        WINDINKEDIT_MINIMAP_CONSTRUCT_DMOD = 1,
};


/** Private fields **/

#define selfp (self->_priv)

struct _WindinkeditMinimapPrivate {
  Uint32 grid_color;
  int rendering_in_progress;
  WindinkeditDmod* dmod;
  WindinkeditMap* map;
  SDL_Surface* mapSquareImage[4];
  SDL_Surface* buf; // temporary surface to render a screen in
};



static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditMinimap* self = (WindinkeditMinimap*)instance;
  SDL_Surface *tmp = SDL_CreateRGBSurface(SDL_SWSURFACE,
					  SCREEN_WIDTH, SCREEN_HEIGHT, 24,
					  0, 0, 0, 0);
  self->_priv = G_TYPE_INSTANCE_GET_PRIVATE(self, WINDINKEDIT_TYPE_MINIMAP, WindinkeditMinimapPrivate);
  //image = SDL_DisplayFormat(tmp);
  self->image = NULL;
  free(tmp);
  selfp->mapSquareImage[0] = NULL;
  selfp->mapSquareImage[1] = NULL;
  selfp->mapSquareImage[2] = NULL;
  selfp->mapSquareImage[3] = NULL;
  
  selfp->grid_color = color_black;
  
  selfp->rendering_in_progress = 0;
}

/**
 * Run after properties are set
 */
static void
___constructed(GObject* object)
{
  WindinkeditMinimap* self = WINDINKEDIT_MINIMAP(object);
  if (parent_class->constructed)
    parent_class->constructed(object);
  windinkedit_minimap_loadSurface(self);
}



static void
___get_property(GObject      *object,
		guint         property_id,
		GValue       *value,
		GParamSpec   *pspec)
{
  WindinkeditMinimap *self = (WindinkeditMinimap *) object;
  
  switch (property_id) {
  case WINDINKEDIT_MINIMAP_CONSTRUCT_DMOD:
    g_value_set_object(value, selfp->dmod);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

static void
___set_property (GObject      *object,
		 guint         property_id,
		 const GValue *value,
		 GParamSpec   *pspec)
{
  WindinkeditMinimap *self = (WindinkeditMinimap *) object;
  
  switch (property_id) {
  case WINDINKEDIT_MINIMAP_CONSTRUCT_DMOD:
    if (selfp->dmod != NULL)
      g_object_unref(selfp->dmod);
    selfp->dmod = WINDINKEDIT_DMOD(g_value_get_object(value));
    selfp->map = windinkedit_dmod_get_map(selfp->dmod);
    /* dmod is passed by reference */
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

/**
 * Drop all references to objects that may reference us
 */
static void
___dispose (GObject *obj)
{
  WindinkeditMinimap *self = (WindinkeditMinimap *)obj;

  g_object_unref(selfp->dmod);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->dispose(obj);
}

/**
 * Clean-up object (after dispose)
 */
static void
___finalize(GObject *obj)
{
  WindinkeditMinimap *self = (WindinkeditMinimap *)obj;

  if (self->image != NULL)
    SDL_FreeSurface(self->image);
  if (selfp->buf != NULL)
    SDL_FreeSurface(selfp->buf);

  for (int i = 0; i < 4; i++)
    {
      if (selfp->mapSquareImage[i] != NULL)
	SDL_FreeSurface(selfp->mapSquareImage[i]);
    }

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->finalize(obj);
}


static void
___class_init(gpointer g_class,
	      gpointer g_class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
  WindinkeditMinimapClass *klass = WINDINKEDIT_MINIMAP_CLASS (g_class);
  GParamSpec *pspec;

  gobject_class->constructed = ___constructed;
  gobject_class->set_property = ___set_property;
  gobject_class->get_property = ___get_property;
  gobject_class->dispose = ___dispose;
  gobject_class->finalize = ___finalize;

  g_type_class_add_private(g_class, sizeof(WindinkeditMinimapPrivate));
  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));
  
  pspec = g_param_spec_object("dmod",
			      "Minimap dmod nick",
			      "Dmod to render in the minimap",
			      WINDINKEDIT_TYPE_DMOD /* type */,
			      (GParamFlags)(G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));
  g_object_class_install_property(gobject_class,
				  WINDINKEDIT_MINIMAP_CONSTRUCT_DMOD,
				  pspec);

  /* Minimap progress signal */
  WindinkeditMinimapClass *this_class = WINDINKEDIT_MINIMAP_CLASS(g_class);
  this_class->progress_signal_id =
    g_signal_new("progress",
		 G_TYPE_FROM_CLASS(g_class),
		 (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS),
		 NULL /* class closure */,
		 NULL /* accumulator */,
		 NULL /* accu_data */,
		 g_cclosure_marshal_VOID__INT /* c_marshaller */,
		 G_TYPE_NONE /* return_type */,
		 1 /* n_params */,
		 G_TYPE_INT);
}

GType windinkedit_minimap_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditMinimapClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      (GClassInitFunc) ___class_init,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditMinimap),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditMinimapType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/


#define MINI_MAP_WIDTH		SQUARE_WIDTH * MAP_COLUMNS
#define MINI_MAP_HEIGHT		SQUARE_HEIGHT * MAP_ROWS

#define SQUARE_EMPTY_NAME	"S06.bmp"
#define SQUARE_EMPTY		0
#define SQUARE_USED_NAME	"S07.bmp"
#define SQUARE_USED		1
#define SQUARE_MIDI_NAME	"S12.bmp"
#define SQUARE_MIDI		2
#define SQUARE_INDOOR_NAME	"S13.bmp"
#define SQUARE_INDOOR		3


int windinkedit_minimap_getScreenAt(WindinkeditMinimap *self, SDL_Surface* surface, int x, int y)
{
  double scale_x = 1.0 * surface->w / self->image->w;
  double scale_y = 1.0 * surface->h / self->image->h;
  int screen_x = (1.0 * x / scale_x) / SQUARE_WIDTH;
  int screen_y = (1.0 * y / scale_y) / SQUARE_WIDTH;
  int hover_screen = screen_y * MAP_COLUMNS + screen_x;
  return hover_screen + 1;
}

void windinkedit_minimap_stopRendering(WindinkeditMinimap *self)
{
  selfp->rendering_in_progress = 0;
}

int windinkedit_minimap_unloadSurface(WindinkeditMinimap *self)
{
  if (self->image != NULL)
    SDL_FreeSurface(self->image);
  return true;
}

// loads the 
int windinkedit_minimap_loadSquare(WindinkeditMinimap *self, const char *filename, int square_num)
{
  if (selfp->mapSquareImage[square_num] != NULL)
    return false;
  
  // load the map squares
  char bmp_file[PATH_MAX];
  
  SDL_Surface *tmp_surface = NULL;
  int error = 0;
  
  sprintf(bmp_file, "%sTiles/%s", windinkedit_dmod_get_dmoddir(selfp->dmod), filename);
  if ((tmp_surface = IMG_Load(bmp_file)) == NULL)
    {
      fprintf(stderr, "Cannot load tile %s\n", bmp_file);
      // try loading from the dink dmod directory
      sprintf(bmp_file, "%sTiles/%s", windinkedit_dmod_get_refdir(selfp->dmod), filename);
      if ((tmp_surface = IMG_Load(bmp_file)) == NULL)
	{
	  fprintf(stderr, "Cannot load tile %s\n", bmp_file);
	  error = 1;
	}
    }
  selfp->mapSquareImage[square_num] = SDL_DisplayFormat(tmp_surface);
  SDL_FreeSurface(tmp_surface);
  
  if (error == 1)
    return false;
  return true;
}

int windinkedit_minimap_loadSurface(WindinkeditMinimap *self)
{
  self->image = windinkedit_graphics_manager_allocateSurface(graphics_manager, MINI_MAP_WIDTH, MINI_MAP_HEIGHT);
  /* Work-around a bug in SDL_gfx, where resizing a minimal surface
     adds a black line at the right of the resized surface: */
  //selfp->buf = windinkedit_graphics_manager_allocateSurface(graphics_manager, SCREEN_WIDTH, SCREEN_HEIGHT);
  selfp->buf = windinkedit_graphics_manager_allocateSurface(graphics_manager, MINI_MAP_WIDTH, MINI_MAP_HEIGHT);
  
  windinkedit_minimap_loadSquare(self, SQUARE_EMPTY_NAME, SQUARE_EMPTY);
  windinkedit_minimap_loadSquare(self, SQUARE_USED_NAME, SQUARE_USED);
  windinkedit_minimap_loadSquare(self, SQUARE_MIDI_NAME, SQUARE_MIDI);
  windinkedit_minimap_loadSquare(self, SQUARE_INDOOR_NAME, SQUARE_INDOOR);
  
  // set color key to black
  Uint32 color_key = color_black;
  
  // now set the color key for source blitting
  SDL_SetColorKey(selfp->mapSquareImage[SQUARE_MIDI], SDL_SRCCOLORKEY, color_key);
  SDL_SetColorKey(selfp->mapSquareImage[SQUARE_INDOOR], SDL_SRCCOLORKEY, color_key);
  
  windinkedit_minimap_drawSquares(self);
  
  return 1;
}

// draws the entire minimap
int windinkedit_minimap_drawMap(WindinkeditMinimap *self, SDL_Surface* surface)
{
  if (selfp->rendering_in_progress == 1)
    return -1;

  selfp->rendering_in_progress = 1;
  
  int cur_screen = 1;
  for (int y = 0; y < MAP_ROWS; y++)
    {
      for (int x = 0; x < MAP_COLUMNS; x++, cur_screen++)
	{
	  // Stop if asked to
	  if (selfp->rendering_in_progress == 0)
	    return -1;

	  //skip if it doesn't need to be updated
	  if (!selfp->map->miniupdated[cur_screen])
	    {
	      // Recompute current screen
	      windinkedit_minimap_updateScreen(self, cur_screen);

	      // Show progress is active
	      if (show_minimap_progress)
		windinkedit_minimap_refreshScreen(self, cur_screen, surface);
	    }
	}
      /* Emit the signal at each line rather than each square because
	 the Gtk progressbar is quite slow to refresh. */
      g_signal_emit(self, WINDINKEDIT_MINIMAP_GET_CLASS(self)->progress_signal_id,
		    0 /* details */, cur_screen - 1/*balances cur_screen++*/);
    }
  selfp->rendering_in_progress = 0;
  return 0;
}

// recompute one screen preview
int windinkedit_minimap_updateScreen(WindinkeditMinimap *self, int screen_idx)
{
  int screen_maths = screen_idx - 1; // start from 0 instead of 1 for maths
  if (selfp->map->screen[screen_maths] != NULL)
    {

      // make clip box for use when rendering each screen
      struct rect clip_box = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

      // Clean-up buffer (avoid displaying parts of previous screen)
      fill_rect(selfp->buf, NULL, selfp->grid_color);
      
      // Draw screen
      windinkedit_screen_drawTiles(selfp->map->screen[screen_maths], selfp->buf, 0, 0);
      windinkedit_screen_drawSprites(selfp->map->screen[screen_maths], selfp->map->cur_vision, selfp->buf, 0, 0);

      // Draw mini screen on minimap
      struct rect src_rect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};
      struct rect dst_rect;
      int col = screen_maths % MAP_COLUMNS;
      int row = screen_maths / MAP_COLUMNS;
      dst_rect.left   = col * SQUARE_WIDTH;
      dst_rect.top    = row * SQUARE_HEIGHT;
      dst_rect.right  = dst_rect.left + SQUARE_WIDTH;
      dst_rect.bottom = dst_rect.top  + SQUARE_HEIGHT;
      //BltStretch(self->image, &dst_rect, selfp->buf, NULL);
      BltStretch(self->image, &dst_rect, selfp->buf, &src_rect);
    }

  // Mark this screen as updated
  selfp->map->miniupdated[screen_maths] = true;
}

// refresh screen on graphic display, with scaling
int windinkedit_minimap_refreshScreen(WindinkeditMinimap *self, int screen_idx, SDL_Surface* surface)
{
      struct rect dst_rect;
      int screen_maths = screen_idx - 1; // start from 0 instead of 1 for maths
      int col = screen_maths % MAP_COLUMNS;
      int row = screen_maths / MAP_COLUMNS;
      dst_rect.left   = col * SQUARE_WIDTH;
      dst_rect.top    = row * SQUARE_HEIGHT;
      dst_rect.right  = dst_rect.left + SQUARE_WIDTH;
      dst_rect.bottom = dst_rect.top  + SQUARE_HEIGHT;

  double scale_x = 1.0 * surface->w / self->image->w;
  double scale_y = 1.0 * surface->h / self->image->h;
  struct rect dst_rect_scaled;
  dst_rect_scaled.left   = dst_rect.left   * scale_x;
  dst_rect_scaled.top    = dst_rect.top    * scale_y;
  dst_rect_scaled.right  = dst_rect.right  * scale_x;
  dst_rect_scaled.bottom = dst_rect.bottom * scale_y;

  BltStretch(surface, &dst_rect_scaled, self->image, &dst_rect);
//   SDL_UpdateRect(surface,
// 		 dst_rect_scaled.left, dst_rect_scaled.top,
// 		 dst_rect_scaled.right-dst_rect_scaled.left, dst_rect_scaled.bottom-dst_rect_scaled.top);
}

// draws purple or red tile on minimap for target screen
int windinkedit_minimap_renderMapSquare(WindinkeditMinimap *self, int cur_screen)
{
	int x_square = cur_screen % MAP_COLUMNS;
	int y_square = cur_screen / MAP_COLUMNS;

	struct rect dest = {0, 0, SQUARE_WIDTH, 0};
	dest.left = x_square * SQUARE_WIDTH;
	dest.top = y_square * SQUARE_HEIGHT;
	dest.right = dest.left + SQUARE_WIDTH;
	dest.bottom = dest.top + SQUARE_HEIGHT;

	// draw blank screen tile
	if (selfp->map->screen[cur_screen] == NULL)
	{
		Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_EMPTY], NULL);
	}	
	else
	{
		// draw non-blank screen tile
		Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_USED], NULL);
		if (selfp->map->midi_num[cur_screen] != 0)
			Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_MIDI], NULL);
		if (selfp->map->indoor[cur_screen] == 1)
			Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_INDOOR], NULL);
	}

	return true;
}

// draws purple and red tiles for entire minimap
int windinkedit_minimap_drawSquares(WindinkeditMinimap *self)
{
	struct rect dest = {0, 0, SQUARE_WIDTH, SQUARE_HEIGHT};
	int cur_screen = 0;

	for (int y = 0; y < MAP_ROWS; y++)
	{
		dest.right = SQUARE_WIDTH;
		dest.left = 0;
		for (int x = 0; x < MAP_COLUMNS; x++)
		{
			// draw blank screen tile
			if (selfp->map->screen[cur_screen] == NULL)
			{
				Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_EMPTY], NULL);
			}
			// draw non-blank screen tile
			else
			{
				Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_USED], NULL);
				if (selfp->map->midi_num[cur_screen] != 0)
					Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_MIDI], NULL);
				if (selfp->map->indoor[cur_screen] == 1)
					Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_INDOOR], NULL);
			}

			cur_screen++;

			dest.left += SQUARE_WIDTH;
			dest.right += SQUARE_WIDTH;
		}
		dest.bottom += SQUARE_HEIGHT;
		dest.top += SQUARE_HEIGHT;
	}

	return true;
}

// used when importing a screen from another map
int windinkedit_minimap_renderImportMap(WindinkeditMinimap *self, int screen_order[], int midi_num[], int indoor[])
{
	int cur_screen = 0;

	struct rect dest = {0, 0, SQUARE_WIDTH, SQUARE_HEIGHT};
	for (int y = 0; y < MAP_ROWS; y++)
	{
		dest.right = SQUARE_WIDTH;
		dest.left = 0;
		for (int x = 0; x < MAP_COLUMNS; x++)
		{
			if (screen_order[cur_screen] == 0)
			{
				Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_EMPTY], NULL);
			}
			else
			{
				Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_USED], NULL);
				if (midi_num[cur_screen] != 0)
					Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_MIDI], NULL);
				if (indoor[cur_screen] == 1)
					Blt(self->image, &dest, selfp->mapSquareImage[SQUARE_INDOOR], NULL);

			}

			dest.left += SQUARE_WIDTH;
			dest.right += SQUARE_WIDTH;

			cur_screen++;
		}
		dest.bottom += SQUARE_HEIGHT;
		dest.top += SQUARE_HEIGHT;
	}

	return true;
}

// draws the minimap onto the screen
void windinkedit_minimap_render(WindinkeditMinimap *self, SDL_Surface* surface)
{
  BltStretch(surface, NULL, self->image, NULL);
//  windinkedit_minimap_drawGrid(self);
}

// draws a box around the currently highlighted screen
void windinkedit_minimap_highlight(WindinkeditMinimap *self, int screen_idx, SDL_Surface* surface)
{
  int screen_maths = screen_idx - 1; // start from 0 instead of 1 for maths
  double scale_x = 1.0 * surface->w / self->image->w;
  double scale_y = 1.0 * surface->h / self->image->h;
  struct rect dst_rect;
  dst_rect.left = (screen_maths % MAP_COLUMNS) * SQUARE_WIDTH  * scale_x;
  dst_rect.top  = (screen_maths / MAP_COLUMNS) * SQUARE_HEIGHT * scale_y;
  dst_rect.right = dst_rect.left + SQUARE_WIDTH * scale_x;
  dst_rect.bottom = dst_rect.top + SQUARE_HEIGHT * scale_y;

  drawBox(&dst_rect, color_minimap_screen_selected, 1);
//   SDL_UpdateRect(surface,
// 		 dst_rect.left, dst_rect.top,
// 		 dst_rect.right-dst_rect.left, dst_rect.bottom-dst_rect.top);
}
