#ifndef MAINFRM_H
#define MAINFRM_H

#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <wx/splitter.h>
#include <wx/treectrl.h>
#include <wx/xrc/xmlres.h>
#include <wx/docview.h>

//#include "StatusBar.h"

class CWinDinkeditView;
//class CLeftView;

class CMainFrame : wxFrame
{

public:
	int updateMousePos(int screen, int x, int y);
	int updateVision(int vision);
	int updateScreenMatch(bool enabled);
	int updateSpriteHard(bool enabled);
	CMainFrame();

// Attributes
protected:
//	wxSplitterWindow m_wndSplitter;
public:

// Operations
public:
/* 	void OnUpdateMousePos(CCmdUI* pCmdUI); */
/* 	void OnUpdateVision(CCmdUI* pCmdUI); */
/* 	void OnUpdateScreenMatch(CCmdUI* pCmdUI); */
/* 	void OnUpdateSpriteHard(CCmdUI* pCmdUI); */
/* 	void OnTimer(unsigned int nIdEvent); */
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
/* 	virtual /\*BOOL*\/int OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext); */
/* 	virtual /\*BOOL*\/int PreCreateWindow(CREATESTRUCT& cs); */
	//}}AFX_VIRTUAL


	void OnNewDmod(wxCommandEvent &event);


// Implementation
public:
	virtual ~CMainFrame();
	CWinDinkeditView* GetRightPane();
//	CLeftView* GetLeftPane();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:  // control bar embedded members
//	CProgStatusBar  m_wndStatusBar;
	wxToolBar    m_wndToolBar;
	wxGauge m_wndProgress;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
/* 	int OnCreate(LPCREATESTRUCT lpCreateStruct); */
/* 	void OnMove(int x, int y); */
/* 	void OnUpdateDmodProperties(CCmdUI* pCmdUI); */
/* 	void OnUpdateFileSave(CCmdUI* pCmdUI); */
/* 	//}}AFX_MSG */
/* 	void OnUpdateViewStyles(CCmdUI* pCmdUI); */
/* 	void OnViewStyle(unsigned int nCommandID); */
	void OnFileOpen(wxCommandEvent &event);
	DECLARE_EVENT_TABLE()
public:
	void OnFileSave(wxCommandEvent &event);
	void OnFilePlaygame(wxCommandEvent &event);
/* 	void OnUpdateFilePlaygame(CCmdUI *pCmdUI); */
	void OnExit(wxCommandEvent &event);

	wxDocument* OpenDocumentFile(wxString file_name);
};

/////////////////////////////////////////////////////////////////////////////

extern CMainFrame* mainWnd;

#endif
