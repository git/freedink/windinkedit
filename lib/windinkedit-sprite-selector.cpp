/**
 * Sprite selection mode

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "windinkedit-sprite-selector.h"

#include "Colors.h"
#include "Common.h"
#include "Globals.h"
#include "windinkedit-map.h"
#include "windinkedit-sequence.h"
#include "windinkedit-sprite.h"
#include "Structs.h"
#include "Undo.h"
#include "Modes.h"


/*************/
/**  Class  **/
/**         **/
/*************/

G_BEGIN_DECLS

static GObjectClass *parent_class = NULL;

/** Private fields **/

#define selfp (self->_priv)

struct _WindinkeditSpriteSelectorPrivate {
  int max_pics;
  int selectorScreen;
  int max_sprite;
  int sprites_per_row;
  int sprites_per_column;
  int pics_displayed;
  int spriteSelectorList[MAX_SEQUENCES];
};



static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditSpriteSelector* self = (WindinkeditSpriteSelector*)instance;
  self->_priv = G_TYPE_INSTANCE_GET_PRIVATE(self, WINDINKEDIT_TYPE_SPRITE_SELECTOR, WindinkeditSpriteSelectorPrivate);
  self->showFrames = 0/*false*/;
}

/**
 * Run after properties are set
 */
static void
___constructed(GObject* object)
{
  WindinkeditSpriteSelector* self = WINDINKEDIT_SPRITE_SELECTOR(object);
  if (parent_class->constructed)
    parent_class->constructed(object);
}



static void
___get_property(GObject      *object,
		guint         property_id,
		GValue       *value,
		GParamSpec   *pspec)
{
  WindinkeditSpriteSelector *self = (WindinkeditSpriteSelector *) object;
  
  switch (property_id) {
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

static void
___set_property (GObject      *object,
		 guint         property_id,
		 const GValue *value,
		 GParamSpec   *pspec)
{
  WindinkeditSpriteSelector *self = (WindinkeditSpriteSelector *) object;
  
  switch (property_id) {
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

/**
 * Drop all references to objects that may reference us
 */
static void
___dispose (GObject *obj)
{
  WindinkeditSpriteSelector *self = (WindinkeditSpriteSelector *)obj;

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->dispose(obj);
}

/**
 * Clean-up object (after dispose)
 */
static void
___finalize(GObject *obj)
{
  WindinkeditSpriteSelector *self = (WindinkeditSpriteSelector *)obj;

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->finalize(obj);
}


static void
___class_init(gpointer g_class,
	      gpointer g_class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
  WindinkeditSpriteSelectorClass *klass = WINDINKEDIT_SPRITE_SELECTOR_CLASS (g_class);
  GParamSpec *pspec;

  gobject_class->constructed = ___constructed;
  gobject_class->set_property = ___set_property;
  gobject_class->get_property = ___get_property;
  gobject_class->dispose = ___dispose;
  gobject_class->finalize = ___finalize;

  g_type_class_add_private(g_class, sizeof(WindinkeditSpriteSelectorPrivate));
  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));
}

GType windinkedit_sprite_selector_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditSpriteSelectorClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      (GClassInitFunc) ___class_init,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditSpriteSelector),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditSpriteSelectorType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/

WindinkeditSpriteSelector* windinkedit_sprite_selector_new()
{
  WindinkeditSpriteSelector* sprite_selector
    = WINDINKEDIT_SPRITE_SELECTOR(g_object_new(WINDINKEDIT_TYPE_SPRITE_SELECTOR, NULL));
  return sprite_selector;
}

void windinkedit_sprite_selector_nextPage(WindinkeditSpriteSelector* self)
{
	if ((selfp->selectorScreen + 1) * selfp->max_pics < selfp->max_sprite)
		selfp->selectorScreen++;
}

void windinkedit_sprite_selector_prevPage(WindinkeditSpriteSelector* self)
{
	if (selfp->selectorScreen > 0)
		selfp->selectorScreen--;
}

void windinkedit_sprite_selector_createList(WindinkeditSpriteSelector* self)
{
	selfp->max_sprite = 0;
	for (int i = 0; i < MAX_SEQUENCES; i++)
	{
		if (current_map->sequence[i] != NULL)
		{
			// add sequence to sprite selector list
			selfp->spriteSelectorList[selfp->max_sprite] = i;

			selfp->max_sprite++;
		}
	}
}

void windinkedit_sprite_selector_resizeScreen(WindinkeditSpriteSelector* self)
{
	selfp->sprites_per_row = current_map->window_width / (SPRITE_SELECTOR_BMP_WIDTH + SPRITE_SELECTOR_BMP_GAP);
	selfp->sprites_per_column = current_map->window_height / (SPRITE_SELECTOR_BMP_HEIGHT + SPRITE_SELECTOR_BMP_GAP);

	selfp->max_pics = selfp->sprites_per_row * selfp->sprites_per_column;

	selfp->selectorScreen = 0;
}

// draw the sequences(first frame only) or frames
int windinkedit_sprite_selector_render(WindinkeditSpriteSelector* self)
{
	if (self->showFrames)
	{
		// draw all frames of current sequence
		selfp->pics_displayed = 0;
		int frame_exist;

		// if no frame, stop drawing
		if (current_map->sequence[self->currentSequence] == NULL)
			return 0/*false*/;

		int maxframe = selfp->max_pics;
		if (maxframe >= MAX_FRAMES)
			maxframe = MAX_FRAMES;

		for (int y = 0; y < selfp->sprites_per_column && selfp->pics_displayed < maxframe; y++)
		{
			for (int x = 0; x < selfp->sprites_per_row && selfp->pics_displayed < maxframe; x++)
			{
				// draw the frame
				frame_exist = windinkedit_sequence_render(
					current_map->sequence[self->currentSequence],
					x * (SPRITE_SELECTOR_BMP_WIDTH + SPRITE_SELECTOR_BMP_GAP), 
					y * (SPRITE_SELECTOR_BMP_HEIGHT + SPRITE_SELECTOR_BMP_GAP), selfp->pics_displayed);

				if (!frame_exist)
					return 1/*true*/;

				selfp->pics_displayed++;
			}
		}
	}
	else
	{
		// draw first frame of each sequence
		selfp->pics_displayed = selfp->selectorScreen * selfp->max_pics;
		for (int y = 0; y < selfp->sprites_per_column && selfp->pics_displayed < selfp->max_sprite; y++)
		{
			for (int x = 0; x < selfp->sprites_per_row && selfp->pics_displayed < selfp->max_sprite; x++)
			{
				windinkedit_sequence_render(
					current_map->sequence[selfp->spriteSelectorList[selfp->pics_displayed] ],
					x * (SPRITE_SELECTOR_BMP_WIDTH + SPRITE_SELECTOR_BMP_GAP), 
					y * (SPRITE_SELECTOR_BMP_HEIGHT + SPRITE_SELECTOR_BMP_GAP), 0);
				
				selfp->pics_displayed++;
			}
		}
	}

	return 1/*true*/;
}

// universal method, works for both frames and sequences with no changes
int windinkedit_sprite_selector_drawGrid(WindinkeditSpriteSelector* self, int x, int y)
{
	int x_sprite = x / (SPRITE_SELECTOR_BMP_WIDTH + SPRITE_SELECTOR_BMP_GAP);
	int y_sprite = y / (SPRITE_SELECTOR_BMP_HEIGHT + SPRITE_SELECTOR_BMP_GAP);

	if (x_sprite >= selfp->sprites_per_row || y_sprite >= selfp->sprites_per_column)
		return 0/*false*/;

	int pic_selected = y_sprite * selfp->sprites_per_row + x_sprite;

	if (!self->showFrames)
	{
		pic_selected += selfp->selectorScreen * selfp->max_pics;
	}

	// make sure it is a valid sequence
	if (pic_selected >= selfp->pics_displayed)
		return 0/*false*/;

	struct rect dest = {(SPRITE_SELECTOR_BMP_WIDTH + SPRITE_SELECTOR_BMP_GAP) * x_sprite - SPRITE_SELECTOR_BMP_GAP,
			(SPRITE_SELECTOR_BMP_HEIGHT + SPRITE_SELECTOR_BMP_GAP) * y_sprite - SPRITE_SELECTOR_BMP_GAP,
			(SPRITE_SELECTOR_BMP_WIDTH + SPRITE_SELECTOR_BMP_GAP) * x_sprite + SPRITE_SELECTOR_BMP_WIDTH + SPRITE_SELECTOR_BMP_GAP,
			(SPRITE_SELECTOR_BMP_HEIGHT + SPRITE_SELECTOR_BMP_GAP) * y_sprite + SPRITE_SELECTOR_BMP_HEIGHT + SPRITE_SELECTOR_BMP_GAP};
	drawBox(&dest, color_tile_grid, SPRITE_SELECTOR_BMP_GAP);


	if (self->showFrames)
	{
		self->currentFrame = pic_selected;
	}
	else
	{
		self->currentSequence = selfp->spriteSelectorList[pic_selected];
		self->currentFrame = 0;
	}

	char buffer[50];
	sprintf(buffer, "Sequence: %d, Frame: %d", self->currentSequence, self->currentFrame + 1);
//	mainWnd->SetStatusText(wxString(buffer, wxConvUTF8));
	printf("windinkedit_sprite_selector_drawGrid(): mainWnd->SetStatusText(%s): TODO\n", buffer);

	return 1/*true*/;
}

void windinkedit_sprite_selector_getSprite(WindinkeditSpriteSelector* self, int x, int y)
{  
	if (self->showFrames)
	{ 
		printf("windinkedit_sprite_selector_drawGrid(): mainWnd->SetStatusText(''): TODO\n");
//		mainWnd->SetStatusText(wxT("")); 
		current_map->render_state = SHOW_SCREEN; 
		WindinkeditSprite* new_mouse_sprite = windinkedit_sprite_new(); 
  
		new_mouse_sprite->sequence = self->currentSequence;	
		new_mouse_sprite->frame = self->currentFrame + 1;  
		new_mouse_sprite->size = 100;  
		new_mouse_sprite->hardness = !sprite_hard; 
		new_mouse_sprite->type = 1;  
		new_mouse_sprite->base_attack = -1;  
		new_mouse_sprite->base_death = -1; 
		new_mouse_sprite->base_idle = -1;  
		new_mouse_sprite->base_walk = -1;  
		new_mouse_sprite->timing = 33; 
		windinkedit_sprite_setVision(new_mouse_sprite, current_map->cur_vision); 
 
		UNDO_ACTION* action = new UNDO_ACTION;  
		action->type = UT_SPRITE_CREATE;  

		if (windinkedit_map_getMouseSprite(current_map) == NULL)
		{ 
			action->sprite_create.old_mouse_sprite = NULL; 
		}
		else
		{
		  action->sprite_create.old_mouse_sprite = windinkedit_map_getMouseSprite(current_map);
		}

		action->sprite_create.new_mouse_sprite = new_mouse_sprite; 
		undo_buffer->addUndo(action); 
	} 
	else 
	{ 
		self->showFrames = 1/*true*/;  
	} 
}

G_END_DECLS
