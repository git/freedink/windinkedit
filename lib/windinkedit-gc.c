/**
 * Draw from non-C languages, and with optional subsurface

 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "windinkedit-gc.h"

#include "SDL.h"



/*************/
/**  Class  **/
/**         **/
/*************/

static GObjectClass *parent_class = NULL;

enum {
  WINDINKEDIT_GC_CONSTRUCT_XXX = 1,
};


/** Private fields **/

#define selfp (self->_priv)

struct _WindinkeditGCPrivate {
  int nothing_yet;
};


static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditGC* self = (WindinkeditGC*)instance;

  self->_priv = G_TYPE_INSTANCE_GET_PRIVATE(self, WINDINKEDIT_TYPE_GC, WindinkeditGCPrivate);

  self->surface = SDL_GetVideoSurface();
  SDL_GetClipRect(self->surface, &self->arect);
}

/**
 * Run after properties are set
 */
static void
___constructed(GObject* object)
{
  WindinkeditGC* self = WINDINKEDIT_GC(object);
  
}


static void
___get_property(GObject      *object,
		guint         property_id,
		GValue       *value,
		GParamSpec   *pspec)
{
  WindinkeditGC *self = (WindinkeditGC *) object;
  
  switch (property_id) {
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

static void
___set_property (GObject      *object,
		 guint         property_id,
		 const GValue *value,
		 GParamSpec   *pspec)
{
  WindinkeditGC *self = (WindinkeditGC *) object;
  
  switch (property_id) {
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

static void
___dispose (GObject *obj)
{
  WindinkeditGC *self = (WindinkeditGC *)obj;
  
  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->dispose(obj);
}

static void
___finalize (GObject *obj)
{
  WindinkeditGC *self = (WindinkeditGC *)obj;
  
  self->surface = NULL;
  
  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->finalize(obj);
}


static void
___class_init (gpointer g_class,
			    gpointer g_class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(g_class);
  WindinkeditGCClass *klass = WINDINKEDIT_GC_CLASS(g_class);
  GParamSpec *pspec;
  
  gobject_class->constructed = ___constructed;
  gobject_class->set_property = ___set_property;
  gobject_class->get_property = ___get_property;
  gobject_class->dispose = ___dispose;
  gobject_class->finalize = ___finalize;

  g_type_class_add_private(g_class, sizeof(WindinkeditGCPrivate));
  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));
}

GType windinkedit_gc_get_type (void)
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditGCClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      (GClassInitFunc) ___class_init,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditGC),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditGCType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/

WindinkeditGC* windinkedit_gc_new()
{
  return WINDINKEDIT_GC(g_object_new(WINDINKEDIT_TYPE_GC, NULL));
}

int windinkedit_gc_set_active_rect(WindinkeditGC* self, int x, int y, int w, int h)
{
  self->arect.x = x;
  self->arect.y = y;
  self->arect.w = w;
  self->arect.h = h;
  SDL_SetClipRect(self->surface, &self->arect);
}

int windinkedit_gc_clear_active_rect(WindinkeditGC* self)
{
  self->arect.x = 0;
  self->arect.y = 0;
  self->arect.w = self->surface->w;
  self->arect.h = self->surface->h;
  SDL_SetClipRect(self->surface, NULL);
}

int windinkedit_gc_flip(WindinkeditGC* self)
{
  SDL_Flip(self->surface);
}

int windinkedit_gc_blit_surface(WindinkeditGC* self, SDL_Surface* src, struct rect* srcrect, struct rect* dstrect)
{
  SDL_Rect* sdl_srcrect = NULL;
  if (srcrect != NULL)
    {
      sdl_srcrect = g_newa(SDL_Rect, 1);
      sdl_srcrect->x = srcrect->left;
      sdl_srcrect->y = srcrect->top;
      sdl_srcrect->w = srcrect->right - srcrect->left;
      sdl_srcrect->h = srcrect->bottom - srcrect->top;
    }
  SDL_Rect tr_dstrect = {self->arect.x, self->arect.y};
  if (dstrect != NULL)
    {
      tr_dstrect.x += dstrect->left;
      tr_dstrect.y += dstrect->top;
    }
  SDL_BlitSurface(src, sdl_srcrect, self->surface, &tr_dstrect);
}

int windinkedit_gc_fill_rect(WindinkeditGC* self, int x, int y, int w, int h, int r, int g, int b)
{
  SDL_Rect tr_dstrect = {x + self->arect.x, y + self->arect.y, w, h};
  SDL_FillRect(self->surface, &tr_dstrect, SDL_MapRGB(self->surface->format, r, g, b));
}

/**
 * Get clip rect relative to the active area's top-left corner (to
 * ease out-of-boundingbox checks)
 */
int windinkedit_gc_get_relative_clip_rect(WindinkeditGC* self, struct rect* clip)
{
  SDL_Rect cur_sdl_clip;
  SDL_GetClipRect(self->surface, &cur_sdl_clip);
  clip->left = cur_sdl_clip.x - self->arect.x;
  clip->top = cur_sdl_clip.y - self->arect.y;
  clip->right = clip->left + cur_sdl_clip.w;
  clip->bottom = clip->top + cur_sdl_clip.h;
}
