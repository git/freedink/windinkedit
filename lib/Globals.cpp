/**
 * Global variables initialization

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

//#include "WinDinkeditView.h"

#include "SDL.h"

#include "Globals.h"
#include "windinkedit-tile.h"
// #include "windinkedit-map.h"
// #include "Engine.h"

#include "Colors.h"

// GLOBALS ////////////////////////////////////////////////

// LPDIRECTDRAW7			lpdd7			= NULL;		// dd7 object
SDL_Surface*	lpddsback		= NULL;		// dd back surface
SDL_Surface*	lpddshidden		= NULL;     // dd hidden surface
// LPDIRECTDRAWCLIPPER		lpddclipper		= NULL;		// dd clipper
// LPDIRECTDRAWCLIPPER		lpddclipperprimary = NULL;	// dd clipper
// DDSURFACEDESC2			ddsdback;		// a direct draw surface description struct
// DDSURFACEDESC2			ddsdhidden;		// a direct draw surface description struct

// DDBLTFX hardFill;

struct rect mapRect;	// stores the location of the drawing window

WindinkeditDmod* current_dmod = NULL;
WindinkeditMap* current_map = NULL;

WindinkeditTile* tileBmp[MAX_TILE_BMPS];

WindinkeditGraphicsManager* graphics_manager = NULL;

unsigned char vision_used[VISIONS_TRACKED];
unsigned char new_vision_used[VISIONS_TRACKED];

int/*bool*/ need_resizing = true;
int/*bool*/ displayhardness = false;
int/*bool*/ sprite_hard = false;
int/*bool*/ show_sprite_info = false;
int/*bool*/ show_minimap_progress = true;
int/*bool*/ optimize_dink_ini = false;
int/*bool*/ fast_minimap_update = true;
int/*bool*/ hover_sprite_info = false;
int/*bool*/ hover_sprite_hardness = false;
int/*bool*/ help_text = true;
int/*bool*/ update_minimap = false;

int screen_gap = 10;
int max_undo_level = 50;
int auto_save_time = 0;
int tile_brush_size = 0;

gchar *main_dink_path;	// pathname to dink directory
gchar *dink_path;	// pathname to dink dmod directory
