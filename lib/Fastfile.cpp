#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
// Fastfile.cpp : implementation file
//


#include "WinDinkedit.h"
#include "Fastfile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Fastfile dialog


Fastfile::Fastfile(wxWindow* pParent /*=NULL*/)
	: wxDialog(Fastfile::IDD, pParent)
{
	//{{AFX_DATA_INIT(Fastfile)
	m_delete_graphics = /*FALSE*/0;
	//}}AFX_DATA_INIT
}


void Fastfile::DoDataExchange(wxValidator* pDX)
{
	wxDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Fastfile)
	DDX_Check(pDX, IDC_DELETE_GRAPHICS, m_delete_graphics);
	//}}AFX_DATA_MAP
}


BEGIN_EVENT_TABLE(Fastfile, wxDialog)
	//{{AFX_MSG_MAP(Fastfile)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_EVENT_TABLE()

/////////////////////////////////////////////////////////////////////////////
// Fastfile message handlers
