/**
 * Graphic mode to edit sprite dink.ini properties

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "windinkedit-sprite-editor.h"

#include "Globals.h"
#include "rect.h"

#include "windinkedit-map.h"
#include "windinkedit-screen.h"
#include "windinkedit-sprite.h"
#include "windinkedit-sprite-editor.h"
#include "Engine.h"
#include "Colors.h"
#include "Common.h"
#include "windinkedit-sequence.h"
#include <fstream>
using namespace std;

#include "SDL_port.h"

/*************/
/**  Class  **/
/**         **/
/*************/

/** Private fields **/

#define selfp (self->_priv)

struct _WindinkeditSpriteEditorPrivate {
  struct rect editor_bounds; //for the bounds to draw sprite
  struct rect hardness_bounds; //real sprite bounds
  int y_offset, x_offset; //for recalculating the hardness
  int seq, frame; //our trusty sequence and frame in the ini
  int width, height;
  int x1, x2, y1, y2;
  int depth_x, depth_y;
  bool edited;
};



static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditSpriteEditor* self = (WindinkeditSpriteEditor*)instance;
  memset(&selfp->hardness_bounds, 0, sizeof(struct rect));
  selfp->x1 = NULL;
  selfp->x2 = NULL;
  selfp->y1 = NULL;
  selfp->y2 = NULL;
  selfp->depth_x = NULL;
  selfp->depth_y = NULL;
  selfp->edited = false;
}

static void
___class_init (gpointer g_class,
	       gpointer g_class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
  WindinkeditSpriteEditorClass *klass = WINDINKEDIT_SPRITE_EDITOR_CLASS (g_class);
  GParamSpec *pspec;

  g_type_class_add_private(g_class, sizeof(WindinkeditSpriteEditorPrivate));
}

GType windinkedit_sprite_editor_get_type (void)
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditSpriteEditorClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      (GClassInitFunc) ___class_init,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditSpriteEditor),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditSpriteEditorType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/

void windinkedit_sprite_editor_DrawSprite(WindinkeditSpriteEditor* self)
{
	selfp->seq = current_map->mouseSprite->sequence;
	selfp->frame = current_map->mouseSprite->frame;
	selfp->width = current_map->sequence[selfp->seq]->frame_width[selfp->frame-1];
	selfp->height = current_map->sequence[selfp->seq]->frame_height[selfp->frame-1];
	selfp->x_offset = (current_map->window_width - selfp->width) / 2;
	selfp->y_offset = (current_map->window_height - selfp->height) / 2;

	selfp->editor_bounds.top = selfp->y_offset;
	selfp->editor_bounds.bottom = current_map->window_height - selfp->y_offset;
	selfp->editor_bounds.left = selfp->x_offset;
	selfp->editor_bounds.right = current_map->window_width - selfp->x_offset;

	Blt(lpddsback, &selfp->editor_bounds,
	    current_map->sequence[selfp->seq]->frame_image[selfp->frame-1], NULL);
}

void windinkedit_sprite_editor_DrawHardness(WindinkeditSpriteEditor* self)
{
	if (!selfp->depth_x && !selfp->depth_y)
	{
	  windinkedit_sequence_getCenter(current_map->sequence[selfp->seq], selfp->frame-1, 100,
							     selfp->depth_x, selfp->depth_y);
	}


	if (selfp->x1 || selfp->x2 || selfp->y1 || selfp->y2)
	{
		selfp->hardness_bounds.left = selfp->x1 + selfp->depth_x;
		selfp->hardness_bounds.right = selfp->x2 + selfp->depth_x;
		selfp->hardness_bounds.top = selfp->y1 + selfp->depth_y;
		selfp->hardness_bounds.bottom = selfp->y2 + selfp->depth_y;
	}else {

		if (current_map->sequence[selfp->seq]->frame_info[selfp->frame-1])
		{
			//use frame hardness
			selfp->depth_y = current_map->sequence[selfp->seq]->frame_info[selfp->frame-1]->center_y;
			selfp->depth_x = current_map->sequence[selfp->seq]->frame_info[selfp->frame-1]->center_x;
			selfp->x1 = current_map->sequence[selfp->seq]->frame_info[selfp->frame-1]->left_boundary;
			selfp->x2 = current_map->sequence[selfp->seq]->frame_info[selfp->frame-1]->right_boundary;
			selfp->y1 = current_map->sequence[selfp->seq]->frame_info[selfp->frame-1]->top_boundary;
			selfp->y2 = current_map->sequence[selfp->seq]->frame_info[selfp->frame-1]->bottom_boundary;
		}
		else if (current_map->sequence[selfp->seq]->left_boundary || 
			current_map->sequence[selfp->seq]->right_boundary || 
			current_map->sequence[selfp->seq]->top_boundary ||
			current_map->sequence[selfp->seq]->left_boundary)
		{
			//use sequence hardness
			selfp->depth_y = current_map->sequence[selfp->seq]->center_y;
			selfp->depth_x = current_map->sequence[selfp->seq]->center_x;
			selfp->x1 = current_map->sequence[selfp->seq]->left_boundary;
			selfp->x2 = current_map->sequence[selfp->seq]->right_boundary;
			selfp->y1 = current_map->sequence[selfp->seq]->top_boundary;
			selfp->y2 = current_map->sequence[selfp->seq]->bottom_boundary;
		}
		else
		{
			//use default hardness
			selfp->x1 = (selfp->depth_x - (current_map->sequence[selfp->seq]->frame_width[selfp->frame-1] / 4)) - selfp->depth_x;
			selfp->y1 = (selfp->depth_y - (current_map->sequence[selfp->seq]->frame_height[selfp->frame-1] / 10)) - selfp->depth_y;
			selfp->x2 = (selfp->depth_x + (current_map->sequence[selfp->seq]->frame_width[selfp->frame-1] / 4)) - selfp->depth_x;
			selfp->y2 = (selfp->depth_y + (current_map->sequence[selfp->seq]->frame_height[selfp->frame-1] / 10)) - selfp->depth_y;
		}
	}
	int x = selfp->depth_x + selfp->x_offset;
	int y = selfp->depth_y + selfp->y_offset;

	selfp->hardness_bounds.left += selfp->x_offset;
	selfp->hardness_bounds.right += selfp->x_offset;
	selfp->hardness_bounds.top += selfp->y_offset;
	selfp->hardness_bounds.bottom += selfp->y_offset;

	struct rect depth_dot = {x-1, y-1, x+2, y+2};

	drawFilledBox(selfp->hardness_bounds, SDL_MapRGB(lpddsback->format, 128,128,128), false, lpddsback);
	drawFilledBox(depth_dot, SDL_MapRGB(lpddsback->format, 0,255,255), false, lpddsback);
}

void windinkedit_sprite_editor_Save(WindinkeditSpriteEditor* self)
{
	if (selfp->edited)
	{
	  //		mainWnd->GetActiveDocument()->SetModifiedFlag(/*TRUE*/1);
	  printf("windinkedit_sprite_editor_Save() mainWnd->GetActiveDocument()->SetModifiedFlag(true);\n");
		selfp->edited = false;
		windinkedit_sequence_addFrameInfo(current_map->sequence[selfp->seq], selfp->frame-1, selfp->depth_x, selfp->depth_y,
								selfp->x1, selfp->y1, selfp->x2, selfp->y2);

		if (!optimize_dink_ini)
		{
			char buffer[255];
			sprintf(buffer, "%s%s", current_map->dmod_path, "dink.ini");
			ofstream fout(buffer, ios::app);
			sprintf(buffer, "\nset_sprite_info %i %i %i %i %i %i %i %i\n",
				selfp->seq, selfp->frame, selfp->depth_x, selfp->depth_y,
				selfp->x1, selfp->y1, selfp->x2, selfp->y2);
				
			fout << buffer;
			fout.close();
		}
	}

	//reset the editor
	memset(&selfp->hardness_bounds, 0, sizeof(struct rect));
	selfp->x1 = NULL;
	selfp->x2 = NULL;
	selfp->y1 = NULL;
	selfp->y2 = NULL;
	selfp->depth_x = NULL;
	selfp->depth_y = NULL;
	
}

void windinkedit_sprite_editor_SetHardness(WindinkeditSpriteEditor* self, int left, int top, int right, int bottom)
{
	//set it edited, and make sure the bounds aren't backwards
	selfp->edited = true;
	if (right < left)
	{
		int temp = left;
		left = right;
		right = temp;
	}

	if (bottom < top)
	{
		int temp = top;
		top = bottom;
		bottom = temp;
	}

	selfp->x1 = left - selfp->x_offset - selfp->depth_x;
	selfp->x2 = right - selfp->x_offset - selfp->depth_x;
	selfp->y1 = top - selfp->y_offset - selfp->depth_y;
	selfp->y2 = bottom - selfp->y_offset - selfp->depth_y;
}

void windinkedit_sprite_editor_SetDepth(WindinkeditSpriteEditor* self, int x, int y)
{
	//set it to edited, and do the math of figuring out the dpeth
	selfp->edited = true;

	int temp = selfp->depth_x - (x - selfp->x_offset);

	selfp->x1 += temp;
	selfp->x2 += temp;

	temp = selfp->depth_y - (y - selfp->y_offset);

	selfp->y2 += temp;
	selfp->y1 += temp;

	selfp->depth_x = x - selfp->x_offset;
	selfp->depth_y = y - selfp->y_offset;
}
