#include "MainFrame.h"
//#include "LeftView.h"
//#include "WinDinkeditView.h"
#include "Engine.h"
//#include "Interface.h"
//#include "OpenDmod.h"

//#include "WinDinkedit.h"


#include "Globals.h"
#include "Engine.h"
#include "windinkedit-map.h"

//#include "resource.h"

CMainFrame* mainWnd;

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

//IMPLEMENT_DYNAMIC_CLASS(CMainFrame, wxFrame)


// enum
//   {
//     ID_NEW_DMOD = wxID_HIGHEST+1
//   };
BEGIN_EVENT_TABLE(CMainFrame, wxFrame)
	//{{AFX_MSG_MAP(CWinDinkeditApp)
	EVT_MENU(XRCID("wxID_EXIT"), CMainFrame::OnExit)
//	EVT_MENU(ID_NEW_DMOD, CMainFrame::OnNewDmod)
	//}}AFX_MSG_MAP
	// Standard file based document commands
// 	EVT_MENU(wxID_NEW, CMainFrame::OnFileNew)
// 	EVT_MENU(wxID_OPEN, CMainFrame::OnFileOpen)
END_EVENT_TABLE()

// BEGIN_EVENT_TABLE(CMainFrame, wxFrame)
// 	//{{AFX_MSG_MAP(CMainFrame)
// 	ON_WM_CREATE()
// 	ON_WM_MOVE()
// 	ON_WM_TIMER()
// 	ON_UPDATE_COMMAND_UI(ID_DMOD_PROPERTIES, OnUpdateDmodProperties)
// 	ON_UPDATE_COMMAND_UI(ID_SAVE_FIX, OnUpdateFileSave)
// 	//}}AFX_MSG_MAP
// 	ON_UPDATE_COMMAND_UI_RANGE(AFX_ID_VIEW_MINIMUM, AFX_ID_VIEW_MAXIMUM, OnUpdateViewStyles)
// 	ON_UPDATE_COMMAND_UI(ID_INDICATOR_MOUSE, OnUpdateMousePos)
// 	ON_UPDATE_COMMAND_UI(ID_INDICATOR_VISION, OnUpdateVision)
// 	ON_UPDATE_COMMAND_UI(ID_INDICATOR_SCR_MATCH, OnUpdateScreenMatch)
// 	ON_UPDATE_COMMAND_UI(ID_INDICATOR_SPR_HARD, OnUpdateSpriteHard)
// 	ON_COMMAND_RANGE(AFX_ID_VIEW_MINIMUM, AFX_ID_VIEW_MAXIMUM, OnViewStyle)
// 	ON_COMMAND(ID_FILE_PLAYGAME, OnFilePlaygame)
// 	ON_COMMAND(ID_SAVE_FIX, OnFileSave)
// 	ON_UPDATE_COMMAND_UI(ID_FILE_PLAYGAME, OnUpdateFilePlaygame)
// END_EVENT_TABLE()

static unsigned int indicators[] =
{
  //	ID_SEPARATOR,           // status line indicator
//	ID_INDICATOR_MOUSE,
//	ID_INDICATOR_VISION,
//	ID_INDICATOR_SCR_MATCH,
//	ID_INDICATOR_SPR_HARD,
//	ID_SEPARATOR,

//	ID_INDICATOR_CAPS,
//	ID_INDICATOR_NUM,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

//CMainFrame::CMainFrame()
CMainFrame::CMainFrame()
{

	mainWnd = this;
//	wxMenu *menuFile = new wxMenu;
	
//	menuFile->Append(wxID_ABOUT, "&About...");
//	menuFile->AppendSeparator();
//	menuFile->Append(wxID_QUIT, "E&xit");
	
//	wxMenuBar *menuBar = new wxMenuBar;
//	menuBar->Append( menuFile, "&File" );
	
//	SetMenuBar(menuBar);	

//	CreateStatusBar(); // TODO: => CProgStatusBar
	SetStatusText(_("Welcome to WinDinkedit!"));
}

CMainFrame::~CMainFrame()
{
//	Game_Shutdown();
}

int CMainFrame::updateMousePos(int screen, int x, int y)
{
  printf("TODO: CMainFrame::updateMousePos(int screen, int x, int y)\n");
// TODO:
// 	int nIndex = m_wndStatusBar.CommandToIndex(ID_INDICATOR_MOUSE);

  char buffer[50];
  sprintf(buffer, "screen: %.3d, x: %.3d, y: %.3d", screen, x, y);

// //	m_wndStatusBar.SetPaneStyle(nIndex, SBPS_NOBORDERS);
// 	m_wndStatusBar.SetPaneText(nIndex, buffer);
  printf("  %s\n", buffer);

	return true;
}

int CMainFrame::updateVision(int vision)
{
	int status_index = 3;
 	wxString buffer;
	buffer.Printf(wxT("vision: %.3d"), vision);
 	SetStatusText(buffer, status_index);
 	return true;
}

int CMainFrame::updateScreenMatch(bool enabled)
{
  printf("TODO: CMainFrame::updateScreenMatch()\n");
// 	int nIndex = m_wndStatusBar.CommandToIndex(ID_INDICATOR_SCR_MATCH);

// 	if (enabled)
// 	{
// 		m_wndStatusBar.SetPaneText(nIndex, "SCR_MATCH");
// 	//	m_wndStatusBar.SetPaneStyle(nIndex, SBPS_NORMAL);
// 	}
// 	else
// 	{
// 		m_wndStatusBar.SetPaneText(nIndex, "");
// 	//	m_wndStatusBar.SetPaneStyle(nIndex, SBPS_DISABLED);
// 	}

	return true;
}

int CMainFrame::updateSpriteHard(bool enabled)
{
  printf("TODO: CMainFrame::updateSpriteHard()\n");
// 	int nIndex = m_wndStatusBar.CommandToIndex(ID_INDICATOR_SPR_HARD);

// 	if (enabled)
// 	{
// 		m_wndStatusBar.SetPaneText(nIndex, "SPR_HARD");
// 	//	m_wndStatusBar.SetPaneStyle(nIndex, SBPS_NORMAL);
// 	}
// 	else
// 	{
// 		m_wndStatusBar.SetPaneText(nIndex, "");
// 	//	m_wndStatusBar.SetPaneStyle(nIndex, SBPS_DISABLED);
// 	}

	return true;
}

// void CMainFrame::OnUpdateMousePos(CCmdUI* pCmdUI)
// {
// 	pCmdUI->Enable();
// }

// void CMainFrame::OnUpdateVision(CCmdUI* pCmdUI)
// {
// 	pCmdUI->Enable();
// }

// void CMainFrame::OnUpdateScreenMatch(CCmdUI* pCmdUI)
// {
// 	pCmdUI->Enable();
// }

// void CMainFrame::OnUpdateSpriteHard(CCmdUI* pCmdUI)
// {
// 	pCmdUI->Enable();
// }

// void CMainFrame::OnTimer(unsigned int nIDEvent)
// {
// 	//kill timer, we will reset it later if needed
// 	KillTimer(nIDEvent);

// 	//what timer got called?
// 	switch (nIDEvent)
// 	{
// 	case TIMER_AUTO_SAVE:
// 		{
// 			if (auto_save_time)
// 			{
// 				//autosave!
// 				current_map->save_dmod();
// 				mainWnd->setStatusText("Dmod Auto-saved..");

// 				SetTimer(TIMER_AUTO_SAVE, auto_save_time * 60000, NULL);
// 			}
// 		} break;
// 	case TIMER_MOUSE_CHECK:
// 		{
// 			//is the mouse still down?
// 			if (KEY_DOWN(VK_LBUTTON) || KEY_DOWN(VK_MBUTTON))
// 			{
// 				SetTimer(TIMER_MOUSE_CHECK, 100, NULL);
// 			} else {
// 				mouseLeftReleased(current_map->x_hover, current_map->y_hover);
// 			}
// 		} break;
// 	default:break; //what timer is that?! Oh screw it, we killed it.
// 	}
// }

// int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
// {
// 	if (wxFrame::OnCreate(lpCreateStruct) == -1)
// 		return -1;
// /*	
// 	if (!m_wndToolBar.CreateEx(this) ||
// 		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
// 	{
// 		TRACE0("Failed to create toolbar\n");
// 		return -1;      // fail to create
// 	}
// 	if (!m_wndDlgBar.Create(this, IDR_MAINFRAME, 
// 		CBRS_ALIGN_TOP, AFX_IDW_DIALOGBAR))
// 	{
// 		TRACE0("Failed to create dialogbar\n");
// 		return -1;		// fail to create
// 	}

// 	if (!m_wndReBar.Create(this) ||
// 		!m_wndReBar.AddBar(&m_wndToolBar) ||
// 		!m_wndReBar.AddBar(&m_wndDlgBar))
// 	{
// 		TRACE0("Failed to create rebar\n");
// 		return -1;      // fail to create
// 	}

// 	if (!m_wndStatusBar.Create(this) ||
// 		!m_wndStatusBar.SetIndicators(indicators,
// 		  sizeof(indicators)/sizeof(unsigned int)))
// 	{
// 		TRACE0("Failed to create status bar\n");
// 		return -1;      // fail to create
// 	}

// 	// TODO: Remove this if you don't want tool tips
// 	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
// 		CBRS_TOOLTIPS | CBRS_FLYBY);
// */

// 	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
// 		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
// 		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
// 	{
// 		TRACE0("Failed to create toolbar\n");
// 		return -1;      // fail to create
// 	}

// 	if (!m_wndStatusBar.Create(this) ||
// 		!m_wndStatusBar.SetIndicators(indicators,
// 		  sizeof(indicators)/sizeof(unsigned int)))
// 	{
// 		TRACE0("Failed to create status bar\n");
// 		return -1;      // fail to create
// 	}

// 	// TODO: Delete these three lines if you don't want the toolbar to
// 	//  be dockable
// 	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
// 	EnableDocking(CBRS_ALIGN_ANY);
// 	DockControlBar(&m_wndToolBar);


// 	updateScreenMatch(false);
// 	updateSpriteHard(false);

// 	return 0;
// }

// /*BOOL*/int CMainFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/,
// 	CCreateContext* pContext)
// {
// 	// create splitter window
// 	if (!m_wndSplitter.CreateStatic(this, 1, 2))
// 		return /*FALSE*/0;

// 	if (!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CLeftView), wxSize(100, 100), pContext) ||
// 		!m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CWinDinkeditView), wxSize(100, 100), pContext))
// 	{
// 		m_wndSplitter.DestroyWindow();
// 		return /*FALSE*/0;
// 	}

// 	return /*TRUE*/1;
// }

// /*BOOL*/int CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
// {
// 	if( !wxFrame::PreCreateWindow(cs) )
// 		return /*FALSE*/0;
// 	// TODO: Modify the Window class or styles here by modifying
// 	//  the CREATESTRUCT cs

// 	return /*TRUE*/1;
// }

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

// CWinDinkeditView* CMainFrame::GetRightPane()
// {
// 	wxWindow* pWnd = m_wndSplitter.GetPane(0, 1);
// 	CWinDinkeditView* pView = DYNAMIC_DOWNCAST(CWinDinkeditView, pWnd);
// 	return pView;
// }

// CLeftView* CMainFrame::GetLeftPane()
// {
// 	wxWindow* pWnd = m_wndSplitter.GetPane(0, 0);
// 	CLeftView* pView = DYNAMIC_DOWNCAST(CLeftView, pWnd);
// 	return pView;
// }

// void CMainFrame::OnMove(int x, int y) 
// {
// 	wxFrame::OnMove(x, y);
	
// 	// TODO: Add your message handler code here
// 	if (current_map)
// 		GetRightPane()->windowMoved(x, y);
// }


// void CMainFrame::OnUpdateDmodProperties(CCmdUI* pCmdUI) 
// {
// 	if (current_map)
// 		pCmdUI->Enable();
// 	else
// 		pCmdUI->Enable(/*FALSE*/0);
// }

// void CMainFrame::OnUpdateFileSave(CCmdUI* pCmdUI) 
// {
// 	if (current_map)
// 		pCmdUI->Enable();
// 	else
// 		pCmdUI->Enable(/*FALSE*/0);
// }

// void CMainFrame::OnFileSave()
// {
// 	if (current_map)
// 	{
// 		BeginWaitCursor();
// 		current_map->save_dmod();
// 		EndWaitCursor();

// 		mainWnd->GetActiveDocument()->SetModifiedFlag(/*FALSE*/0);
// 	}
// }


// void CMainFrame::OnFilePlaygame()
// {
//   int result = wxMessageBox(_("Warning, this will save you d-mod, and run dink, continue?"),
// 														_("WARNING: Save Confirmation"), wxYES_NO);

// 	if (result == wxNO)
// 		return;

// 	current_map->save_dmod();
// 	mainWnd->GetActiveDocument()->SetModifiedFlag(/*FALSE*/0);

// 	char openline[256];
// 	char param[10];
// 	sprintf(openline, "%sdink.exe", main_dink_path.GetData());
// 	sprintf(param, "-game %s", Dmod_Name.GetData());

// 	HINSTANCE NEW = ShellExecute(NULL, "open", openline, param, NULL, SW_SHOWDEFAULT);

// 	if (int(NEW) < 32)
// 	{
// 		char error[256];
// 		sprintf(error, "Error executing %s Error Code: %i", openline, int(NEW));
// 		wxMessageBox(error, "ERROR: Running D-mod", wxOK);
// 	}
// }

// void CMainFrame::OnUpdateFilePlaygame(CCmdUI *pCmdUI)
// {
// 	if (current_map)
// 		pCmdUI->Enable();
// 	else
// 		pCmdUI->Enable(/*FALSE*/0);
// }

// void CMainFrame::OnNewDmod(wxCommandEvent &event)
// {
// 	char dmod_name[256];
	
// 	NewDmodDialog newdlg(NULL);

// 	// give the dialog a pointer to the dmod_name buffer
// 	newdlg.setDmodNamePtr(dmod_name);

// 	int retcode = newdlg.DoModal();

// 	if (retcode == wxOK)
// 	{
// 		OpenDocumentFile(dmod_name);
// 	}
// }

void CMainFrame::OnFileOpen(wxCommandEvent &event)
{
	wxString dmod_name;
	wxString choices[3];
	choices[0] = wxT("dink");
	choices[1] = wxT("island");
	choices[2] = wxT("dinkmines");
	dmod_name = ::wxGetSingleChoice(wxT("Select your D-Mod"), wxT("WinDinkedit"),
																	3, choices, this);
	
	if (dmod_name != wxT(""))
		{
			OpenDocumentFile(dmod_name);
		}
}

wxDocument* CMainFrame::OpenDocumentFile(wxString file_name)
{
// 	char buffer[PATH_MAX];
// 	strcpy(buffer, file_name);
	
// 	Dmod_Name = getDmodName(buffer);

	return NULL;
	//	return wxApp::OpenDocumentFile(Dmod_Name.GetData());
}

void CMainFrame::OnExit(wxCommandEvent &event)
{
  Close();
}
