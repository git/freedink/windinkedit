/**
 * Graphic mode to edit sprite dink.ini properties

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef WINDINKEDIT_SPRITE_EDITOR_H
#define WINDINKEDIT_SPRITE_EDITOR_H

#include <glib-object.h>
#include "SDL.h"
#include "rect.h"

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_SPRITE_EDITOR         (windinkedit_sprite_editor_get_type ())
#define WINDINKEDIT_SPRITE_EDITOR(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_SPRITE_EDITOR, WindinkeditSpriteEditor))
#define WINDINKEDIT_SPRITE_EDITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_SPRITE_EDITOR, WindinkeditSpriteEditorClass))
#define WINDINKEDIT_IS_SPRITE_EDITOR(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_SPRITE_EDITOR))
#define WINDINKEDIT_IS_SPRITE_EDITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_SPRITE_EDITOR))
#define WINDINKEDIT_SPRITE_EDITOR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_SPRITE_EDITOR, WindinkeditSpriteEditorClass))

#ifndef _WINDINKEDIT_TYPEDEF_SPRITE_EDITOR
#define _WINDINKEDIT_TYPEDEF_SPRITE_EDITOR
typedef struct _WindinkeditSpriteEditor WindinkeditSpriteEditor;
#endif
typedef struct _WindinkeditSpriteEditorClass WindinkeditSpriteEditorClass;
typedef struct _WindinkeditSpriteEditorPrivate WindinkeditSpriteEditorPrivate;
  
struct _WindinkeditSpriteEditor {
  GObject parent;
  /*< private >*/
  WindinkeditSpriteEditorPrivate* _priv;
};

struct _WindinkeditSpriteEditorClass {
  GObjectClass parent;
};

/* used by WINDINKEDIT_TYPE_SPRITE_EDITOR */
GType windinkedit_sprite_editor_get_type (void);


/* API. */
void windinkedit_sprite_editor_DrawSprite(WindinkeditSpriteEditor* self);
void windinkedit_sprite_editor_DrawHardness(WindinkeditSpriteEditor* self);
void windinkedit_sprite_editor_Save(WindinkeditSpriteEditor* self);
void windinkedit_sprite_editor_SetHardness(WindinkeditSpriteEditor* self, int left, int top, int right, int bottom);
void windinkedit_sprite_editor_SetDepth(WindinkeditSpriteEditor* self, int x, int y);
  
G_END_DECLS

#endif
