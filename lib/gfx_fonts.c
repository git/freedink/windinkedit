/**
 * Fonts

 * Copyright (C) 2007, 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "gfx_fonts.h"

#include <stdlib.h>
#include "SDL.h"
#include "SDL_ttf.h"
#include "vgasys_fon.h"

#define FONT_SIZE 16

/**
 * This function draws the sent text on the sent surface using color
 * index as the color in the palette
 */
int Draw_Text_GDI(const char *text, int x, int y, int color, SDL_Surface* lpdds)
{
  static TTF_Font *font = NULL;
  if (font == NULL)
    {
      if (TTF_Init() == -1) {
	fprintf(stderr, "TTF_Init: %s\n", TTF_GetError());
	return -1;
      }
      font = TTF_OpenFontRW(SDL_RWFromMem(vgasys_fon, sizeof(vgasys_fon)),
			    1, FONT_SIZE);
      // Note: vgasys.fon has fixed height, so FONT_SIZE is not really
      // taken into account
    }

  //if it's out of bounds, kill it.
  if (x < 0)
    x = 0;
  if (y < 0)
    y = 0;
  
  SDL_Color color_struct;
  SDL_GetRGB(color, lpdds->format,
	     &color_struct.r,
	     &color_struct.g,
	     &color_struct.b);
  SDL_Surface *tmp = TTF_RenderText_Solid(font, text, color_struct);
  SDL_Rect dst;
  dst.x = x;
  dst.y = y;
  dst.w = dst.h = 0;
  SDL_BlitSurface(tmp, NULL, lpdds, &dst);
  
  return 0;
}
