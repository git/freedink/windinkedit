#include "windinkedit-main.h"

#include "Globals.h"
#include "Colors.h"
#include "windinkedit-dmod.h"
#include "windinkedit-graphics-manager.h"
#include "windinkedit-map.h"
#include "windinkedit-minimap.h"
#include "Engine.h" /* Game_Main() */
#include "Modes.h"
#include "Undo.h"
#include "HardTileSelector.h"

#include "SDL.h"

G_BEGIN_DECLS

GType windinkedit_main_get_type (void)
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditMainClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      NULL,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditMain),
      0,      /* n_preallocs */
      NULL    /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditMainType",
				   &info, (GTypeFlags)0);
  }
  return type;
}

/**
 * Static bootstrap method
 */
void windinkedit_init()
{
  printf("windinkedit_init\n");
  /* Grab video buffer initialized with SDL_WINDOWID by the GUI */
  lpddsback = SDL_GetVideoSurface();
  // lpddsback = SDL_SetVideoMode(new_width, new_height, 0, SDL_SWSURFACE | SDL_RESIZABLE);
  // lpddsback = SDL_SetVideoMode(640, 480, 0, SDL_SWSURFACE | SDL_RESIZABLE);

  graphics_manager = WINDINKEDIT_GRAPHICS_MANAGER(g_object_new(WINDINKEDIT_TYPE_GRAPHICS_MANAGER, NULL));

  colorformat = lpddsback->format->BytesPerPixel;
//   gchar* main_dink_path = g_strdup("/downloads/dink");
//   gchar* dink_path      = g_strdup("/downloads/dink/dink");

  Game_Init();	// load config, init colors and auto-save

//   gchar *my_dmod_path = g_strdup("/downloads/dink/dink");
//   WindinkeditDmod* dmod = windinkedit_dmod_new();
//   int retval = windinkedit_dmod_open(dmod, dink_path, my_dmod_path);
//   g_return_if_fail(retval == 1/*true*/);
//   current_dmod = dmod;

//   current_map = windinkedit_dmod_get_map(dmod);
//   windinkedit_map_set_graphics_size(current_map, 640, 480);

//  Game_Main();

//   free(main_dink_path);
//   free(dink_path);
//   free(my_dmod_path);
}

void windinkedit_main_quit()
{
  hard_tile_selector->unloadAllBitmaps();
  delete hard_tile_selector;
//  g_object_unref(sprite_selector);
  delete undo_buffer;
  Game_Shutdown();
}  

void windinkedit_main_rebuild_screen (WindinkeditMain *self)
{
  printf("windinkedit_main_rebuild_screen\n");
  Game_Main();
}

void windinkedit_main_mouse_move (WindinkeditMain *self, int x, int y)
{
  //    mouseMove(x, y);
}

void windinkedit_main_mouse_left_pressed (WindinkeditMain *self, int x, int y)
{
  //    mouseLeftPressed(x, y);
}

// WindinkeditMap* windinkedit_main_get_map(WindinkeditMain *self)
// {
//   return current_map;
// }

// WindinkeditMinimap* windinkedit_main_get_minimap(WindinkeditMain *self)
// {
//   return current_map->minimap;
// }

void windinkedit_main_test_fill(WindinkeditMain *self)
{
  SDL_Rect dst = {100, 100, 200, 200};
  SDL_FillRect(lpddsback, NULL, SDL_MapRGB(lpddsback->format, 0, 0, 255));
}

/**
 * Test displaying SDL surface to a Gtk widget, when one screen
 * surface isn't enough.
 */
void windinkedit_main_test_RGB(WindinkeditMain *self, GdkDrawable *drawable)
{
  /* A GC is necessary, otherwise this break a GDK assertion */
  GdkGC* ignored_gc = gdk_gc_new(drawable);


  /*** Solution 1 ***/
  /* This is inefficient because we need to use an intermediary
     24bit-RGB format. */

  /* gdk_draw_rgb_image uses hardcoded 24bit RGB (in that order),
     which is usually the reverse order than my x86 display */
        Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
        rmask = 0xff000000;
        gmask = 0x00ff0000;
        bmask = 0x0000ff00;
        amask = 0x00000000;
#else
        rmask = 0x000000ff;
        gmask = 0x0000ff00;
        bmask = 0x00ff0000;
        amask = 0x00000000;
#endif
  SDL_Surface* tmp = SDL_CreateRGBSurface(SDL_SWSURFACE, lpddsback->w, lpddsback->h,
					  24, rmask, gmask, bmask, amask);
  SDL_BlitSurface(lpddsback, NULL, tmp, NULL);

//   gdk_draw_rgb_image(drawable, ignored_gc,
// 		     0 /* x */, 0 /* y */, 640, 480,
// 		     GDK_RGB_DITHER_NONE,
// 		     (guchar*)tmp->pixels, tmp->pitch);
  /* Note that gdk_draw_rgb_image can't draw a subrect of the original
     pixels - doing that would require calling it line per line */

  /* 'tmp' could be reused and only recreated on widget resize; for
     now always recreate it */
  SDL_FreeSurface(tmp);


  /*** Solution 2 - better perfs ***/

  /* Another way is to use a GdkImage (GdkPixbuf = client-side
   * canonical 24bit-RGB, GdkImage = client-side with X server depth,
   * GdkPixmap = server-side with X server depth but created only from
   * XBM or XPM,
   * cf. http://library.gnome.org/devel/gdk/2.6/gdk-Drawing-Primitives.html#gdk-drawable-get-image). */

  /* SDL's depth (if auto) and XImage's depth are usually the same */
  int width = -1, height = -1;
  gdk_drawable_get_size(drawable, &width, &height);
  GdkVisual* visual = gdk_drawable_get_visual(drawable);
  GdkImage* image = gdk_image_new(GDK_IMAGE_FASTEST, visual, width, height);
  printf("lpddsback is %d, image is %d, visual is %d, (%dx%d)\n",
	 lpddsback->format->BitsPerPixel, image->bits_per_pixel, gdk_drawable_get_visual(drawable)->depth,
	 width, height);
  SDL_Surface* tmp2 = SDL_CreateRGBSurfaceFrom(image->mem, width, height, image->bits_per_pixel, image->bpl,
					       visual->red_mask, visual->green_mask, visual->blue_mask, 0);

  SDL_BlitSurface(lpddsback, NULL, tmp2, NULL);

  // ~Flip/UpdateRect:
  gdk_draw_image(drawable, ignored_gc, image, 0, 0,
		 0, 0, -1, -1);

  /* tmp2 could be directly used as the destination surface to avoid
     blitting each time */
  SDL_FreeSurface(tmp2);
  g_object_unref(image);

  g_object_unref(ignored_gc);
}

void windinkedit_main_map_centerOnScreen(WindinkeditMain *self, WindinkeditMap *current_map, int screen_maths)
{
  int col = screen_maths % MAP_COLUMNS;
  int row = screen_maths / MAP_COLUMNS;

  int x_location = col * (SCREEN_WIDTH + screen_gap) + SCREEN_WIDTH / 2 - (current_map->window_width / 2);
  int y_location = row * (SCREEN_HEIGHT + screen_gap) + SCREEN_HEIGHT / 2 - (current_map->window_height / 2);
  windinkedit_map_updateMapPosition(current_map, x_location, y_location);
}

SDL_Surface* windinkedit_main_map_GetVideoSurface(WindinkeditMain* self)
{
  return SDL_GetVideoSurface();
}

G_END_DECLS
