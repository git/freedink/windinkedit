#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
// SpriteLibraryEntry.cpp : implementation file
//


#include "SpriteLibraryEntry.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SpriteLibraryEntry dialog


SpriteLibraryEntry::SpriteLibraryEntry(wxWindow* pParent /*=NULL*/)
	: wxDialog(SpriteLibraryEntry::IDD, pParent)
{
	//{{AFX_DATA_INIT(SpriteLibraryEntry)
	m_sprite_name = _T("");
	//}}AFX_DATA_INIT
}


void SpriteLibraryEntry::DoDataExchange(wxValidator* pDX)
{
	wxDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SpriteLibraryEntry)
	DDX_Text(pDX, IDC_Name, m_sprite_name);
	DDV_MaxChars(pDX, m_sprite_name, 19);
	//}}AFX_DATA_MAP
}


BEGIN_EVENT_TABLE(SpriteLibraryEntry, wxDialog)
	//{{AFX_MSG_MAP(SpriteLibraryEntry)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_EVENT_TABLE()

/////////////////////////////////////////////////////////////////////////////
// SpriteLibraryEntry message handlers
