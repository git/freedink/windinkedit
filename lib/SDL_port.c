/**
 * SDL wrappers for porting DX code quickly

 * Copyright (C) 2003  Shawn Betts
 * Copyright (C) 2007, 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "math.h"
#include "SDL_rotozoom.h"
#include "SDL_port.h"
#include "rect.h"

void
CopyRect2(SDL_Rect* dest, struct rect* src)
{
  dest->x = src->left;
  dest->y = src->top;
  dest->w = src->right - src->left;
  dest->h = src->bottom - src->top;
}

void
Blt (SDL_Surface* dst, struct rect* dst_rect, SDL_Surface* src, struct rect* src_rect)
{
  SDL_Rect *dst_r = NULL, *src_r = NULL;
//   printf("Blt: blit(%p -> %p)", src, dst);
//   printf(" ");
//   if (src_rect != NULL)
//     printf("[(%d,%d)x(%d,%d)]",
// 	   src_rect->left, src_rect->top,
// 	   src_rect->right-src_rect->left, src_rect->bottom-src_rect->top);
//   else
//     printf("[all]");
//   printf("->");
//   if (dst_rect != NULL)
//     printf("[(%d,%d)x(%d,%d)]",
// 	   dst_rect->left, dst_rect->top,
// 	   dst_rect->right-dst_rect->left, dst_rect->bottom-dst_rect->top);
//   else
//     printf("[all]");
//   printf("\n");

  if (dst_rect != NULL)
    {
      dst_r = (SDL_Rect *) malloc (sizeof (SDL_Rect));
      CopyRect2 (dst_r, dst_rect);
    }

  if (src_rect != NULL)
    {
      src_r = (SDL_Rect *) malloc (sizeof (SDL_Rect));
      CopyRect2 (src_r, src_rect);
    }

  SDL_BlitSurface (src, src_r, dst, dst_r);
//   printf("SDL_BlitSurface(%p, ", src);
//   if (src_r != NULL)
//     printf("(%d,%d:%d,%d)", src_r->x, src_r->y, src_r->w, src_r->h);
//   else
//     printf("(all)");
//   printf(", ");
//   printf("%p, ", dst);
//   if (dst_r != NULL)
//     printf("(%d,%d)", dst_r->x, dst_r->y);
//   else
//     printf("(all)");
//   printf("\n");

  if (dst_r)
    free (dst_r);
  if (src_r)
    free (src_r);
}

void BltStretch(SDL_Surface* dst, struct rect* dst_rect, SDL_Surface* src, struct rect* src_rect)
{
  SDL_Rect src_r, dst_r;
  SDL_Surface *scaled;
  double sx, sy;

  if (dst_rect != NULL)
    CopyRect2 (&dst_r, dst_rect);
  else
    {
      dst_r.x = dst_r.y = 0;
      dst_r.w = dst->w;
      dst_r.h = dst->h;
    }

  if (src_rect != NULL)
    CopyRect2 (&src_r, src_rect);
  else
    {
      src_r.x = src_r.y = 0;
      src_r.w = src->w;
      src_r.h = src->h;
    }

  sx = 1.0 * dst_r.w / src_r.w;
  sy = 1.0 * dst_r.h / src_r.h;
  /* In principle, double's are precised up to 15 decimal
     digits */
  if (fabs(sx-1) > 1e-10 || fabs(sy-1) > 1e-10)
    {
      scaled = zoomSurface(src, sx, sy, SMOOTHING_OFF);
      /* Disable transparency if it wasn't active in the source surface
	 (SDL_gfx bug, report submitted to the author) */
      if ((src->flags & SDL_SRCCOLORKEY) == 0)
	SDL_SetColorKey(scaled, 0, 0);
      src_r.x = (int) round(src_r.x * sx);
      src_r.y = (int) round(src_r.y * sy);
      src_r.w = (int) round(src_r.w * sx);
      src_r.h = (int) round(src_r.h * sy);
      if (SDL_BlitSurface(scaled, &src_r, dst, &dst_r) < 0)
	printf("Error blitting surface: %s\n", SDL_GetError());
      SDL_FreeSurface(scaled);
    }
  else
    {
      /* No scaling */
      if (SDL_BlitSurface(src, &src_r, dst, &dst_r) < 0) {
	printf("Error blitting surface: %s\n", SDL_GetError());
      }
    }
}


void fill_rect(SDL_Surface* surface, struct rect* box, int color)
{
  SDL_Rect b;

  if (box)
    {
      CopyRect2 (&b, box);
      SDL_FillRect (surface, &b, color);
    }
  else
    {
      SDL_FillRect (surface, NULL, color);
    }
}

/**
 * Get a surface sharing the same pixels than 'surface', but with a
 * different origin and dimensions. You can free it normally with
 * SDL_FreeSurface() without affecting the parent surface.
 */
SDL_Surface* SubSurface(SDL_Surface* surface, SDL_Rect* subrect)
{
  SDL_PixelFormat *format = surface->format;
  int offset = subrect->x * format->BytesPerPixel
    + subrect->y * surface->pitch;
  char* pixels = (char *)surface->pixels + offset;


  if (SDL_MUSTLOCK(surface))
    SDL_LockSurface(surface);

  /* Trick: */
  SDL_Surface* subsurface =
    SDL_CreateRGBSurfaceFrom(pixels, subrect->w, subrect->h,
			     format->BitsPerPixel, surface->pitch,
			     format->Rmask, format->Gmask,
			     format->Bmask, format->Amask);
  /* SDL_CreateRGBSurfaceFrom sets the SDL_PREALLOC flag, so the
     surface can be freed without affecting 'pixels'. */

  if (SDL_MUSTLOCK(surface))
    SDL_UnlockSurface(surface);
}
