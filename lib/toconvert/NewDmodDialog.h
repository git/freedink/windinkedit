#if !defined(AFX_NEWDMODDIALOG_H__87836A94_9B55_44BF_ABB1_08195F61C8A6__INCLUDED_)
#define AFX_NEWDMODDIALOG_H__87836A94_9B55_44BF_ABB1_08195F61C8A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewDmodDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// NewDmodDialog dialog

class NewDmodDialog : public wxDialog
{
// Construction
public:
	NewDmodDialog(wxWindow* pParent = NULL);   // standard constructor
	void setDmodNamePtr(char *buffer);

// Dialog Data
	//{{AFX_DATA(NewDmodDialog)
	enum { IDD = IDD_NEW_DMOD };
	wxString	m_description;
	wxString	m_directory;
	wxString	m_dmod_name;
	wxString	m_author;
	wxString	m_email_website;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(NewDmodDialog)
	protected:
	virtual void DoDataExchange(wxValidator* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(NewDmodDialog)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_EVENT_TABLE()

private:
	char *dmod_name;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWDMODDIALOG_H__87836A94_9B55_44BF_ABB1_08195F61C8A6__INCLUDED_)
