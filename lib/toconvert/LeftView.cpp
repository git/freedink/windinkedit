#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
// LeftView.cpp : implementation of the CLeftView class
//


#include "WinDinkeditDoc.h"
#include "LeftView.h"
#include "Globals.h"
#include "windinkedit-map.h"
// #include "Interface.h"
// #include "SpriteLibrary.h"
// #include "SpriteLibraryEntry.h"
// #include "WinDinkeditView.h"


IMPLEMENT_DYNAMIC_CLASS(CLeftView, wxView)

BEGIN_EVENT_TABLE(CLeftView, wxView)
	//{{AFX_MSG_MAP(CLeftView)
// 	ON_WM_LBUTTONDBLCLK()
// 	ON_WM_KEYUP()
// 	ON_WM_KEYDOWN()
// 	ON_COMMAND(IDM_REFRESH_LIST, OnRefreshList)
// 	ON_WM_RBUTTONDOWN()
// 	ON_COMMAND(IDM_DELETE_SPRITE_ENTRY, OnDeleteSpriteEntry)
// 	ON_COMMAND(IDM_RENAME_SPRITE_ENTRY, OnRenameSpriteEntry)
	//}}AFX_MSG_MAP
END_EVENT_TABLE()

CLeftView::CLeftView()
{
}

bool CLeftView::OnCreate(wxDocument* doc, long flags)
{
	return true;
}

void CLeftView::OnDraw(wxDC* dc)
{

}

void CLeftView::OnUpdate(wxView* sender, wxObject* hint)
{
}

bool CLeftView::OnClose(bool deleteWindow)
{
//     if (!GetDocument()->Close())
//         return false;
    return true;
}

// void CLeftView::UpdateVisionTree()
// {
// 	wxTreeCtrl& tree = GetTreeCtrl();

// 	HTREEITEM vision_num;

// 	// first empty the subtree out
// 	while((vision_num = tree.GetChildItem(visions_root)) != NULL)
// 	{
// 		tree.DeleteItem(vision_num);
// 	}

// 	char buffer[5];

// 	// always add 0
// 	sprintf(buffer, "%d", 0);
// 	tree.InsertItem(buffer, visions_root);

// 	// add back the rest
// 	for (int i = 1; i < VISIONS_TRACKED; i++)
// 	{
// 		if (vision_used[i] != 0)
// 		{
// 			sprintf(buffer, "%d", i);
// 			tree.InsertItem(buffer, visions_root);
// 		}
// 	}
// }

// void CLeftView::OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags) 
// {
// 	// TODO: Add your message handler code here and/or call default
// 	keyPressed((unsigned char)nChar);
// }

// void CLeftView::OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags) 
// {
// 	// TODO: Add your message handler code here and/or call default
// 	keyReleased((unsigned char)nChar);
// }

// void CLeftView::UpdateSpriteLibraryTree()
// {
// 	wxTreeCtrl& tree = GetTreeCtrl();

// 	HTREEITEM sprite_node;

// 	// first empty the subtree out
// 	while((sprite_node = tree.GetChildItem(sprite_library_root)) != NULL)
// 	{
// 		tree.DeleteItem(sprite_node);
// 	}

// 	char buffer[MAX_SPRITE_NAME_LENGTH];

// 	current_map->sprite_library.getNextInitialize();

// 	while (current_map->sprite_library.getNext(buffer))
// 	{
// 		tree.InsertItem(buffer, sprite_library_root);
// 	}
// 	tree.SortChildren(sprite_library_root);
// }

// #define MAX_C_FILENAME_LENGTH 20

// void CLeftView::UpdateScriptTree()
// {
// 	HANDLE hFind;
// 	WIN32_FIND_DATA dataFind;
// 	/*BOOL*/int bMoreFiles = /*TRUE*/1;

// 	wxTreeCtrl& tree = GetTreeCtrl();

// 	char full_path[250];
// 	sprintf(full_path, "%sstory\\*.c", current_map->dmod_path);

// 	char filename[MAX_C_FILENAME_LENGTH];

// 	hFind = FindFirstFile(full_path, &dataFind);

// 	// get all the *.c files in the directory
// 	while(hFind != INVALID_HANDLE_VALUE && bMoreFiles == /*TRUE*/1)
// 	{
// 		int length = strlen(dataFind.cFileName) - 2;

// 		if (length >= MAX_C_FILENAME_LENGTH)
// 			length = MAX_C_FILENAME_LENGTH - 1;

// 		strncpy(filename, dataFind.cFileName, length);
// 		filename[length] = 0;

// 		// make lower
// 		for (int i = 1; i < MAX_C_FILENAME_LENGTH - 1; i++)
// 		{
// 			filename[i] = tolower(filename[i]);
// 		}

// 		tree.InsertItem(filename, script_root);

// 		bMoreFiles = FindNextFile(hFind, &dataFind);
// 	}

// 	FindClose(hFind);
// }

// int CLeftView::PopulateTree()
// {
// 	wxTreeCtrl& tree = GetTreeCtrl();

// 	tree.DeleteAllItems();

// 	visions_root = tree.InsertItem("Visions In Use");
// 	script_root = tree.InsertItem("Scripts");
// 	sprite_library_root = tree.InsertItem("Sprite Library");

// 	SetWindowLong(tree, GWL_STYLE, 
// 		WS_VISIBLE | WS_CHILD | TVS_HASLINES | WS_EX_CLIENTEDGE | TVS_SINGLEEXPAND);
// 	// TVS_HASBUTTONS | TVS_LINESATROOT

// 	UpdateScriptTree();
// 	UpdateSpriteLibraryTree();
// 	UpdateVisionTree();
// //	tree.Expand(visions_root, TVE_EXPAND);
// 	tree.SelectItem(visions_root);

// 	tree.SortChildren(script_root);

// 	return true;
// }

// void CLeftView::OnLButtonDblClk(unsigned int nFlags, wxPoint point) 
// {
// 	if (!current_map)
// 		return;

// 	// need to get selected item
// 	wxTreeCtrl& tree = GetTreeCtrl();
// 	HTREEITEM filename = tree.GetSelectedItem();

// 	if (filename == NULL)
// 		return;

// 	char buffer[250];
	
// 	strcpy(buffer, tree.GetItemText(filename).operator char*());

// 	if (script_root == tree.GetParentItem(filename))
// 	{
// 		editScript(buffer);
// 	}
// 	else if (visions_root == tree.GetParentItem(filename))
// 	{
// 		setVision(atoi(buffer));
// 	}
// 	else if (sprite_library_root == tree.GetParentItem(filename))
// 	{
// 		current_map->sprite_library.getSprite(buffer);
// 	}
	
// 	CTreeView::OnLButtonDblClk(nFlags, point);
// }

/**
 * User right-clicked on the list and click on 'refresh' (the only
 * context menu entry)
 */
// void CLeftView::OnRefreshList() 
// {
// 	PopulateTree();
// }

// void CLeftView::OnRButtonDown(unsigned int nFlags, wxPoint point) 
// {
// 	if (!current_map)
// 		return;

// 	struct rect location;

// 	GetWindowRect(&location);

// 	// need to get selected item
// 	wxTreeCtrl& tree = GetTreeCtrl();
// 	HTREEITEM filename = tree.GetSelectedItem();

// 	if (filename == NULL)
// 		return;

// 	char buffer[250];
	
// 	strcpy(buffer, tree.GetItemText(filename).operator char*());

// 	if (sprite_library_root == tree.GetParentItem(filename))
// 	{
// 		CMenu menuPopup;
// 		menuPopup.LoadMenu(IDR_SPRITE_ENTRY);
// 		menuPopup.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN, point.x + location.left, point.y + location.top, this);

// 	}
// 	else
// 	{
// 		CMenu menuPopup;
// 		menuPopup.LoadMenu(IDR_SIDEBAR);
// 		menuPopup.GetSubMenu(0)->TrackPopupMenu(TPM_LEFTALIGN, point.x + location.left, point.y + location.top, this);
// 	}

	
// 	CTreeView::OnRButtonDown(nFlags, point);
// }

// void CLeftView::OnDeleteSpriteEntry() 
// {
// 	// need to get selected item
// 	wxTreeCtrl& tree = GetTreeCtrl();
// 	HTREEITEM filename = tree.GetSelectedItem();

// 	if (filename == NULL)
// 		return;

// 	char buffer[250];
	
// 	strcpy(buffer, tree.GetItemText(filename).operator char*());

// 	current_map->sprite_library.removeSprite(buffer);	
// }

// void CLeftView::OnRenameSpriteEntry() 
// {
// 	// TODO: Add your command handler code here
// 	// need to get selected item
// 	wxTreeCtrl& tree = GetTreeCtrl();
// 	HTREEITEM filename = tree.GetSelectedItem();

// 	if (filename == NULL)
// 		return;

// 	char old_name[250];
	
// 	strcpy(old_name, tree.GetItemText(filename).operator char*());


// 	// create the dialog box
// 	SpriteLibraryEntry newdlg(this);

// 	newdlg.m_sprite_name = old_name;

// 	// set the title of the dialog box
// 	char title_buffer[300];
// 	sprintf(title_buffer, "Rename sprite entry %s", old_name);
// 	//newdlg.SetWindowText(title_buffer);

// 	// now show the dialog box
// 	mainWnd->GetRightPane()->gainFocus();
// 	int retcode = newdlg.DoModal();

// 	// check if ok or cancel was pressed
// 	if (retcode == wxOK)
// 	{
// 		char new_name[MAX_SPRITE_NAME_LENGTH];
// 		strcpy(new_name, newdlg.m_sprite_name.operator char*());

// 		current_map->sprite_library.renameSprite(old_name, new_name);
// 	}
// }
