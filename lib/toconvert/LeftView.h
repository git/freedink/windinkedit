#ifndef _LEFTVIEW_H
#define _LEFTVIEW_H

class CWinDinkeditDoc;

class CLeftView : public wxView
{
protected: // create from serialization only
	CLeftView();
	DECLARE_DYNAMIC_CLASS(CLeftView)

public:
	bool OnCreate(wxDocument* doc, long flags);
	void OnDraw(wxDC* dc);
	void OnUpdate(wxView* sender, wxObject* hint);
	bool OnClose(bool deleteWindow);

// Attributes
public:
	CWinDinkeditDoc* GetDocument();
/* 	int PopulateTree(); */
/* 	void UpdateVisionTree(); */
/* 	void UpdateSpriteLibraryTree(); */
/* 	void UpdateScriptTree(); */

private:
/* 	HTREEITEM visions_root; */
/* 	HTREEITEM script_root; */
/* 	HTREEITEM sprite_library_root; */

// Generated message map functions
protected:
	//{{AFX_MSG(CLeftView)
/* 	void OnLButtonDblClk(unsigned int nFlags, wxPoint point); */
/* 	void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags); */
/* 	void OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags); */
/* 	void OnRefreshList(); */
/* 	void OnRButtonDown(unsigned int nFlags, wxPoint point); */
/* 	void OnDeleteSpriteEntry(); */
/* 	void OnRenameSpriteEntry(); */
	//}}AFX_MSG
	DECLARE_EVENT_TABLE()
};

#endif
