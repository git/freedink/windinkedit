#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>

#include "WinDinkedit.h"

#include "MainFrame.h"
#include "WinDinkeditDoc.h"
#include "LeftView.h"

#include "Globals.h"
#include "Colors.h"
#include "windinkedit-map.h"
// #include "Common.h"
// #include "NewDmodDialog.h"

#include "OpenDmod.h"
#include "MainFrame.h"

#include "SDL.h"
#include "SDL_image.h"


#include "Engine.h"
#include "Minimap.h"
#include "Interface.h"


bool CWinDinkeditApp::OnInit()
{
  // Mainly for make distcheck
  if (argc == 2)
    {
      wxString arg = argv[1];
      if (arg.Cmp(_T("--help")) == 0)
	{
	  wxString progname = argv[0];
	  printf("Usage: %s [dmod]\n", (const char*)progname.fn_str());
	  printf("\n");
	  printf("If dmod is given, the DMod will be opened.\n");
	  printf("\n");
	  /* printf ("Type 'info freedink' for more information\n"); */
	  printf("Report bugs to %s.\n", PACKAGE_BUGREPORT);
	  exit(EXIT_SUCCESS); // don't 'return', otherwise wxWidgets will wait forever for a GUI to appear
	}
      else if (arg.Cmp(_T("--version")) == 0)
	{
	  printf ("%s %s\n", PACKAGE_NAME, VERSION);
	  printf ("Copyright (C) 2006, 2007, 2008 by contributors\n");
	  printf ("License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n");
	  printf ("This is free software: you are free to change and redistribute it.\n");
	  printf ("There is NO WARRANTY, to the extent permitted by law.\n");
	  exit(EXIT_SUCCESS); // don't 'return', otherwise wxWidgets will wait forever for a GUI to appear
	}
    }

  gchar app_path = g_get_current_dir();

	// get the dink directory path

	// first check in the dinksmallwood.ini file
//	if (readDinksmallwoodIni(app_path) == false)
//	{
//		for (int i = app_path.Len() - 2; i > 0; i--)
//		{
//			if (app_path.GetChar(i) == wxT('/'))
//			{
//				break;
//			}
//			app_path.Truncate(i);
//		}
//	}

	dink_path = main_dink_path = app_path;
	main_dink_path = g_strdup(app_path);
	dink_path = g_strdup(app_path);
//	dink_path += wxT("dink/");
	g_free(dink_path);
	dink_path = "/downloads/dink/dink/";


	// check to make sure we have the right directory
	wxString buffer;
//	buffer = app_path + wxT("dink/Tiles");
	buffer = wxT("/usr/share/dink/dink/Tiles");

	while (!wxFileName::DirExists(buffer))
	{
	  ::wxMessageBox(_("The Dink reference directory cannot be located."
			   " Please select it (it contains among others 'dink/Tiles' and 'dink/Graphics'."));
		// failed to open dink.exe, path is wrong
		main_dink_path = ::wxDirSelector();

		if (main_dink_path.empty())
		{
			::wxMessageBox(_("Please run the dink game to restore the path."), _("Error"));
			exit(1);
			break;
		}
			
// TODO:
//	 writeDinksmallwoodIni(buffer);

		if (main_dink_path.Last() != wxT('/'))
			main_dink_path.Append(wxT('/'));
		
		app_path = main_dink_path;
		
		dink_path = main_dink_path;
		dink_path.Append(wxT("dink/"));
		
		buffer = app_path + wxT("dink/Tiles");
	}




	
	/* SDL */
	/* Init 'light' SDL */
	if (SDL_Init(SDL_INIT_TIMER) == -1)
		{
			fprintf(stderr, "SDL_Init: %s\n", SDL_GetError());
			return 0;
		}
	// putenv("SDL_VIDEODRIVER=dummy");
	lpddsback = SDL_SetVideoMode(640, 480, 0, SDL_SWSURFACE | SDL_RESIZABLE);
	mapRect.left = mapRect.top = 0;
	mapRect.right = 640;
	mapRect.bottom = 480;
	
	colorformat = lpddsback->format->BytesPerPixel;
	if (!(colorformat == 2 || colorformat == 3 || colorformat == 4))
		{
			::wxMessageBox(wxT("Invalid Color Depth, please use either 16, 24, or 32 bit color depth."),
				       wxT("Error: Invalid Color Depth"));
			return false;
		}
		
	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
// TODO:
// (check wxConfig)
//	SetRegistryKey(wxT("WinDinkedit"));

// TODO:
//	LoadStdProfileSettings();  // Load standard INI file options (including MRU (ie most recently used files))




// 	// Register the application's document templates.  Document templates
// 	//  serve as the connection between documents, frame windows and views.

// 	m_docManager = new wxDocManager;
// 	m_docManager->SetMaxDocsOpen(1); // required?

//  	wxDocTemplate* pDocTemplate;
//  	pDocTemplate = new wxDocTemplate(
// 		m_docManager, wxT("D-Mod"), wxT("*"), wxT(""), wxT(""), wxT("Name-for-Doc"), wxT("Name-for-View"),
//  		CLASSINFO(CWinDinkeditDoc),
//  		CLASSINFO(CLeftView));



	// Parse command line for standard shell commands, DDE, file open
// TODO:
//	CCommandLineInfo cmdInfo;
//	ParseCommandLine(cmdInfo);

// TODO
//	bool open_file = false;
//	if (cmdInfo.m_nShellCommand == CCommandLineInfo::FileOpen)
//	{
//		open_file = true;
//		strcpy(buffer, cmdInfo.m_strFileName); 
//	}
//
//	cmdInfo.m_nShellCommand = CCommandLineInfo::FileNew;
//
//	// Dispatch commands specified on the command line
//	if (!ProcessShellCommand(cmdInfo))
//		return /*FALSE*/0;

//	// The one and only window has been initialized, so show and update it.
//	m_pMainWnd->ShowWindow(SW_SHOW);
//	m_pMainWnd->UpdateWindow();




// 	wxXmlResource::Get()->InitAllHandlers();
// 	//// Note: loading the first resource takes 1-2 seconds :/
// 	//InitOpenDmod_Base();
// 	InitMainFrame_Base();



// 	CMainFrame* frame = new CMainFrame();
//	wxFrame* frame = new wxFrame(NULL, -1, wxT("Hello World"), wxPoint(50,50), wxSize(450,340) );
//	OpenDmod* frame = new OpenDmod();
// 	frame->Centre(wxBOTH);
// 	frame->Show(true);
// 	SetTopWindow(frame);

	//CMainFrame* frame2 = new CMainFrame();
 	//frame2->Show(true);

	//wxDocParentFrame *main_frame = new wxDocParentFrame(m_docManager, (wxFrame *) NULL, wxID_ANY, _T("WinDinkedit"));

// 	wxString choices[3];
// 	choices[0] = wxT("dink");
// 	choices[1] = wxT("island");
// 	choices[2] = wxT("dinkmines");
// 	//::wxMessageBox(::wxGetSingleChoice(wxT("Select your D-Mod"), wxT("Myy Title"), 3, choices, frame));

//	if (open_file)
//	{
//		OpenDocumentFile(buffer);


	//m_docManager->CreateDocument(wxT("dink"), wxDOC_NEW);
	current_map = new Map(dmod_path);
	current_map->finishLoading();
// 	mainWnd = frame;
	//SDL_BlitSurface(current_map->minimap->image, NULL, lpddsback, NULL);
	//SDL_BlitSurface(IMG_Load("artwork.bmp"), NULL, lpddsback, NULL);
	//Game_Main();
	printf("current_map->render_state = %d\n", current_map->render_state);
	//SDL_Flip(lpddsprimary);
	SDL_Event event;
	
	int quit = 0;
	// Produce multiple key events when it's left pressed (e.g. arrow keys)
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
	while (!quit && SDL_WaitEvent(&event))
	  {
	    switch(event.type)
	      {
	      case SDL_KEYDOWN:
		printf("Key pressed\n");
		if (event.key.keysym.sym == 'q' || event.key.keysym.sym == SDLK_ESCAPE)
		  quit = 1;
		else
		  keyPressed(event.key.keysym.sym);
		break;

	      case SDL_KEYUP:
		keyReleased(event.key.keysym.sym);
		break;

	      case SDL_MOUSEMOTION:
		// There can be a lot of mouse move events. To prevent
		// them from piling up, we only take the latest one
		// and discard the others. When you move a sprite and
		// events are piling up, the sprite will eventually
		// follow your mouse with a 1-2s delay.  Maybe this is
		// only interesting in sprite-moving situations, when
		// only the last mouse position counts: it might be
		// desastrous in hardness-drawing situations, where
		// all the successive mouse positions do count.
		while (SDL_PeepEvents(&event, 1, SDL_GETEVENT,
				      SDL_EVENTMASK(SDL_MOUSEMOTION)) > 0);
		mouseMove(event.motion.x, event.motion.y);
		break;
		
	      case SDL_MOUSEBUTTONDOWN:
	      case SDL_MOUSEBUTTONUP:
		printf("mouse(%d,%d)\n", event.button.x, event.button.y);
		if (event.button.button == SDL_BUTTON_LEFT)
		  {
		    if (event.button.type == SDL_MOUSEBUTTONDOWN)
		      mouseLeftPressed(event.button.x, event.button.y);
		    else if (event.button.type == SDL_MOUSEBUTTONUP)
		      mouseLeftReleased(event.button.x, event.button.y);
		  }
		else if (event.button.button == SDL_BUTTON_RIGHT)
		  {
		    // TODO: pass a WinDinkeditView
		    // if (button->type == SDL_MOUSEBUTTONDOWN)
		    // mouseRightPressed(button->x, button->y, WDEV);
		    // else if (button->type == SDL_MOUSEBUTTONUP)
		    // mouseRightReleased(button->x, button->y, WDEV);
		  }
		// TODO: double click
		break;
		
	      case SDL_VIDEOEXPOSE:
		printf("ExposeEvent\n");
		break;
		
	      case SDL_VIDEORESIZE:
		gui_OnSize(event.resize.w, event.resize.h);
		break;
		
	      case SDL_QUIT:
		quit = 1;
		break;
	      }
	    Game_Main();
	  }
	frame->Close(); // main window - kill wxWidgets

//	}

	return true;
}

/**
 * Cleanup our app before wxWidgets shutdowns
 */
int CWinDinkeditApp::OnExit()
{
	delete m_docManager;

	SDL_Quit();

	return 0;
}

IMPLEMENT_APP(CWinDinkeditApp)
