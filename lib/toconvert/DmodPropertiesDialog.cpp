#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
// DmodPropertiesDialog.cpp : implementation file
//


#include "WinDinkedit.h"
#include "DmodPropertiesDialog.h"
#include "Common.h"
#include "windinkedit-map.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DmodPropertiesDialog dialog


DmodPropertiesDialog::DmodPropertiesDialog(wxWindow* pParent /*=NULL*/)
	: wxDialog(DmodPropertiesDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(DmodPropertiesDialog)
	m_author = _T("");
	m_description = _T("");
	m_dmod_title = _T("");
	m_email_website = _T("");
	//}}AFX_DATA_INIT

	readDmodDiz(current_map->dmod_path.GetData(), m_dmod_title, m_author, m_email_website, m_description);
}


void DmodPropertiesDialog::DoDataExchange(wxValidator* pDX)
{
	wxDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DmodPropertiesDialog)
	DDX_Text(pDX, IDC_AUTHOR, m_author);
	DDX_Text(pDX, IDC_DMOD_DESCRIPTION, m_description);
	DDX_Text(pDX, IDC_DMOD_TITLE, m_dmod_title);
	DDX_Text(pDX, IDC_EMAIL_WEBSITE, m_email_website);
	//}}AFX_DATA_MAP
}


BEGIN_EVENT_TABLE(DmodPropertiesDialog, wxDialog)
	//{{AFX_MSG_MAP(DmodPropertiesDialog)
	//}}AFX_MSG_MAP
END_EVENT_TABLE()

/////////////////////////////////////////////////////////////////////////////
// DmodPropertiesDialog message handlers

void DmodPropertiesDialog::OnOK() 
{
	UpdateData();
	
	writeDmodDiz(current_map->dmod_path.GetData(), m_dmod_title.GetData(), 
		m_author.GetData(), m_email_website.GetData(), m_description.GetData());
	
	wxDialog::OnOK();
}
