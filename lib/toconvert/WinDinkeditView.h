#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "WinDinkedit.h"
#include "windinkedit-sprite.h"

#ifndef _WINDINKEDITVIEW_H
#define _WINDINKEDITVIEW_H

class CWinDinkeditDoc;

class CWinDinkeditView : public wxView
{
public:
	void displaySpriteMenu(WindinkeditSprite* sprite, int screen_num, int sprite_num, int x, int y);
	void displayScreenMenu(int screen, int x, int y);
	void gainFocus();

protected: // create from serialization only
	CWinDinkeditView();
	DECLARE_DYNAMIC_CLASS(wxView)

// Attributes
public:
	CWinDinkeditDoc* GetDocument();
	int windowMoved(int x, int y);

// Operations
public:
	void OnAutoUpdateMinimap(wxCommandEvent &event);

// Implementation
public:
	virtual ~CWinDinkeditView();

public:
	bool OnCreate(wxDocument* doc, long flags);
	void OnDraw(wxDC* dc);
	void OnUpdate(wxView* sender, wxObject* hint);
	bool OnClose(bool deleteWindow);

// Generated message map functions
public:
	//{{AFX_MSG(CWinDinkeditView)
	void OnLButtonDown(unsigned int nFlags, wxPoint point);
	void OnKeyDown(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	void OnSize(unsigned int nType, int cx, int cy);
	void OnMouseMove(unsigned int nFlags, wxPoint point);
	void OnKeyUp(unsigned int nChar, unsigned int nRepCnt, unsigned int nFlags);
	void OnLButtonUp(unsigned int nFlags, wxPoint point);
	void OnMove(int x, int y);
	void OnLButtonDblClk(unsigned int nFlags, wxPoint point);
	void OnScreenProperties();
	void OnDeleteScreen();
	void OnSpriteProperties();
	void OnVision();
	void OnNewScreen();
	void OnRButtonDown(unsigned int nFlags, wxPoint point);
	void OnViewMinimap();
	void OnViewSpriteMode();
	void OnViewTileMode();
	void OnViewTileset1();
	void OnViewTileset10();
	void OnViewTileset11();
	void OnViewTileset3();
	void OnViewTileset4();
	void OnViewTileset5();
	void OnViewTileset6();
	void OnViewTileset7();
	void OnViewTileset8();
	void OnViewTileset9();
	void OnViewTileset2();
	void OnScreenMatch();
	void OnPaint();
	void OnSpriteHardness();
	void OnViewHardboxMode();
	void OnSetWarpBegin();
	void OnSetWarpEnd();
	void OnImportScreen();
	void OnAutoUpdateMinimap();
	void OnScreenShot();
	void OnStoreSprite();
	void OnFfcreate();
	void OnCopyScreen();
	void OnPasteScreen();
	void OnCompressScripts();
	void OnOptions();
	void OnEditUndo();
	void OnEditRedo();
	void OnRevertHardTile();
	void OnTransformHardnessNormal();
	void OnTransformHardnessLow();
	void OnTransformHardnessUnknown();
	void OnSetDefaultHardTile();
	void OnEditHardTile();
	//}}AFX_MSG
	DECLARE_EVENT_TABLE()
};

#endif
