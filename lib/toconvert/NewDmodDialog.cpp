#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
// NewDmodDialog.cpp : implementation file
//


#include "WinDinkedit.h"
#include "NewDmodDialog.h"
#include "Common.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// NewDmodDialog dialog


NewDmodDialog::NewDmodDialog(wxWindow* pParent /*=NULL*/)
	: wxDialog(NewDmodDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(NewDmodDialog)
	m_description = _T("");
	m_directory = _T("");
	m_dmod_name = _T("");
	m_author = _T("");
	m_email_website = _T("");
	//}}AFX_DATA_INIT
}


void NewDmodDialog::DoDataExchange(wxValidator* pDX)
{
	wxDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(NewDmodDialog)
	DDX_Text(pDX, IDC_DMOD_DESCRIPTION, m_description);
	DDX_Text(pDX, IDC_DMOD_DIRECTORY_NAME, m_directory);
	DDX_Text(pDX, IDC_DMOD_TITLE, m_dmod_name);
	DDX_Text(pDX, IDC_AUTHOR, m_author);
	DDX_Text(pDX, IDC_EMAIL_WEBSITE, m_email_website);
	//}}AFX_DATA_MAP
}

void NewDmodDialog::setDmodNamePtr(char *buffer)
{
	dmod_name = buffer;
}


BEGIN_EVENT_TABLE(NewDmodDialog, wxDialog)
	//{{AFX_MSG_MAP(NewDmodDialog)
	//}}AFX_MSG_MAP
END_EVENT_TABLE()

/////////////////////////////////////////////////////////////////////////////
// NewDmodDialog message handlers

void NewDmodDialog::OnOK() 
{
	UpdateData();
	
	if (strlen(m_directory.GetData()) == 0)
	{
		wxMessageBox("You must specify a dmod directory name before continuing", "Must specify name", wxOK);
		return;
	}
	
	if (createDmod(m_directory.GetData(), m_dmod_name.GetData(), 
		m_author.GetData(), m_email_website.GetData(), m_description.GetData()) == false)
	{
		wxDialog::OnCancel();
	}
	else
	{
		strcpy(dmod_name, m_directory.GetData());
		wxDialog::OnOK();
	}
}
