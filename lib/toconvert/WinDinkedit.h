#ifndef WINDINKEDIT_H
#define WINDINKEDIT_H

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
#include <wx/docview.h>

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CWinDinkeditApp:
// See WinDinkedit.cpp for the implementation of this class
//

class CWinDinkeditApp : public wxApp
{
public:
	bool OnInit();
	int OnExit();

protected:
	wxDocManager* m_docManager;
};

DECLARE_APP(CWinDinkeditApp)
#endif
