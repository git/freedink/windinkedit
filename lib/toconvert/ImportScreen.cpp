#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
// ImportScreen.cpp : implementation file
//


#include "WinDinkedit.h"
#include "ImportScreen.h"
#include "Globals.h"
#include "windinkedit-map.h"
#include "ImportMap.h"
#include "Common.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ImportScreen dialog


ImportScreen::ImportScreen(wxWindow* pParent /*=NULL*/)
	: wxDialog(ImportScreen::IDD, pParent)
{
	//{{AFX_DATA_INIT(ImportScreen)
	//}}AFX_DATA_INIT

	
}


void ImportScreen::DoDataExchange(wxValidator* pDX)
{
	wxDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ImportScreen)
	DDX_Control(pDX, IDC_DMOD_IMPORT_LIST, m_dmod_list);
	//}}AFX_DATA_MAP
}

void ImportScreen::makeDmodList()
{
	createDmodList(main_dink_path.GetData(), &m_dmod_list);

	if (current_map->import_dmod_path.GetLength() == 0)
	{
		m_dmod_list.SelectString(-1, getDmodName(current_map->dmod_path) );
	}
	else
	{
		m_dmod_list.SelectString(-1, getDmodName(current_map->import_dmod_path));
	}
}


BEGIN_EVENT_TABLE(ImportScreen, wxDialog)
	//{{AFX_MSG_MAP(ImportScreen)
	ON_LBN_DBLCLK(IDC_DMOD_IMPORT_LIST, OnDblclkDmodImportList)
	//}}AFX_MSG_MAP
END_EVENT_TABLE()

/////////////////////////////////////////////////////////////////////////////
// ImportScreen message handlers

/*BOOL*/int ImportScreen::InitDialog() 
{
	wxDialog::InitDialog();

	makeDmodList();
	
	// TODO: Add extra initialization here
	
	return /*TRUE*/1;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return /*FALSE*/0
}

// load the dmod import screen
void ImportScreen::OnOK()
{
	int index;
	
	// return if an invalid dmod name is selected
	if ((index = m_dmod_list.GetCurSel()) == CB_ERR)
	{
		wxMessageBox("Please select a dmod from the list", "Error", wxOK);
		return;
	}

	wxString dmod_name_entered;
	m_dmod_list.GetText(index, dmod_name_entered);

	current_map->import_dmod_path = main_dink_path + dmod_name_entered + wxString("\\");


	current_map->render_state = SHOW_SCREEN_IMPORTER;

	if (current_map->import_map == NULL)
		current_map->import_map = new ImportMap();
	
	wxDialog::OnOK();
}

void ImportScreen::OnDblclkDmodImportList() 
{
	OnOK();
}
