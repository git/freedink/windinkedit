#ifndef _INTERFACE_H
#define _INTERFACE_H

#include <wx/string.h>

// defines

#define KEY_ESC		SDLK_ESCAPE
#define KEY_DELETE	SDLK_DELETE
#define KEY_LCTRL	SDLK_LCTRL
#define KEY_RCTRL	SDLK_RCTRL
#define KEY_LSHIFT	SDLK_LSHIFT
#define KEY_RSHIFT	SDLK_RSHIFT
#define KEY_TAB		SDLK_TAB
#define KEY_ENTER	SDLK_RETURN
#define KEY_BACKQUOTE	SDLK_BACKQUOTE
#define KEY_PAGEUP	SDLK_PAGEUP
#define KEY_PAGEDOWN	SDLK_PAGEDOWN
#define KEY_ARROW_UP	SDLK_UP
#define KEY_ARROW_DOWN	SDLK_DOWN
#define KEY_ARROW_LEFT	SDLK_LEFT
#define KEY_ARROW_RIGHT	SDLK_RIGHT

//function key support it SHOULD work, not tested fully.
#define KEY_FUNCTION_1	SDLK_F1
#define KEY_FUNCTION_2	SDLK_F2
#define KEY_FUNCTION_3	SDLK_F3
#define KEY_FUNCTION_4	SDLK_F4
#define KEY_FUNCTION_5	SDLK_F5
#define KEY_FUNCTION_6	SDLK_F6
#define KEY_FUNCTION_7	SDLK_F7
#define KEY_FUNCTION_8	SDLK_F8
#define KEY_FUNCTION_9	SDLK_F9
#define KEY_FUNCTION_10	SDLK_F10
#define KEY_FUNCTION_11	SDLK_F11
#define KEY_FUNCTION_12	SDLK_F12

//these keys should support all plus and minus keys
//including on number pad and keyboard. Language doesn't matter
//#define KEY_PLUS		0x6B | 0xBB
//#define KEY_MINUS		0x6D | 0xBD
//they don't work for some odd reason

// prototypes

class CWinDinkeditView;

void initializeInput();

void keyPressed(int key);
void keyReleased(int key);

int mouseLeftPressed(int x, int y);
int mouseLeftReleased(int x, int y);
int mouseRightPressed(int x, int y, CWinDinkeditView* view_window);
int mouseLeftDoubleClick(int x, int y);
int mouseMove(int x, int y);

int setVision(int new_vision);
bool editScript(wxString filename);

void gui_OnSize(int width, int height);

#endif
