/**
 * Sprite selection mode

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _WINDINKEDIT_SPRITE_SELECTOR_H
#define _WINDINKEDIT_SPRITE_SELECTOR_H

#include <glib-object.h>

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_SPRITE_SELECTOR         (windinkedit_sprite_selector_get_type ())
#define WINDINKEDIT_SPRITE_SELECTOR(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_SPRITE_SELECTOR, WindinkeditSpriteSelector))
#define WINDINKEDIT_SPRITE_SELECTOR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_SPRITE_SELECTOR, WindinkeditSpriteSelectorClass))
#define WINDINKEDIT_IS_SPRITE_SELECTOR(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_SPRITE_SELECTOR))
#define WINDINKEDIT_IS_SPRITE_SELECTOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_SPRITE_SELECTOR))
#define WINDINKEDIT_SPRITE_SELECTOR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_SPRITE_SELECTOR, WindinkeditSpriteSelectorClass))

#ifndef _WINDINKEDIT_TYPEDEF_SPRITE_SELECTOR
#define _WINDINKEDIT_TYPEDEF_SPRITE_SELECTOR
typedef struct _WindinkeditSpriteSelector WindinkeditSpriteSelector;
#endif
typedef struct _WindinkeditSpriteSelectorClass WindinkeditSpriteSelectorClass;
typedef struct _WindinkeditSpriteSelectorPrivate WindinkeditSpriteSelectorPrivate;
  
struct _WindinkeditSpriteSelector {
  GObject parent;
  int/*bool*/ showFrames;
  int currentFrame, currentSequence;
  /*< private >*/
  WindinkeditSpriteSelectorPrivate* _priv;
};

struct _WindinkeditSpriteSelectorClass {
  GObjectClass parent;
};

/* used by WINDINKEDIT_TYPE_SPRITE_SELECTOR */
GType windinkedit_sprite_selector_get_type(void);

/* API. */
WindinkeditSpriteSelector* windinkedit_sprite_selector_new();
int windinkedit_sprite_selector_render(WindinkeditSpriteSelector* self);
int windinkedit_sprite_selector_drawGrid(WindinkeditSpriteSelector* self, int x, int y);
void windinkedit_sprite_selector_getSprite(WindinkeditSpriteSelector* self, int x, int y);
void windinkedit_sprite_selector_nextPage(WindinkeditSpriteSelector* self);
void windinkedit_sprite_selector_prevPage(WindinkeditSpriteSelector* self);
void windinkedit_sprite_selector_createList(WindinkeditSpriteSelector* self);
void windinkedit_sprite_selector_resizeScreen(WindinkeditSpriteSelector* self);

G_END_DECLS

#endif
