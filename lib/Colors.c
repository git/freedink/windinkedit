/**
 * Default colors definition

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "Colors.h"
#include "Globals.h"

Uint32 color_black;
Uint32 color_white;
Uint32 color_red;
Uint32 color_green;
Uint32 color_blue;
Uint32 color_yellow;

Uint32 color_tile_grid;
Uint32 color_mouse_box;
Uint32 color_blank_screen_fill;
Uint32 color_tile_grid_in_boundary;
Uint32 color_tile_grid_out_boundary;
Uint32 color_map_screen_grid;
Uint32 color_minimap_screen_selected;
Uint32 color_screen_selected_box;
Uint32 color_sprite_info_text;
Uint32 color_sprite_hover_box;

Uint32 color_sprite_hardness;
Uint32 color_warp_sprite_hardness;
Uint32 color_normal_tile_hardness;
Uint32 color_low_tile_hardness;
Uint32 color_other_tile_hardness;

Uint32 color_polygon_fill_hardness;

Uint32 help_text_color;

int colorformat;

void setColors()
{
  color_black                   = DDColorMatch(lpddsback,   0,   0,   0);
  color_white                   = DDColorMatch(lpddsback, 255, 255, 255);
  color_red                     = DDColorMatch(lpddsback, 255,   0,   0);
  color_green                   = DDColorMatch(lpddsback,   0, 255,   0);
  color_blue                    = DDColorMatch(lpddsback,   0,   0, 255);
  color_yellow                  = DDColorMatch(lpddsback, 255, 255,   0);

  color_tile_grid               = DDColorMatch(lpddsback, 255, 200,   0);
  color_mouse_box               = DDColorMatch(lpddsback, 255, 255, 255);
  color_blank_screen_fill       = DDColorMatch(lpddsback,   0,   0,   0);
  color_tile_grid_in_boundary   = DDColorMatch(lpddsback,   0, 255,   0);
  color_tile_grid_out_boundary  = DDColorMatch(lpddsback,   0,   0, 255);
  color_map_screen_grid         = DDColorMatch(lpddsback,   0,   0, 220);
  color_minimap_screen_selected = DDColorMatch(lpddsback, 255,   0,   0);
  color_screen_selected_box     = DDColorMatch(lpddsback,   0, 255,   0);
  color_sprite_info_text        = DDColorMatch(lpddsback, 255,   0,   0);
  color_sprite_hover_box        = DDColorMatch(lpddsback,   0, 255,   0);
  color_sprite_hardness         = DDColorMatch(lpddsback, 170, 170, 170);
  color_warp_sprite_hardness    = DDColorMatch(lpddsback, 255,   0,   0);
  color_normal_tile_hardness    = DDColorMatch(lpddsback, 220, 220, 170);
  color_low_tile_hardness       = DDColorMatch(lpddsback,   0, 220, 220);
  color_other_tile_hardness     = DDColorMatch(lpddsback, 255,   0, 255);
  color_polygon_fill_hardness   = DDColorMatch(lpddsback, 255,   0,   0);

  help_text_color               = color_yellow;
}

//-----------------------------------------------------------------------------
// Name: DDColorMatch()
// Desc: Convert a RGB color to a pysical color.
//-----------------------------------------------------------------------------
Uint32 DDColorMatch(SDL_Surface *pdds, Uint8 r, Uint8 g, Uint8 b)
{
  return SDL_MapRGB(pdds->format, r, g, b);
}
