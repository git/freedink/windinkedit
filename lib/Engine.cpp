/**
 * Editor initialization and mode multiplexer

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

//#include "WinDinkeditView.h"

#include "SDL.h"
#include "SDL_port.h"
#include "rect.h"


#include "Globals.h"
#include "Engine.h"
#include "windinkedit-sequence.h"
#include "windinkedit-minimap.h"
#include "windinkedit-screen.h"
#include "windinkedit-sprite-editor.h"
#include "windinkedit-graphics-manager.h"
#include "windinkedit-map.h"
#include "windinkedit-sprite-selector.h"
#include "Colors.h"
#include "ImportMap.h"
#include "Structs.h"
#include "Common.h"
#include "Modes.h"
#include "TileSelector.h"

// DEFINES ////////////////////////////////////////////////

// MACROS /////////////////////////////////////////////////

//#define ABS	(var) ((var < 0) ? (var) : (-var))

// TYPES //////////////////////////////////////////////////

// PROTOTYPES /////////////////////////////////////////////

// GLOBALS ////////////////////////////////////////////////

//DDBLTFX			screen_grid_fill;	// used to fill backbuffer

int/*bool*/ draw_box = 0/*false*/;

int mouse_x_position, mouse_y_position;
int mouse_x_origin, mouse_y_origin;

// FUNCTIONS //////////////////////////////////////////////


int Game_Init()
{
	// read the WDE ini file
	readWDEIni();
	
	setColors();

	// TODO: clean-up memory in case we're loading another D-Mod

// 	spriteeditor = new SpriteEditor();

//	if (auto_save_time)
//		SetTimer(mainWnd->GetSafeHwnd(), TIMER_AUTO_SAVE, auto_save_time * 60000, NULL);

	return 1/*true*/;
}


void Game_Shutdown()
{
	windinkedit_graphics_manager_empty(graphics_manager);

	// release the secondary surface
	if (lpddsback != NULL)
	{
		SDL_FreeSurface(lpddsback);
		lpddsback = NULL;
	}

	// release the hidden surface
	if (lpddshidden != NULL)
	{
		SDL_FreeSurface(lpddshidden);
		lpddshidden = NULL;
	}

	// write the WDE ini file
//	writeWDEIni();
}

///////////////////////////////////////////////////////////

int Game_Main(void)
{
WindinkeditMap* current_map = NULL;
	// if a dmod isn't open or the screen doesn't have focus don't do anything
	if (!current_map)
	  {
	    printf("Game_Main: current_map is NULL\n");
	    return 0/*false*/;
	  }

	// draw the main screen
	if (current_map->render_state == SHOW_SCREEN)
	{
	  // draw the screen(s)
//WGC		windinkedit_map_drawScreens(current_map);

		if (current_map->screenMode == TILE_MODE)
		{
			if (draw_box)
			{
				// ???
				//struct rect dest = {mouse_x_origin, mouse_y_origin, mouse_x_position, mouse_y_position};
				//(dest, color_mouse_box, 1);
			}
			else
			{
 				tile_selector->renderScreenTileHover(mouse_x_position, mouse_y_position);
			}
  			tile_selector->screenRender();

			ScreenText("Press F12 to Toggle this text, and F5 to change it's color.", 90);
			ScreenText("Press 'R' for Sprite mode, 'T' for Tile Mode, and 'H' for Hardness Mode", 70);
			ScreenText("Click to Select a tile (click and drag for multiple tiles).", 50);
			ScreenText("Press 'C' to copy tiles, and press 'S' to stamp the tiles", 30);
			
			
		}
		else if (current_map->screenMode == HARDBOX_MODE)
		{
			if (draw_box)
			{
				struct rect dest = {mouse_x_origin, mouse_y_origin, mouse_x_position, mouse_y_position};
				//(dest, color_mouse_box, 1); ???
			} else 
			{
// 				current_map->hard_tile_selector.renderScreenTileHover(mouse_x_position, mouse_y_position);
			}
// 			current_map->hard_tile_selector.screenRender();
		
			ScreenText("Press F12 to Toggle this text, and F5 to change it's color.", 90);
			ScreenText("Press 'R' for Sprite mode, 'T' for Tile Mode, and 'H' for Hardness Mode.", 70);
			ScreenText("Click to Select a tile. Press 'C' to copy tiles,",50);
			ScreenText("press 'Q' to edit the copied hardness, and press 'S' to stamp the tiles.", 30);
			
		} else {
			ScreenText("Press F12 to Toggle this text, and F5 to change it's color.", 110);
			ScreenText("Press 'R' for Sprite mode, 'T' for Tile Mode, and 'H' for Hardness Mode", 90);
			ScreenText("Click a sprite to pick it up. Right click to view it's properties.", 70);
			ScreenText("Right click a map square to view it's details.", 50);
			ScreenText("Press 'M' for screen match, space bar for hardness view, and 'I' for info", 30);
		}
	}
	// check if minimap needs to be shown
	else if (current_map->render_state == SHOW_MINIMAP)
	{
		// draw a box around the selected tile
	  windinkedit_minimap_render(current_map->minimap, lpddsback);

		//ScreenText("Press F12 to Toggle this text, and F5 to change it's color.", 70);
		//ScreenText("Press Spacebar to detail minimap. Click a square to view it.", 50);
		//ScreenText("Right click on any map square to select it's details.", 30);
	}
	// check if sprite selector should be shown
	else if (current_map->render_state == SHOW_SPRITE_SELECTOR)
	{
		// fills the back buffer with black
		SDL_FillRect(lpddsback, NULL, color_map_screen_grid);
//		windinkedit_sprite_selector_render(sprite_selector);
//		windinkedit_sprite_selector_drawGrid(sprite_selector, mouse_x_position, mouse_y_position);
	}
	// check if tile selector should be shown
	else if (current_map->render_state == SHOW_TILE_SELECTOR)
	{
		// fills the back buffer with black
		SDL_FillRect(lpddsback, NULL, color_map_screen_grid);
 		tile_selector->render();

		if (draw_box)
		{
			struct rect dest = {mouse_x_origin, mouse_y_origin, mouse_x_position, mouse_y_position};
			drawBox(&dest, color_mouse_box, 1);
		}
		else
		{
 			tile_selector->renderTileHover(mouse_x_position, mouse_y_position);
		}
	}
	// check if sprite selector should be shown
	else if (current_map->render_state == SHOW_HARD_TILE_SELECTOR)
	{
		// fills the back buffer with black
		SDL_FillRect(lpddsback, NULL, color_map_screen_grid);
// 		current_map->hard_tile_selector.render();
// 		current_map->hard_tile_selector.drawGrid(mouse_x_position, mouse_y_position);
	}
	else if (current_map->render_state == SHOW_SCREEN_IMPORTER)
	{
		import_map->render();
	}
	else if (current_map->render_state == SHOW_TILE_HARDNESS_EDITOR)
	{
		// fills the back buffer with black
		SDL_FillRect(lpddsback, NULL, color_map_screen_grid);
// 		current_map->hard_tile_selector.displayEditor(mouse_x_position, mouse_y_position);

		ScreenText("Press F12 to Toggle this text, and F5 to change it's color.", 70);
		ScreenText("Click a hard box to change the hardness.", 50);
		ScreenText("Hold Control for low hardness, and Shift for unknown hardness.", 30);
	}
	else if (current_map->render_state == SHOW_SPRITE_HARDNESS_EDITOR)
	{
		if (!draw_box)
		{
			// fills the back buffer with black
			SDL_FillRect(lpddsback, NULL, color_map_screen_grid);
// 			spriteeditor->DrawSprite();
// 			spriteeditor->DrawHardness();
		}

		ScreenText("Press F12 to Toggle this text, and F5 to change it's color.", 70);
		ScreenText("Click and drag to change the hard box. Right click to change the Depth Que", 50);
		ScreenText("Press Escape to exit.", 30);

	}

	SDL_Flip(lpddsback);

	windinkedit_graphics_manager_checkMemory(graphics_manager);

	return 1/*true*/;
}
