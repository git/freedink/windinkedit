#ifndef _COMMON_H
#define _COMMON_H

#include "SDL.h"

// PROTOTYPES /////////////////////////////////////////////

bool  readDinksmallwoodIni(char* path);
bool writeDinksmallwoodIni(char *path);

bool readWDEIni();
bool writeWDEIni();

bool createDmod(char* dmod_dir_name, char* dmod_title, char* copyright1, char* copyright2, char* description);
void compensateScreenGap(int &x, int &y);
void CopyAllFiles(char* path_in, char* path_out, char* sub_path);

bool fixBounds(struct rect &dest_box, struct rect &src_box, struct rect* clip_box);
bool fixBounds(struct rect &box, struct rect* clip_box);
void drawBox(struct rect* box, int color, int line_width);
int drawFilledBox(struct rect &box, int color, bool transparent, SDL_Surface* &surface);
void drawLine(int x1, int y1, int x2, int y2, int color);
void ScreenText(const char *output, int y_offset);

/* bool readDmodDiz(char* path, wxString &dmod_title, wxString &copyright1, wxString &copyright2, wxString &description); */
/* bool writeDmodDiz(char* path, char* dmod_title, char* copyright1, char* copyright2, char* description); */
/* bool createDmodList(char* dink_path, wxListBox *dmod_list); */

#endif
