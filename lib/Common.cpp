// INCLUDES ///////////////////////////////////////////////
#include "SDL.h"
#include "SDL_gfxPrimitives.h"
#include "SDL_port.h"

#include "Colors.h"
#include "Common.h"
#include "gfx_fonts.h"
#include "Globals.h"
#include "windinkedit-map.h"
//#include "windinkedit-minimap.h"

// #include <fstream>
// using namespace std;

#define MAX_DMOD_NAME_LENGTH	20

// reads the windinkedit ini file
bool readWDEIni()
{
	printf("readWDEIni: TODO\n");
// 	char default_string[256];
// 	char return_string[256];
// 	char file_path[PATH_MAX];
// 	char windows_path[PATH_MAX];

// 	if (GetWindowsDirectory(windows_path, PATH_MAX) == NULL)
// 		return false;

// 	sprintf(file_path, "%s\\prefs.ini", windows_path);

// 	// screen gap
// 	sprintf(default_string, "%d", DEFAULT_SCREEN_GAP);
// 	if( GetPrivateProfileString("Display_Settings",
// 		"screen_gap", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	screen_gap = atoi(return_string);

// 	//auto update minimap
// 	sprintf(default_string, "%d", 0);
// 	if( GetPrivateProfileString("Display_Settings",
// 		"update_minimap", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	update_minimap = atoi(return_string);

// 	// max undo level
// 	sprintf(default_string, "%d", DEFAULT_MAX_UNDO_LEVEL);
// 	if( GetPrivateProfileString("General_Settings",
// 		"Max_Undo_Level", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	max_undo_level = atoi(return_string);

// 	// auto save time
// 	sprintf(default_string, "%d", DEFAULT_AUTOSAVE_TIME);
// 	if( GetPrivateProfileString("General_Settings",
// 		"Auto_Save_Time", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	auto_save_time = atoi(return_string);

// 	//Display Settings
// 	sprintf(default_string, "%d", true);
// 	if( GetPrivateProfileString("Display_Settings",
// 		"show_minimap_progress", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	show_minimap_progress = atoi(return_string);

// 	sprintf(default_string, "%d", false);
// 	if(GetPrivateProfileString("General_Settings",
// 		"Optimize_Dink_Ini", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	optimize_dink_ini = atoi(return_string);

// 	sprintf(default_string, "%i", 1);
// 	if(GetPrivateProfileString("General_Settings",
// 		"Tile_Brush_Size", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	tile_brush_size = atoi(return_string);

// 	sprintf(default_string, "%i", fast_minimap_update);
// 	if(GetPrivateProfileString("Display_Settings",
// 		"Fast_Minimap_Update", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	fast_minimap_update = atoi(return_string);

// 	sprintf(default_string, "%i", hover_sprite_info);
// 	if(GetPrivateProfileString("General_Settings",
// 		"Hover_Sprite_Info", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	hover_sprite_info = atoi(return_string);

// 	sprintf(default_string, "%i", hover_sprite_hardness);
// 	if(GetPrivateProfileString("General_Settings",
// 		"Hover_Sprite_Hardness", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	hover_sprite_hardness = atoi(return_string);

// 	sprintf(default_string, "%i", help_text);
// 	if (GetPrivateProfileString("General_Settings",
// 		"Show_Help_Text", default_string, return_string, 256, file_path) <= 0)
// 		return false;

// 	help_text = atoi(return_string);
	
	return true;
}

// writes the windinkedit ini file
bool writeWDEIni()
{
	printf("writeWDEIni: TODO\n");
// 	char default_string[256];
// 	char file_path[PATH_MAX];
// 	char windows_path[PATH_MAX];

// 	if (GetWindowsDirectory(windows_path, PATH_MAX) == NULL)
// 		return false;

// 	sprintf(file_path, "%s\\prefs.ini", windows_path);

// 	// screen gap
// 	sprintf(default_string, "%d", screen_gap);
// 	if (WritePrivateProfileString("Display_Settings", 
// 		"screen_gap", default_string, file_path) == NULL)
// 		return false;
	
// 	//auto update minimap
// 	sprintf(default_string, "%d", update_minimap);
// 	if (WritePrivateProfileString("Display_Settings", 
// 		"update_minimap", default_string, file_path) == NULL)
// 		return false;

// 	//display minimap update progress
// 	sprintf(default_string, "%d", show_minimap_progress);
// 	if (WritePrivateProfileString("Display_Settings",
// 		"show_minimap_progress", default_string, file_path) == NULL)
// 		return false;

// 	// max undo level
// 	sprintf(default_string, "%d", max_undo_level);
// 	if (WritePrivateProfileString("General_Settings", 
// 		"Max_Undo_Level", default_string, file_path) == NULL)
// 		return false;

// 	// max undo level
// 	sprintf(default_string, "%d", auto_save_time);
// 	if (WritePrivateProfileString("General_Settings", 
// 		"Auto_Save_Time", default_string, file_path) == NULL)
// 		return false;

// 	//optimize dink ini
// 	sprintf(default_string, "%d", optimize_dink_ini);
// 	if (WritePrivateProfileString("General_Settings",
// 		"Optimize_Dink_Ini", default_string, file_path) == NULL)
// 		return false;

// 	//Brush size for hard tiles
// 	sprintf(default_string, "%i", tile_brush_size);
// 	if (WritePrivateProfileString("General_Settings",
// 		"Tile_Brush_Size", default_string, file_path) == NULL)
// 		return false;

// 	//fast minimap updater
// 	sprintf(default_string, "%i", fast_minimap_update);
// 	if (WritePrivateProfileString("Display_Settings",
// 		"Fast_Minimap_Update", default_string, file_path) == NULL)
// 		return false;

// 	sprintf(default_string, "%i", hover_sprite_info);
// 	if (WritePrivateProfileString("General_Settings",
// 		"Hover_Sprite_Info", default_string, file_path) == NULL)
// 		return false;

// 	sprintf(default_string, "%i", hover_sprite_hardness);
// 	if (WritePrivateProfileString("General_Settings",
// 		"Hover_Sprite_Hardness", default_string, file_path) == NULL)
// 		return false;

// 	sprintf(default_string, "%i", help_text);
// 	if (WritePrivateProfileString("General_Settings",
// 		"Show_Help_Text", default_string, file_path) == NULL)
// 		return false;
	
	return true;
}

bool writeDinksmallwoodIni(char *path)
{
	printf("writeDinksmallwoodIni: TODO\n");
// 	char windows_path[PATH_MAX];
// 	char dink_ini_path[PATH_MAX];
	
// 	// first find the location of the windows directory
// 	if (GetWindowsDirectory(windows_path, PATH_MAX) == NULL)
// 		return false;

// 	sprintf(dink_ini_path, "%s\\dinksmallwood.ini", windows_path);

// 	// open the dink.ini file
// 	ofstream dink_ini(dink_ini_path);

// 	if (dink_ini == NULL)
// 		return false;

// 	dink_ini << "[Dink Smallwood Directory Information for the CD to read]\n" 
// 		<< path 
// 		<< "/*FALSE*/0\n";

// 	dink_ini.close();

	return true;
}

// gets the path of the main dink directory
bool readDinksmallwoodIni(char* path)
{
	printf("readDinksmallwoodIni: TODO\n");
// 	char windows_path[PATH_MAX];
// 	char dink_ini_path[PATH_MAX];
// 	char dink_pathname[PATH_MAX];
	
// 	// first find the location of the windows directory
// 	if (GetWindowsDirectory(windows_path, PATH_MAX) == NULL)
// 		return false;

// 	sprintf(dink_ini_path, "%s\\dinksmallwood.ini", windows_path);

// 	// open the dink.ini file
// 	ifstream dink_ini(dink_ini_path);

// 	if (dink_ini == NULL)
// 		return false;

// 	// get the first line
// 	dink_ini.getline(dink_pathname, PATH_MAX);

// 	if (dink_pathname[0] == 0)
// 		return false;

// 	// now get the second line
// 	dink_ini.getline(dink_pathname, PATH_MAX);

// 	if (dink_pathname[0] == 0)
// 		return false;

// 	//if not slash on the end, we shall add it.
// 	if (dink_pathname[strlen(dink_pathname)-1] != '\\')
// 		sprintf(path, "%s\\", dink_pathname);
// 	else
// 		strcpy(path, dink_pathname);

	return true;
}

// bool readDmodDiz(char* path, wxString &dmod_title, wxString &copyright1, 
// 				 wxString &copyright2, wxString &description)
// {
// 	printf("readDmodDiz: TODO\n");
// 	char dmod_diz_path[256];

// 	sprintf(dmod_diz_path, "%s\\dmod.diz", path);

// 	// open the dink.diz file
// 	ifstream dmod_diz(dmod_diz_path);

// 	if (dmod_diz == NULL)
// 		return false;

// 	char buffer[1024];
	
// 	dmod_diz.getline(buffer, 1024);
// 	dmod_title = wxString::FromAscii(buffer);

// 	dmod_diz.getline(buffer, 1024);
// 	copyright1 = wxString::FromAscii(buffer);

// 	dmod_diz.getline(buffer, 1024);
// 	copyright2 = wxString::FromAscii(buffer);

// 	description.Empty();
// 	while (dmod_diz.eof() == false)
// 	{
// 		dmod_diz.getline(buffer, 1024);
// 		description += buffer;
// 		description += "\n";
// 	}

// 	return true;
// }

// writes out the dmod.diz file for the dmod
// bool writeDmodDiz(char* path, char* dmod_title, char* copyright1, char* copyright2, char* description)
// {
// 	char dmod_diz_path[256];

// 	sprintf(dmod_diz_path, "%s\\dmod.diz", path);

// 	// open the dink.diz file
// 	ofstream dmod_diz(dmod_diz_path);

// 	if (dmod_diz == NULL)
// 		return false;

// 	dmod_diz << dmod_title << endl;
// 	dmod_diz << copyright1 << endl;
// 	dmod_diz << copyright2 << endl;
// 	dmod_diz << description << endl;

// 	return true;
// }


void CopyAllFiles(char path_in[PATH_MAX], char path_out[PATH_MAX], char sub_path[PATH_MAX])
{
	printf("CopyAllFiles: TODO\n");
// 	if (path_in[strlen(path_in) -1] != '\\')
// 		strcat(path_in, "\\");

// 	if (path_out[strlen(path_out) -1] != '\\')
// 		strcat(path_out, "\\");


// 	//varibles used for the searching
// 	HANDLE hFind;
// 	WIN32_FIND_DATA dataFind;
// 	bool bMoreFiles = true;

// 	//strings
// 	char findpath[250];
// 	char fullsrcpath[250];
// 	char fulldestpath[250];
// 	char in_temp[250];
// 	char out_temp[250];

// 	//tell it to find *.*
// 	sprintf(fullsrcpath, "%s%s*.*", path_in, sub_path);
// 	hFind = FindFirstFile(fullsrcpath, &dataFind);

// 	while (hFind != INVALID_HANDLE_VALUE && bMoreFiles == true)
// 	{
// 		sprintf(in_temp, "%s%s%s", path_in, sub_path, dataFind.cFileName);
// 		sprintf(out_temp, "%s%s%s", path_out, sub_path, dataFind.cFileName);

// 		CopyFile(in_temp, out_temp, /*TRUE*/1);
// 		bMoreFiles = FindNextFile(hFind, &dataFind);
// 	}

// 	//find new directory
// 	sprintf(fullsrcpath, "%s%s*.", path_in, sub_path);
// 	hFind = FindFirstFile(fullsrcpath, &dataFind);
// 	bMoreFiles = true;

// 	while (hFind != INVALID_HANDLE_VALUE && bMoreFiles == true)
// 	{
// 		//make sure it's directory
// 		if ((dataFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0 && dataFind.cFileName[0] != '.')
// 		{
// 			char temp[50];

// 			sprintf(temp, "%s\\", dataFind.cFileName);

// 			sprintf(in_temp, "%s%s%s", path_in, sub_path, temp);
// 			sprintf(out_temp, "%s%s%s", path_out, sub_path, temp);
// 			sprintf(fullsrcpath, "%s%s", path_in, sub_path);
// 			sprintf(fulldestpath, "%s%s", path_out, sub_path);

// 			CreateDirectory(out_temp, NULL);
// 			CopyAllFiles(fullsrcpath, fulldestpath, temp);
// 		}
// 		bMoreFiles = FindNextFile(hFind, &dataFind);
// 	}
}

// bool createDmod(char* dmod_dir_name, char* dmod_title, char* copyright1, char* copyright2, char* description)
// {
// 	//varibles needed
// 	char dmod_path[PATH_MAX];
// 	char buffer[250];
// 	char path[PATH_MAX];

// 	//used for opening files.
// 	FILE *stream;

	
// 	sprintf(dmod_path, "%s%s", main_dink_path.GetData(), dmod_dir_name);

// 	// create the main dmod directory
// 	if (CreateDirectory(dmod_path, NULL) == /*FALSE*/0)
// 	{
// 		char error_message[1024];
// 		sprintf(error_message, "Could not create dmod %s, bad dmod name or dmod with that name already exists", dmod_dir_name);
// 		wxMessageBox(NULL, error_message, "Could not create dmod", wxOK);
// 		return false;
// 	}

// 	//use default skeleton location.
// 	sprintf(buffer, "%s\\skeleton\\HARD.dat", main_dink_path.GetData());
// 	sprintf(path, "%s\\skeleton\\", main_dink_path.GetData());

// 	//try to open the hard.dat (it's the important one)
// 	while ((stream = fopen(buffer, "rb")) == NULL)
// 	{
// 		//propmt user we could not find it.
// 		wxMessageBox(mainWnd->GetSafeHwnd(), 
// 			"Skeleton directory not found. Please select.", NULL, wxOK);

// 		//make it look pretty for the user
// 		sprintf(buffer, "%s\\skeleton", main_dink_path.GetData());

// 		wxString path = ::wxDirSelector("Choose D-Mod location", buffer);
// 		//what did the click.
// 		if (path.empty())
// 		{
// 			//cancel. Notify user d-mod was not created.
// 			::wxMessageBox("D-mod was not created.");
// 			return false;
// 		} else {
// 			//they clicked ok. Lets do it all over again.
// 			sprintf(buffer, "%s", newdlg.m_strFolderPath.GetData());
// 			sprintf(path, "%s", newdlg.m_strFolderPath.GetData());

// 			if (buffer[strlen(buffer) -1] != '\\')
// 				sprintf(buffer, "%s\\", buffer);

// 			sprintf(buffer, "%sHARD.dat", buffer);
// 		}
// 	}

// 	CopyAllFiles(path, dmod_path, "");

// 	// write out the dmod.diz
// 	writeDmodDiz(dmod_path, dmod_title, copyright1, copyright2, description);

// 	return true;
// }

void compensateScreenGap(int &x, int &y)
{
	// compensate for screen gap
	if (x > SCREEN_WIDTH)
	{
		x -= screen_gap;

		if (x < SCREEN_WIDTH)
			x = SCREEN_WIDTH;
	}

	if (x < 0)
	{
		x += screen_gap;

		if (x > 0)
			x = 0;
	}

	if (y > SCREEN_HEIGHT)
	{
		y -= screen_gap;

		if (y < SCREEN_HEIGHT)
			y = SCREEN_HEIGHT;
	}

	if (y < 0)
	{
		y += screen_gap;

		if (y > 0)
			y = 0;
	}
}

// bool verifyDirectory(char *directory)
// {
// 	char full_path[256];
// 	strcpy(full_path, directory);
// 	strcat(full_path, "/dink.dat");

// 	sprintf(full_path, "%s%s/dink.dat", (const char*)main_dink_path.fn_str(), directory);

// 	FILE* input = fopen(full_path, "rb");

// 	if (input == NULL)
// 	{
// 		return false;
// 	}
// 	else
// 	{
// 		fclose(input);
// 		return true;
// 	}
// }

// bool createDmodList(char* dink_path, wxListBox *dmod_list)
// {
// 	printf("createDmodList: TODO\n");
// 	HANDLE hFind;
// 	WIN32_FIND_DATA dataFind;
// 	/*BOOL*/int bMoreFiles = /*TRUE*/1;
// 	int i;

// 	dmod_list->ResetContent();

// 	char dmod_path[256];
// 	char filename[MAX_DMOD_NAME_LENGTH];

// 	sprintf(dmod_path, "%s*.*", main_dink_path);

// 	hFind = FindFirstFile(dmod_path, &dataFind);

// 	// get all the *.c files in the directory
// 	while(hFind != INVALID_HANDLE_VALUE && bMoreFiles == /*TRUE*/1)
// 	{
// 		if ((dataFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) > 0 &&
// 			dataFind.cFileName[0] != '.')
// 		{
// 			strncpy(filename, dataFind.cFileName, MAX_DMOD_NAME_LENGTH);
// 			filename[MAX_DMOD_NAME_LENGTH - 1] = 0;

// 			// make lower
// 			for (i = 1; i < MAX_DMOD_NAME_LENGTH - 1; i++)
// 			{
// 				filename[i] = tolower(filename[i]);
// 			}

// 			if (verifyDirectory(filename) == true)
// 				dmod_list->AddString(filename);
// 		}

// 		bMoreFiles = FindNextFile(hFind, &dataFind);
// 	}

// 	FindClose(hFind);

// 	return true;
// }

///////////////////////////////////////////////////////////

bool fixBounds(struct rect &dest_box, struct rect &src_box, struct rect* clip_box)
{
	if (dest_box.left < clip_box->left)
	{
		src_box.left += clip_box->left - dest_box.left;
		dest_box.left = clip_box->left;
	}

	if (dest_box.right > clip_box->right)
	{
		src_box.right -= dest_box.right - clip_box->right;
		dest_box.right = clip_box->right;
	}

	if (dest_box.left >= dest_box.right)
		return false;

	if (dest_box.top < clip_box->top)
	{
		src_box.top += clip_box->top - dest_box.top;
		dest_box.top = clip_box->top;
	}

	if (dest_box.bottom > clip_box->bottom)
	{
		src_box.bottom -= dest_box.bottom - clip_box->bottom;
		dest_box.bottom = clip_box->bottom;
	}

	if (dest_box.top >= dest_box.bottom)
		return false;

	return true;
}

///////////////////////////////////////////////////////////

bool fixBounds(struct rect &box, struct rect* clip_box)
{
	int temp; //used to hold for fixing boxes.

	//check if we are inverted, if so, correct.
	if (box.left > box.right)
	{
		temp = box.right;
		box.right = box.left;
		box.left = temp;
	}
	if (box.top > box.bottom)
	{
		temp = box.top;
		box.top = box.bottom;
		box.bottom = temp;
	}
	if (clip_box->left > clip_box->right)
	{
		temp = clip_box->right;
		clip_box->right = clip_box->left;
		clip_box->left = temp;
	}
	if (clip_box->top > clip_box->bottom)
	{
		temp = clip_box->top;
		clip_box->top = clip_box->bottom;
		clip_box->bottom = temp;
	}

	//acctually clip the bounds.
	if (box.left < clip_box->left)
	{
		box.left = clip_box->left;
	}

	if (box.right > clip_box->right)
	{
		box.right = clip_box->right;
	}

	if (box.left >= box.right)
		return false;

	if (box.top < clip_box->top)
	{
		box.top = clip_box->top;
	}

	if (box.bottom > clip_box->bottom)
	{
		box.bottom = clip_box->bottom;
	}

	if (box.top >= box.bottom)
		return false;

	return true;
}

///////////////////////////////////////////////////////////

void drawBox(struct rect* box, int color, int line_width)
{
  if (box->left > box->right)
    {
      int temp = box->left;
      box->left = box->right;
      box->right = temp;
    }
  
  if (box->top > box->bottom)
    {
      int temp = box->top;
      box->top = box->bottom;
      box->bottom = temp;
    }
  
  // fill in the destination of the box
  struct rect dest;
  
  // draw left
  dest.left	= box->left;
  dest.right	= box->left + line_width;
  dest.top	= box->top;
  dest.bottom	= box->bottom;
  fill_rect(lpddsback, &dest, color);
  
  // draw right
  dest.left = box->right - line_width;
  dest.right = box->right;
  fill_rect(lpddsback, &dest, color);
  
  // draw top
  dest.left	= box->left + line_width;
  dest.right	= box->right - line_width;
  dest.top	= box->top;
  dest.bottom	= box->top + line_width;
  fill_rect(lpddsback, &dest, color);
  
  // draw bottom
  dest.top	= box->bottom - line_width;
  dest.bottom	= box->bottom;
  fill_rect(lpddsback, &dest, color);
}

int drawFilledBox(struct rect &box, int color, bool transparent, SDL_Surface* &surface)
{
	int temp;
	if (box.left > box.right)
	{
		temp = box.left;
		box.left = box.right;
		box.right = temp;
	}

	if (box.top > box.bottom)
	{
		temp = box.top;
		box.top = box.bottom;
		box.bottom = temp;
	}

// 	struct rect clip_box = {0, 0, current_map->window_width, current_map->window_height};

// 	fixBounds(box, &clip_box);
	
	if (transparent)
	{
		/*struct rect box_temp = box;

		temp = box_temp.left;
		box_temp.left = 0;
		box_temp.right -= temp;

		temp = box_temp.top;
		box_temp.top = 0;
		box_temp.bottom -= temp;

		lpddshidden->Blt(&box_temp, NULL, NULL, DDBLT_COLORFILL | DDBLT_WAIT, &gridcolor);
		lpddsback->Blt(&box, lpddshidden, &box_temp, DDBLT_WAIT | DDBLT_KEYSRC, NULL);*/
		fill_rect(surface, &box, color);
	}
	else
	{
		fill_rect(surface, &box, color);
	}

	return true;
}

void drawLine(int x1, int y1, int x2, int y2, int color)
{
	lineColor(lpddsback, x1, y1, x2, y2, color);
}

void ScreenText(const char output[PATH_MAX], int y_offset)
{
	if (help_text)
	{
//		Draw_Text_GDI(output, 10, current_map->window_height - y_offset, help_text_color, lpddsback);
	}
}
