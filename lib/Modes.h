/**
 * Secondary UI modes initialization

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _MODES_H
#define _MODES_H

#ifndef _WINDINKEDIT_TYPEDEF_SPRITE_SELECTOR
#define _WINDINKEDIT_TYPEDEF_SPRITE_SELECTOR
typedef struct _WindinkeditSpriteSelector WindinkeditSpriteSelector;
#endif
class HardTileSelector;
class ImportMap;
class Undo;
class TileSelector;

extern HardTileSelector* hard_tile_selector; /* sizeof = 2010564 */
extern TileSelector* tile_selector;
//extern WindinkeditSpriteSelector* sprite_selector;
//extern WindinkeditSpriteEditor* spriteeditor;
extern ImportMap* import_map;
extern Undo* undo_buffer;

/* SpriteLibrary sprite_library; */

#endif
