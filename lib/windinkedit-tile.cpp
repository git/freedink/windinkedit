#include "windinkedit-tile.h"
#include <glib/gprintf.h>

#include "Globals.h"
#include "Engine.h"

#include "windinkedit-dmod.h"
#include "windinkedit-gc.h"
#include "windinkedit-graphics-manager.h"
#include "windinkedit-map.h"
#include "Colors.h"
#include "Common.h"

#include "SDL.h"
#include "SDL_port.h"
#include "SDL_image.h"


/*************/
/**  Class  **/
/**         **/
/*************/

static GObjectClass *parent_class = NULL;

enum {
        WINDINKEDIT_TILE_CONSTRUCT_FILE_LOCATION = 1,
};


/** Private fields **/

#define selfp (self->_priv)

struct _WindinkeditTilePrivate {
  int file_location;
  int width, height;  // size of bitmap
  SDL_Surface* image;
  GRAPHICQUE* graphic_que_location;	// so we don't run out of memory
};


static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditTile* self = (WindinkeditTile*)instance;
  self->_priv = G_TYPE_INSTANCE_GET_PRIVATE(self, WINDINKEDIT_TYPE_TILE, WindinkeditTilePrivate);
  selfp->image = NULL;
}


static void
___get_property(GObject      *object,
		guint         property_id,
		GValue       *value,
		GParamSpec   *pspec)
{
  WindinkeditTile *self = (WindinkeditTile *) object;
  
  switch (property_id) {
  case WINDINKEDIT_TILE_CONSTRUCT_FILE_LOCATION:
    g_value_set_int(value, selfp->file_location);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

static void
___set_property (GObject      *object,
		 guint         property_id,
		 const GValue *value,
		 GParamSpec   *pspec)
{
  WindinkeditTile *self = (WindinkeditTile *) object;
  
  switch (property_id) {
  case WINDINKEDIT_TILE_CONSTRUCT_FILE_LOCATION:
    selfp->file_location = g_value_get_int(value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

/**
 * Clean-up object (after dispose)
 */
static void
___finalize(GObject *obj)
{
  WindinkeditTile *self = (WindinkeditTile *)obj;

  if (selfp->image != NULL)
    {
      SDL_FreeSurface(selfp->image);
      selfp->image = NULL;
    }

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->finalize(obj);
}


static void
___class_init (gpointer g_class,
	       gpointer g_class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
  WindinkeditTileClass *klass = WINDINKEDIT_TILE_CLASS (g_class);
  GParamSpec *pspec;

  gobject_class->set_property = ___set_property;
  gobject_class->get_property = ___get_property;
  gobject_class->finalize = ___finalize;

  g_type_class_add_private(g_class, sizeof(WindinkeditTilePrivate));
  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));
  
  pspec = g_param_spec_int("file-location",
			   "Tile file-location nick",
			   "X in TsX.bmp",
			   1,  /* minimum */
			   MAX_TILE_BMPS,  /* maximum */
			   1,  /* default */
			   (GParamFlags)(G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));
  g_object_class_install_property(gobject_class,
				  WINDINKEDIT_TILE_CONSTRUCT_FILE_LOCATION,
				  pspec);
}

GType windinkedit_tile_get_type (void)
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditTileClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      (GClassInitFunc) ___class_init,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditTile),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditTileType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/

WindinkeditTile* windinkedit_tile_new(int file_location)
{
  return WINDINKEDIT_TILE(g_object_new(WINDINKEDIT_TYPE_TILE, "file-location", file_location, NULL));
}

int windinkedit_tile_loadSurface(WindinkeditTile* self)
{
	char bmp_file[PATH_MAX];
	sprintf(bmp_file, "%sTiles/Ts%.2d.bmp",
		windinkedit_dmod_get_dmoddir(current_dmod), selfp->file_location);
	
	if ((selfp->image = IMG_Load(bmp_file)) == NULL)
	{
		// try loading from the dink dmod directory
		sprintf(bmp_file, "%sTiles/Ts%.2d.bmp", windinkedit_dmod_get_refdir(current_dmod), selfp->file_location);
		if ((selfp->image = IMG_Load(bmp_file)) == NULL)
			{
				printf("Tile::loadSurface: error load %s: %s\n", bmp_file, IMG_GetError());
				return false;
			}
	}

	selfp->graphic_que_location = windinkedit_graphics_manager_addGraphic(graphics_manager, &selfp->image);

	return true;
}

// render the entire tile bitmap
int windinkedit_tile_renderAll(WindinkeditTile* self, int x, int y, int dest_width, int dest_height)
{
	// draw a bob at the x,y defined in the BOB
	// on the destination surface defined in dest
	if (selfp->image == NULL)
		windinkedit_tile_loadSurface(self);
	
	if (selfp->image == NULL)
		return false;

	struct rect dest_rect;                        

	// fill in the destination rect
	dest_rect.left   = x;
	dest_rect.top    = y;
	dest_rect.right  = x + int(double(dest_width) * (double(selfp->width) / double(SCREEN_WIDTH)));
	dest_rect.bottom = y + int(double(dest_height) * (double(selfp->height) / double(SCREEN_HEIGHT)));

	// blt to destination surface
	Blt(lpddsback, &dest_rect, selfp->image, NULL);

	windinkedit_graphics_manager_moveLocation(graphics_manager, selfp->graphic_que_location);

	return true;
} // end render

int windinkedit_tile_render(WindinkeditTile* self, int tileX, int tileY, SDL_Surface* surface, SDL_Rect* dstrect)
{
  // draw a bob at the x,y defined in the BOB
  // on the destination surface defined in dest
  if (selfp->image == NULL)
    windinkedit_tile_loadSurface(self);
  
  SDL_Rect srcrect;
  
  // fill in the source rect
  srcrect.x = tileX * TILEWIDTH;
  srcrect.y = tileY * TILEHEIGHT;
  srcrect.w = TILEWIDTH;
  srcrect.h = TILEHEIGHT;
  
  // blt to destination surface
  SDL_BlitSurface(selfp->image, &srcrect, surface, dstrect);
  
  windinkedit_graphics_manager_moveLocation(graphics_manager, selfp->graphic_que_location);
  
  return 1/*true*/;
}
