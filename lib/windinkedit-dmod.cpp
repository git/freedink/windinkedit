/**
 * Main D-Mod data structure

 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <fstream>
using namespace std;

#include "windinkedit-dmod.h"
#include "windinkedit-map.h"
#include "windinkedit-minimap.h"
#include "windinkedit-sequence.h"
#include "windinkedit-tile.h"
#include "Modes.h"
#include "Undo.h"
#include "HardTileSelector.h"

static int windinkedit_dmod_free_sequences(WindinkeditDmod *self);
static void windinkedit_dmod_free_tiles(WindinkeditDmod *self);
int windinkedit_dmod_parse_dink_ini(WindinkeditDmod *self);
int windinkedit_dmod_save_dink_ini(WindinkeditDmod *self);


/*************/
/**  Class  **/
/**         **/
/*************/

static GObjectClass *parent_class = NULL;

enum {
        WINDINKEDIT_DMOD_REFDIR = 1,
        WINDINKEDIT_DMOD_DMODDIR = 2,
};


/** Private fields **/

#define selfp (self->_priv)

struct _WindinkeditDmodPrivate {
  const gchar* refdir;                         /* path to the dink reference directory */
  const gchar* dmoddir;                        /* path to the current D-Mod */
  WindinkeditMap* map;                   /* map.dat, with its screens and its sprites */
  WindinkeditTile* tiles[MAX_TILE_BMPS]; /* background tiles Ts*.bmp and their hardness from hard.dat */
  WindinkeditSequence* sequences[MAX_SEQUENCES]; /* graphics sequences defined in dink.ini */
  /* WindinkeditHardnessTileset */       /* hardness tiles from hard.dat - currently in HardTileSelector */
  WindinkeditMinimap* minimap;           /* map preview, to update when one of the above changes */
};


static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditDmod* self = (WindinkeditDmod*)instance;
  self->_priv = G_TYPE_INSTANCE_GET_PRIVATE(self, WINDINKEDIT_TYPE_DMOD, WindinkeditDmodPrivate);

  //zero out the sequence memory
  memset(selfp->sequences, 0, MAX_SEQUENCES * sizeof(WindinkeditSequence*));
}

/**
 * Run after properties are set
 */
static void
___constructed(GObject* object)
{
  WindinkeditDmod* self = WINDINKEDIT_DMOD(object);
  if (parent_class->constructed != NULL)
    parent_class->constructed(object);
}



static void
___get_property(GObject      *object,
		guint         property_id,
		GValue       *value,
		GParamSpec   *pspec)
{
  WindinkeditDmod *self = (WindinkeditDmod *) object;
  
  switch (property_id) {
  case WINDINKEDIT_DMOD_REFDIR:
    g_value_set_string(value, selfp->refdir);
    break;
  case WINDINKEDIT_DMOD_DMODDIR:
    g_value_set_string(value, selfp->dmoddir);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

static void
___set_property (GObject      *object,
		 guint         property_id,
		 const GValue *value,
		 GParamSpec   *pspec)
{
  WindinkeditDmod *self = (WindinkeditDmod *) object;
  
  switch (property_id) {
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

/**
 * Drop all references to objects that may reference us
 */
static void
___dispose (GObject *obj)
{
  WindinkeditDmod *self = (WindinkeditDmod *)obj;

  g_object_unref(selfp->map);
  g_object_unref(selfp->minimap);
  windinkedit_dmod_free_sequences(self);
  windinkedit_dmod_free_tiles(self);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->dispose(obj);
}

/**
 * Clean-up object (after dispose)
 */
static void
___finalize(GObject *obj)
{
  WindinkeditDmod *self = (WindinkeditDmod *)obj;

  g_free((char*)selfp->refdir);
  g_free((char*)selfp->dmoddir);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->finalize(obj);
}


static void
___class_init(gpointer g_class,
	      gpointer g_class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
  WindinkeditDmodClass *klass = WINDINKEDIT_DMOD_CLASS (g_class);
  GParamSpec *pspec;

  gobject_class->constructed = ___constructed;
  gobject_class->set_property = ___set_property;
  gobject_class->get_property = ___get_property;
  gobject_class->dispose = ___dispose;
  gobject_class->finalize = ___finalize;

  g_type_class_add_private(g_class, sizeof(WindinkeditDmodPrivate));
  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));
  
  pspec = g_param_spec_string ("refdir",
			       "Full path to the Dink reference directory",
			       "Contains base graphics, tiles, etc., e.g. /usr/share/dink/dink/",
			       NULL /* default value */,
			       (GParamFlags)(G_PARAM_READABLE));
  g_object_class_install_property (gobject_class,
				   WINDINKEDIT_DMOD_REFDIR,
				   pspec);

  pspec = g_param_spec_string ("dmoddir",
			       "Full path to the current D-Mod",
			       "This directory contains graphics/, tiles/, dink.ini, etc.",
			       NULL /* default value */,
			       (GParamFlags)(G_PARAM_READABLE));
  g_object_class_install_property (gobject_class,
				   WINDINKEDIT_DMOD_DMODDIR,
				   pspec);

  /* Warn everybody that we're shutting down */
  WindinkeditDmodClass *this_class = WINDINKEDIT_DMOD_CLASS(g_class);
  this_class->close_signal_id =
    g_signal_new("close",
		 G_TYPE_FROM_CLASS(g_class),
		 (GSignalFlags)(G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS),
		 NULL /* class closure */,
		 NULL /* accumulator */,
		 NULL /* accu_data */,
		 g_cclosure_marshal_VOID__VOID /* c_marshaller */,
		 G_TYPE_NONE /* return_type */,
		 0 /* n_params */);
}

GType windinkedit_dmod_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditDmodClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      (GClassInitFunc) ___class_init,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditDmod),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditDmodType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/

static int windinkedit_dmod_read_tile_bmps(WindinkeditDmod* self)
{
  for (int i = 0; i < MAX_TILE_BMPS; i++)
    {
      selfp->tiles[i] = windinkedit_tile_new(i + 1);
    }
  return 1/*true*/;
}


WindinkeditDmod* windinkedit_dmod_new()
{
  return WINDINKEDIT_DMOD(g_object_new(WINDINKEDIT_TYPE_DMOD, NULL));
}

const gchar* windinkedit_dmod_get_refdir(WindinkeditDmod* self)
{
  return selfp->refdir;
}

const gchar* windinkedit_dmod_get_dmoddir(WindinkeditDmod* self)
{
  return selfp->dmoddir;
}

WindinkeditMap* windinkedit_dmod_get_map(WindinkeditDmod* self)
{
  return selfp->map;
}

WindinkeditMinimap* windinkedit_dmod_get_minimap(WindinkeditDmod* self)
{
  return selfp->minimap;
}

WindinkeditSequence* windinkedit_dmod_get_sequence(WindinkeditDmod* self, int idx)
{
  g_return_val_if_fail(idx >= 0 && idx < MAX_SEQUENCES, NULL);

  return selfp->sequences[idx];
}

WindinkeditTile* windinkedit_dmod_get_tile(WindinkeditDmod* self, int idx)
{
  g_return_val_if_fail(idx >= 0 && idx < MAX_TILE_BMPS, NULL);

  return selfp->tiles[idx];
}

int windinkedit_dmod_open(WindinkeditDmod* self, gchar* refdir, gchar* dmoddir)
{
  selfp->refdir = g_strconcat(refdir, "/", NULL);
  //selfp->dmoddir = g_strdup(dmoddir);
  selfp->dmoddir = g_strconcat(dmoddir, "/", NULL);

  selfp->map = WINDINKEDIT_MAP(g_object_new(WINDINKEDIT_TYPE_MAP, "dmod", self, NULL));
  selfp->minimap = WINDINKEDIT_MINIMAP(g_object_new(WINDINKEDIT_TYPE_MINIMAP, "dmod", self, NULL));
  selfp->map->minimap = selfp->minimap;

  current_dmod = self;
  current_map = selfp->map;

  if (!windinkedit_map_parse_dink_dat(selfp->map))
    {
      return 0/*false*/;
    }
  if (!windinkedit_dmod_read_tile_bmps(self))
    {
      return 0/*false*/;
    }
  if (!windinkedit_dmod_parse_dink_ini(self))
    {
      return 0/*false*/;
    }
  if (!windinkedit_map_parse_hard_dat(selfp->map))
    {
      return 0/*false*/;
    }

  windinkedit_minimap_drawSquares(selfp->minimap);

  undo_buffer = new Undo();
  hard_tile_selector = new HardTileSelector();
//  tile_selector = new TileSelector();
//  sprite_selector = windinkedit_sprite_selector_new();

// 	sprite_library.readList();
// 	sprite_selector.createList();
//	mainWnd->GetLeftPane()->PopulateTree();

  return 1/*true*/;
}

int windinkedit_dmod_save(WindinkeditDmod *self)
{
  if (optimize_dink_ini)
    windinkedit_dmod_save_dink_ini(self);
  
  if (!windinkedit_map_save_dink_dat(selfp->map))
    {
      return 0/*false*/;
	}
  if (!windinkedit_map_save_map_dat(selfp->map))
    {
      return 0/*false*/;
    }
  // 	sprite_library.storeList();
  if (!windinkedit_map_save_hard_dat(selfp->map))
    {
      return 0/*false*/;
    }

  return 1/*true*/;
}

static int windinkedit_dmod_free_sequences(WindinkeditDmod *self)
{
  for (int i = 0; i < MAX_SEQUENCES; i++)
    {
      if (selfp->sequences[i])
	{
	  g_object_unref(selfp->sequences[i]);
	  selfp->sequences[i] = NULL;
	}
    }
  return 1/*true*/;
}

static void windinkedit_dmod_free_tiles(WindinkeditDmod *self)
{
  for (int i = 0; i < MAX_TILE_BMPS; i++)
    {
      if (selfp->tiles[i])
	{
	  g_object_unref(selfp->tiles[i]);
	  selfp->tiles[i] = NULL;
	}
    }
}

int windinkedit_dmod_save_dink_ini(WindinkeditDmod *self)
{
	char buffer[PATH_MAX];

	gchar *filename = g_strconcat(selfp->dmoddir, "dink.ini", NULL);
	// open the dink.ini file
	ofstream dink_ini(filename);
	g_free(filename);

	for (int cur_seq = 1; cur_seq <= MAX_SEQUENCES; cur_seq++)
	{
		if (selfp->sequences[cur_seq] != NULL)
		{
			if (selfp->sequences[cur_seq]->now == true)
			{
				//print the load_sequence_now
				dink_ini << "load_sequence_now " << 
					selfp->sequences[cur_seq]->graphics_path << " " <<
					cur_seq << " ";
			} else{
				dink_ini << "load_sequence " << 
					selfp->sequences[cur_seq]->graphics_path << " " <<
					cur_seq << " ";
			}

			//do we need to print all this?
			if (selfp->sequences[cur_seq]->frame_delay ||
				selfp->sequences[cur_seq]->center_x || 
				selfp->sequences[cur_seq]->center_y || 
				selfp->sequences[cur_seq]->left_boundary || 
				selfp->sequences[cur_seq]->top_boundary || 
				selfp->sequences[cur_seq]->right_boundary || 
				selfp->sequences[cur_seq]->bottom_boundary)
			{
				//yes we do...
				dink_ini << selfp->sequences[cur_seq]->frame_delay << " " <<
				selfp->sequences[cur_seq]->center_x << " " <<
				selfp->sequences[cur_seq]->center_y << " " <<
				selfp->sequences[cur_seq]->left_boundary << " " <<
				selfp->sequences[cur_seq]->top_boundary << " " <<
				selfp->sequences[cur_seq]->right_boundary << " " <<
				selfp->sequences[cur_seq]->bottom_boundary << " ";
			}

			//print special attrib's if needed
			if (selfp->sequences[cur_seq]->type == BLACK)
				dink_ini << "BLACK";

			else if (selfp->sequences[cur_seq]->type == NOTANIM)
				dink_ini << "NOTANIM";

			else if (selfp->sequences[cur_seq]->type == LEFTALIGN)
				dink_ini << "LEFTALIGN";

			dink_ini << "\n";
		}
	}

	dink_ini << "\n\n";

	for (int cur_seq = 1; cur_seq < MAX_SEQUENCES; cur_seq++)
	{
		if (selfp->sequences[cur_seq] != NULL)
		{
			for (int cur_frame = 1; cur_frame < MAX_FRAMES; cur_frame++)
			{
				if (selfp->sequences[cur_seq]->frame_info[cur_frame-1] != NULL)
				{
					sprintf(buffer, "set_sprite_info %i %i %i %i %i %i %i %i", cur_seq, cur_frame, selfp->sequences[cur_seq]->frame_info[cur_frame-1]->center_x,
						selfp->sequences[cur_seq]->frame_info[cur_frame-1]->center_y,
						selfp->sequences[cur_seq]->frame_info[cur_frame-1]->left_boundary,
						selfp->sequences[cur_seq]->frame_info[cur_frame-1]->top_boundary,
						selfp->sequences[cur_seq]->frame_info[cur_frame-1]->right_boundary,
						selfp->sequences[cur_seq]->frame_info[cur_frame-1]->bottom_boundary);

					dink_ini << buffer << endl;
				}
			}
		}
	}

	dink_ini << "\n\n";

	for (int cur_seq = 1; cur_seq < MAX_SEQUENCES; cur_seq++)
	{
		if (selfp->sequences[cur_seq] != NULL)
		{
			for (int cur_frame = 1; cur_frame < MAX_FRAMES; cur_frame++)
			{
				if (selfp->sequences[cur_seq]->set_frame_delay[cur_frame-1] != 0)
				{
					sprintf(buffer, "set_frame_delay %i %i %i", cur_seq, cur_frame, selfp->sequences[cur_seq]->set_frame_delay[cur_frame-1]);
					dink_ini << buffer << endl;
				}
				if (selfp->sequences[cur_seq]->set_frame_special[cur_frame-1] != 0)
				{
					sprintf(buffer, "set_frame_special %i %i %i", cur_seq, cur_frame, selfp->sequences[cur_seq]->set_frame_special[cur_frame-1]);
					dink_ini << buffer << endl;
				}
				if (selfp->sequences[cur_seq]->set_frame_frame_seq[cur_frame] != 0)
				{
					sprintf(buffer, "set_frame_frame %i %i %i", cur_seq, cur_frame, selfp->sequences[cur_seq]->set_frame_frame_seq[cur_frame]);
					dink_ini << buffer;

					if (selfp->sequences[cur_seq]->set_frame_frame_seq[cur_frame] != -1)
						dink_ini << " " << selfp->sequences[cur_seq]->set_frame_frame_frame[cur_frame];

					dink_ini << endl;
				}
			}
		}
	}
	dink_ini.close();

	return true;
}

int windinkedit_dmod_parse_dink_ini(WindinkeditDmod *self)
{
  gchar *filename = g_strconcat(selfp->dmoddir, "dink.ini", NULL);
  // open the dink.ini file
  ifstream dink_ini(filename);
  g_free(filename);

	char input_line[MAX_LINE_LENGTH];
	char parse_inputs[MAX_PARSE_INPUTS][MAX_LINE_LENGTH];

	int num_tokens = 0;

	int num_special_frames = 0;

	while (dink_ini.getline(input_line, MAX_LINE_LENGTH))
	{
		// quick hack to make getline() works like woe's
		if (input_line[strlen(input_line)-1] == '\r')
			input_line[strlen(input_line)-1] = '\0';
		
		// now let's process the text from this line

		// check for comments or blank lines and skip them
		if (input_line[0] == ';' || input_line[0] == '/' || strlen(input_line) == 0)
			continue;

		// parse line into up to MAX_PARSE_INPUTS strings
		char *token;
		token = strtok(input_line, " ,\t,\n");
		if (token == NULL)
			continue;
		strcpy(parse_inputs[0], token);
		
		for (num_tokens = 1; num_tokens < MAX_PARSE_INPUTS; num_tokens++)
		{
			token = strtok(NULL, " ,\t,\n");
			if (token == NULL)
				break;
			strcpy(parse_inputs[num_tokens], token);
		}

		if (num_tokens > 2)
		{
			// process command: load_sequence_now
			if (strcasecmp("load_sequence_now", parse_inputs[0]) == 0 || strcasecmp("load_sequence", parse_inputs[0]) == 0)
			{
				bool now;

				if (strcasecmp("load_sequence_now", parse_inputs[0]) == 0)
					now = true;
				else
					now = false;

				int frame_delay = 0;
				int center_x = 0;
				int center_y = 0;
				int left_boundary = 0;
				int top_boundary = 0;
				int right_boundary = 0;
				int bottom_boundary = 0;
				int type = 0;

				int sequence_num = atoi(parse_inputs[2]);

				if (num_tokens >= 10)
				{
					frame_delay = atoi(parse_inputs[3]);
					center_x = atoi(parse_inputs[4]);
					center_y = atoi(parse_inputs[5]);
					left_boundary = atoi(parse_inputs[6]);
					top_boundary = atoi(parse_inputs[7]);
					right_boundary = atoi(parse_inputs[8]);
					bottom_boundary = atoi(parse_inputs[9]);
				//	type = NOTANIM;
				}
				else if (num_tokens != 3)
				{
					if (strcasecmp("BLACK", parse_inputs[3]) == 0)
					{
						type = BLACK;
					}
					else if (strcasecmp("NOTANIM", parse_inputs[3]) == 0)
					{
						type = NOTANIM;
					}
					else if (strcasecmp("LEFTALIGN", parse_inputs[3]) == 0)
					{
						type = LEFTALIGN;
					}
					else
					{
						frame_delay = atoi(parse_inputs[3]);
					}
				}

				if (selfp->sequences[sequence_num] != NULL)
					continue;

				selfp->sequences[sequence_num] = windinkedit_sequence_new(self, sequence_num + 1, parse_inputs[1], frame_delay, type,
						center_x, center_y, left_boundary, top_boundary, right_boundary, bottom_boundary, now);
			}
			else if (strcasecmp("set_sprite_info", parse_inputs[0]) == 0)
			{
				int cur_sequence = atoi(parse_inputs[1]);
				// check if sequence exists first
				if (selfp->sequences[cur_sequence] == NULL)
					continue;
				
				int frame_num = atoi(parse_inputs[2]) - 1;

				windinkedit_sequence_addFrameInfo(selfp->sequences[cur_sequence], frame_num,
									   atoi(parse_inputs[3]),
									   atoi(parse_inputs[4]),
									   atoi(parse_inputs[5]),
									   atoi(parse_inputs[6]),
									   atoi(parse_inputs[7]),
									   atoi(parse_inputs[8]));

				num_special_frames++;
			}
			else if (strcasecmp("set_frame_frame", parse_inputs[0]) == 0)
			{
				int dest_sequence = atoi(parse_inputs[1]);
				int dest_frame = atoi(parse_inputs[2]);
				int cur_sequence = atoi(parse_inputs[3]);
				int cur_frame = atoi(parse_inputs[4]);

				if (selfp->sequences[dest_sequence] == NULL)
					continue;

				selfp->sequences[dest_sequence]->set_frame_frame_seq[dest_frame] = cur_sequence;

				if (cur_sequence != -1)
				  selfp->sequences[dest_sequence]->set_frame_frame_frame[dest_frame] = cur_frame;
			}
			else if (strcasecmp("set_frame_delay", parse_inputs[0]) == 0)
			{
				int cur_sequence = atoi(parse_inputs[1]);
				int cur_frame = atoi(parse_inputs[2]);
				int delay = atoi(parse_inputs[3]);

				if (selfp->sequences[cur_sequence] == NULL)
					continue;

				selfp->sequences[cur_sequence]->set_frame_delay[cur_frame-1] = delay;
			}
			else if (strcasecmp("set_frame_special", parse_inputs[0]) == 0)
			{
				int cur_sequence = atoi(parse_inputs[1]);
				int cur_frame = atoi(parse_inputs[2]);
				int hit = atoi(parse_inputs[3]);

				if (selfp->sequences[cur_sequence] == NULL)
					continue;

				selfp->sequences[cur_sequence]->set_frame_special[cur_frame-1] = hit;
			}
		}
	} // end while loop

	if (num_special_frames >= MAX_FRAME_INFOS)
	{
	  printf("Warning: Too many set_sprite_info commands");
	  printf("Note that you have exceeded the maximum number of \"set_sprite_info\" commands by %d. These will still work in WinDinkedit but will be ignored by Dinkedit and the Dink engine. Enable Optimize Dink.ini in the options menu and WDE will attempt to fix this on the next save.", num_special_frames - MAX_FRAME_INFOS + 1);
	}

	return true;
}
