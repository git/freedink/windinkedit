/**
 * Main D-Mod data structure

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _WINDINKEDIT_MAP_H
#define _WINDINKEDIT_MAP_H

#include <glib-object.h>
#include "Globals.h" /* NUM_SCREENS... */
#include "windinkedit-tile.h"
#include "Structs.h" /* TILEDATA */

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_MAP		  (windinkedit_map_get_type ())
#define WINDINKEDIT_MAP(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_MAP, WindinkeditMap))
#define WINDINKEDIT_MAP_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_MAP, WindinkeditMapClass))
#define WINDINKEDIT_IS_MAP(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_MAP))
#define WINDINKEDIT_IS_MAP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_MAP))
#define WINDINKEDIT_MAP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_MAP, WindinkeditMapClass))

#ifndef _WINDINKEDIT_TYPEDEF_MAP
#define _WINDINKEDIT_TYPEDEF_MAP
typedef struct _WindinkeditMap WindinkeditMap;
#endif
typedef struct _WindinkeditMapClass WindinkeditMapClass;
typedef struct _WindinkeditMapPrivate WindinkeditMapPrivate;

/* Avoid including full .h headers just for pointers */
#ifndef _WINDINKEDIT_TYPEDEF_MINIMAP
#define _WINDINKEDIT_TYPEDEF_MINIMAP
typedef struct _WindinkeditMinimap WindinkeditMinimap;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_SCREEN
#define _WINDINKEDIT_TYPEDEF_SCREEN
typedef struct _WindinkeditScreen WindinkeditScreen;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_SPRITE
#define _WINDINKEDIT_TYPEDEF_SPRITE
typedef struct _WindinkeditSprite WindinkeditSprite;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_SEQUENCE
#define _WINDINKEDIT_TYPEDEF_SEQUENCE
typedef struct _WindinkeditSequence WindinkeditSequence;
#endif

struct _WindinkeditMap {
  GObject parent;

  //gchar *dmod_path;	// pathname to dmod directory
  gchar *import_dmod_path;
    
  WindinkeditMinimap* minimap;
    
  int render_state;
  int screenMode;
  WindinkeditSprite* mouseSprite;
    
  WindinkeditScreen* screen[NUM_SCREENS];
    
  int midi_num[NUM_SCREENS];
  int indoor[NUM_SCREENS];
  int/*bool*/ miniupdated[NUM_SCREENS];
    
  // coordinates of window in relation to position on map
  struct rect window; // absolute (x,y) pos from screen#1(0,0)
  int window_width;
  int window_height;
  int cur_vision;
    
  /*< private >*/
  WindinkeditMapPrivate* _priv;
};

struct _WindinkeditMapClass {
  GObjectClass parent;
};

/* used by WINDINKEDIT_TYPE_MAP */
GType windinkedit_map_get_type (void);


/* API. */
int windinkedit_map_drawScreens(WindinkeditMap *self, SDL_Surface* surface);

WindinkeditSprite* windinkedit_map_getMouseSprite(WindinkeditMap *self);
void    windinkedit_map_loadMouseSprite(WindinkeditMap *self, WindinkeditSprite* sprite, int screen_num);
void    windinkedit_map_setMouseSprite(WindinkeditMap *self, WindinkeditSprite* sprite);
  
void windinkedit_map_placeTiles(WindinkeditMap *self, int first_x_tile, int first_y_tile, int width, int height, 
				struct TILEDATA tile_set[SCREEN_TILE_WIDTH * 2][SCREEN_TILE_HEIGHT * 2]);

void windinkedit_map_updateMapPosition(WindinkeditMap *self, int x, int y);
void windinkedit_map_setVision(WindinkeditMap *self, int new_vision);

WindinkeditScreen* windinkedit_map_get_screen(WindinkeditMap* self, int screen_maths);
void windinkedit_map_set_screen(WindinkeditMap* self, int screen_maths, WindinkeditScreen* screen);
void windinkedit_map_set_graphics_size(WindinkeditMap* self, int new_width, int new_height);

int windinkedit_map_parse_dink_dat(WindinkeditMap *self);
int windinkedit_map_parse_map_dat(WindinkeditMap *self, int *screen_order);
int windinkedit_map_save_dink_dat(WindinkeditMap *self);
int windinkedit_map_save_map_dat(WindinkeditMap *self);

int windinkedit_map_save_hard_dat(WindinkeditMap *self);
int windinkedit_map_parse_hard_dat(WindinkeditMap *self);

G_END_DECLS

#endif
