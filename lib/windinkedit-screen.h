#ifndef WINDINKEDIT_SCREEN_H
#define WINDINKEDIT_SCREEN_H

#include <glib-object.h>
#include "windinkedit-sprite.h"
#include "windinkedit-tile.h"
#include "Globals.h"
#include "Structs.h"

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_SCREEN         (windinkedit_screen_get_type ())
#define WINDINKEDIT_SCREEN(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_SCREEN, WindinkeditScreen))
#define WINDINKEDIT_SCREEN_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_SCREEN, WindinkeditScreenClass))
#define WINDINKEDIT_IS_SCREEN(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_SCREEN))
#define WINDINKEDIT_IS_SCREEN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_SCREEN))
#define WINDINKEDIT_SCREEN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_SCREEN, WindinkeditScreenClass))

#ifndef _WINDINKEDIT_TYPEDEF_SCREEN
#define _WINDINKEDIT_TYPEDEF_SCREEN
typedef struct _WindinkeditScreen WindinkeditScreen;
#endif
typedef struct _WindinkeditScreenClass WindinkeditScreenClass;
typedef struct _WindinkeditScreenPrivate WindinkeditScreenPrivate;
  
struct _WindinkeditScreen {
  GObject parent;
  struct TILEDATA tiles[SCREEN_TILE_HEIGHT][SCREEN_TILE_WIDTH];
  WindinkeditSprite* sprite[MAX_SPRITES];
  char script[BASE_SCRIPT_MAX_LENGTH];

  /*< private >*/
  WindinkeditScreenPrivate* _priv;
};

struct _WindinkeditScreenClass {
  GObjectClass parent;
};


/* used by WINDINKEDIT_TYPE_SCREEN */
GType windinkedit_screen_get_type(void);

WindinkeditScreen* windinkedit_screen_new(WindinkeditDmod *context);
WindinkeditScreen* windinkedit_screen_copy(WindinkeditScreen* self);
WindinkeditSprite* windinkedit_screen_getSprite(WindinkeditScreen* self, int x, int y, int cur_vision, int* psprite_num);
int windinkedit_screen_addSprite(WindinkeditScreen* self, int sprite_num, WindinkeditSprite *new_sprite);
void windinkedit_screen_removeSprite(WindinkeditScreen* self, int sprite_num);
int windinkedit_screen_findFirstAvailableSpriteSlot(WindinkeditScreen* self);
int windinkedit_screen_drawTiles(WindinkeditScreen* self, SDL_Surface* surface, int offset_x, int offset_y);
int windinkedit_screen_drawSprites(WindinkeditScreen* self, int cur_vision,
				   SDL_Surface* surface, int offset_x, int offset_y);
int windinkedit_screen_drawHardTiles(WindinkeditScreen* self, int x_offset, int y_offset);
int windinkedit_screen_drawHardSprites(WindinkeditScreen* self, int x_offset, int y_offset, struct rect* clip_rect, int cur_vision);

G_END_DECLS

#endif
