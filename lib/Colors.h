/**
 * Default colors definition

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _COLORS_H_
#define _COLORS_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "SDL.h"

// PROTOTYPES /////////////////////////////////////////////
typedef struct COLORREF
{
  Uint8 r;
  Uint8 g;
  Uint8 b;
} COLORREF;

void setColors();
Uint32 DDColorMatch(SDL_Surface * pdds, Uint8 r, Uint8 g, Uint8 b);

// COLORS

extern Uint32 color_black;
extern Uint32 color_white;
extern Uint32 color_red;
extern Uint32 color_green;
extern Uint32 color_blue;
extern Uint32 color_yellow;

extern Uint32 color_tile_grid;
extern Uint32 color_mouse_box;
extern Uint32 color_blank_screen_fill;
extern Uint32 color_tile_grid_in_boundary;
extern Uint32 color_tile_grid_out_boundary;
extern Uint32 color_map_screen_grid;
extern Uint32 color_minimap_screen_selected;
extern Uint32 color_screen_selected_box;
extern Uint32 color_sprite_info_text;
extern Uint32 color_sprite_hover_box;

extern Uint32 color_sprite_hardness;
extern Uint32 color_warp_sprite_hardness;
extern Uint32 color_normal_tile_hardness;
extern Uint32 color_low_tile_hardness;
extern Uint32 color_other_tile_hardness;

extern Uint32 color_polygon_fill_hardness;

extern Uint32 help_text_color;

extern int colorformat;

#ifdef __cplusplus
}
#endif

#endif
