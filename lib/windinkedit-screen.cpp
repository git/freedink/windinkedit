#include "windinkedit-screen.h"

#include "SDL.h"

#include "rect.h"
#include "Engine.h"
#include "Globals.h"
#include "windinkedit-dmod.h"
#include "windinkedit-map.h"
#include "windinkedit-sequence.h"
#include "windinkedit-sprite.h"
#include "windinkedit-tile.h"
#include "HardTileSelector.h"
#include "Common.h"


/*************/
/**  Class  **/
/**         **/
/*************/

static GObjectClass *parent_class = NULL;

enum {
  WINDINKEDIT_SCREEN_CONSTRUCT_DMOD = 1,
};


/** Private fields **/

#define selfp (self->_priv)

struct _WindinkeditScreenPrivate {
  WindinkeditDmod* dmod;
};


static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditScreen* self = (WindinkeditScreen*)instance;

  self->_priv = G_TYPE_INSTANCE_GET_PRIVATE(self, WINDINKEDIT_TYPE_SCREEN, WindinkeditScreenPrivate);

  // initialize the sprites
  memset(self->sprite, 0, MAX_SPRITES * sizeof(WindinkeditSprite*));
  
  // initialize tiles (bitmaps and hardness)
  memset(self->tiles, 0, SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH * sizeof(TILEDATA));
  
  // set base script to nothing.
  memset(self->script, 0, BASE_SCRIPT_MAX_LENGTH);
}

static void
___get_property(GObject      *object,
		guint         property_id,
		GValue       *value,
		GParamSpec   *pspec)
{
  WindinkeditScreen *self = (WindinkeditScreen *) object;
  
  switch (property_id) {
  case WINDINKEDIT_SCREEN_CONSTRUCT_DMOD:
    g_value_set_object(value, selfp->dmod);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

static void
___set_property (GObject      *object,
		 guint         property_id,
		 const GValue *value,
		 GParamSpec   *pspec)
{
  WindinkeditScreen *self = (WindinkeditScreen *) object;
  
  switch (property_id) {
  case WINDINKEDIT_SCREEN_CONSTRUCT_DMOD:
    g_return_if_fail(selfp->dmod == NULL);
    selfp->dmod = WINDINKEDIT_DMOD(g_value_get_object(value));
    /* dmod is passed by reference */
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

/**
 * Clean-up object (after dispose)
 */
static void
___finalize(GObject *obj)
{
  WindinkeditScreen *self = (WindinkeditScreen *)obj;

  // free all the sprites on the screen
  for (int i = 0; i < MAX_SPRITES; i++)
    {
      if (self->sprite[i])
	{
	  g_object_unref(self->sprite[i]);
	  self->sprite[i] = NULL;
	}
    }

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->finalize(obj);
}

static void
___class_init (gpointer g_class,
	       gpointer g_class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
  WindinkeditScreenClass *klass = WINDINKEDIT_SCREEN_CLASS (g_class);
  GParamSpec *pspec;

  gobject_class->set_property = ___set_property;
  gobject_class->get_property = ___get_property;
  gobject_class->finalize = ___finalize;

  g_type_class_add_private(g_class, sizeof(WindinkeditScreenPrivate));
  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));

  pspec = g_param_spec_object("dmod",
			      "D-Mod that contains this screen",
			      "Allow to get D-Mod properties such as its path",
			      WINDINKEDIT_TYPE_DMOD /* type */,
			      (GParamFlags)(G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));
  g_object_class_install_property(gobject_class,
				  WINDINKEDIT_SCREEN_CONSTRUCT_DMOD,
				  pspec);
}

GType windinkedit_screen_get_type (void)
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditScreenClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      (GClassInitFunc) ___class_init,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditScreen),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditScreenType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/

WindinkeditScreen* windinkedit_screen_new(WindinkeditDmod *context)
{
  return WINDINKEDIT_SCREEN(g_object_new(WINDINKEDIT_TYPE_SCREEN, "dmod", context, NULL));
}

WindinkeditScreen* windinkedit_screen_copy(WindinkeditScreen* self)
{
  WindinkeditScreen* copy_screen = WINDINKEDIT_SCREEN(g_object_new(WINDINKEDIT_TYPE_SCREEN, NULL));

  // copy tiles
  memcpy(self->tiles, copy_screen->tiles, sizeof(TILEDATA) * SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH);
  
  // initialize the sprites
  memset(self->sprite, 0, MAX_SPRITES * sizeof(int));
  
  // now copy over the ones necessary
  int cur_sprite = 0;
  for (int i = 0; i < MAX_SPRITES; i++)
    {
      if (copy_screen->sprite[i])
	{
	  self->sprite[cur_sprite] = windinkedit_sprite_copy(copy_screen->sprite[i]);
	  cur_sprite++;
	}
    }
  
  // copy over the base script for the screen
  memcpy(self->script, copy_screen->script, BASE_SCRIPT_MAX_LENGTH);
}

// draw all the hardness for the tiles on the screen
int windinkedit_screen_drawHardTiles(WindinkeditScreen* self, int x_offset, int y_offset)
{
	int hard_tile_num;

	for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)
	{
		for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
		{
		  TILEDATA* cur_tile_use = &self->tiles[y][x];
		  if (cur_tile_use->alt_hardness != 0)
		    {
		      // use the user specified hardness
		      hard_tile_num = cur_tile_use->alt_hardness;
		    }
		  else
		    {
		      // use the default hardness for this tile
		      WindinkeditTile* cur_tile = windinkedit_dmod_get_tile(selfp->dmod, cur_tile_use->bmp);
		      hard_tile_num = cur_tile->tile_hardness[cur_tile_use->y][cur_tile_use->x];
		    }
		  
		  // render the tile
		  // 			current_map->hard_tile_selector.renderHardness(x * TILEWIDTH + x_offset, y * TILEHEIGHT + y_offset, 
		  // 				hard_tile_num, true);
		}
	}
	return true;
}

// draw all the sprite hardboxes for the screen
int windinkedit_screen_drawHardSprites(WindinkeditScreen* self, int x_offset, int y_offset, struct rect* clip_rect, int cur_vision)
{
	WindinkeditSequence *cur_seq;
	// read in the sprites to a sprite list so we can sort for depth que
	for (int i = 0; i < MAX_SPRITES; i++)
	{
		// render the sprite
		if (self->sprite[i] != NULL)
		{
		  // if vision doesn't match, go on to the next possible sprite
		  int sprite_vision = windinkedit_sprite_getVision(self->sprite[i]);
		  if (sprite_vision != cur_vision && sprite_vision != 0)
		    continue;
		  
		  cur_seq = windinkedit_dmod_get_sequence(selfp->dmod, self->sprite[i]->sequence);
		  if (cur_seq != NULL)
		    {
		      windinkedit_sequence_clipRenderHardness(cur_seq, x_offset, y_offset, self->sprite[i], clip_rect);
		    }
		}
	}

	return true;
}

// draw all the tiles for the screen
int windinkedit_screen_drawTiles(WindinkeditScreen* self, SDL_Surface* surface, int offset_x, int offset_y)
{
  SDL_Rect dstrect = {0, 0, TILEWIDTH, TILEHEIGHT};
  for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)
    {
      dstrect.x = 0;
      for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
	{
	  // render the tile
	  TILEDATA* cur_tile_use = &self->tiles[y][x];
	  WindinkeditTile* cur_tile = windinkedit_dmod_get_tile(selfp->dmod, cur_tile_use->bmp);
	  SDL_Rect dstrect_offset = dstrect;
	  dstrect_offset.x += offset_x;
	  dstrect_offset.y += offset_y;
	  windinkedit_tile_render(cur_tile, cur_tile_use->x, cur_tile_use->y, surface, &dstrect_offset);
	  
	  dstrect.x += TILEWIDTH;
	}
      dstrect.y += TILEHEIGHT;
    }
  return 1/*true*/;
}

///////////////////////////////////////////////////////////

// draw all the sprites to the screen
int windinkedit_screen_drawSprites(WindinkeditScreen* self, int cur_vision,
				   SDL_Surface* surface, int offset_x, int offset_y)
{
  struct SPRITEQUE
  {
    WindinkeditSprite* sprite;
    int depth, sprite_num;
  };

  int mouseSprite_x_old, mouseSprite_y_old;
  
  SPRITEQUE sprite_list[MAX_SPRITES + 1];
  memset(sprite_list, 0, (MAX_SPRITES + 1) * sizeof(int));
  
  // background sprites
  SPRITEQUE background_sprite_list[MAX_SPRITES + 1];
  memset(background_sprite_list, 0, (MAX_SPRITES + 1) * sizeof(int));
  
  int sprite_vision;
  
  // read in the sprites to a sprite list so we can sort for depth que
  int sprite_que_length = 0;
  int background_sprite_que_length = 0;
  int temp_depth;
  for (int i = 0; i < MAX_SPRITES; i++)
    {
      // render the sprite
      if (self->sprite[i] != NULL)
	{
	  sprite_vision = windinkedit_sprite_getVision(self->sprite[i]);
	  
	  // used for tracking visions on the sidebar
	  if (sprite_vision < VISIONS_TRACKED)
	    new_vision_used[sprite_vision] = 1;
	  
	  // don't draw the sprite if it's vision doesn't match the current one
	  // unless the sprite's vision is 0 (appears on all screens)
	  if (sprite_vision != cur_vision && sprite_vision != 0)
	    continue;
	  
	  // set sprite depth to it's y coordinate if it has a depth of 0
	  if (self->sprite[i]->depth == 0)
	    temp_depth = self->sprite[i]->y;
	  else
	    temp_depth = self->sprite[i]->depth;
	  
	  if (self->sprite[i]->type == 0)
	    {
	      // background sprite
	      background_sprite_list[background_sprite_que_length].depth = temp_depth;
	      background_sprite_list[background_sprite_que_length].sprite = self->sprite[i];
	      background_sprite_list[background_sprite_que_length].sprite_num = i;
	      background_sprite_que_length++;
	    }
	  else
	    {
	      // foreground sprite
	      sprite_list[sprite_que_length].depth = temp_depth;
	      sprite_list[sprite_que_length].sprite = self->sprite[i];
	      sprite_list[sprite_que_length].sprite_num = i;
	      sprite_que_length++;
	    }
	}
    }
  
  WindinkeditSprite* mouseSprite = windinkedit_map_getMouseSprite(windinkedit_dmod_get_map(selfp->dmod));
  if (mouseSprite != NULL)
    {
      mouseSprite_x_old = mouseSprite->x;
      mouseSprite_y_old = mouseSprite->y;

//		mouseSprite->x -= x_offset;
//		mouseSprite->y -= y_offset;

      // compensate for screen gap
      compensateScreenGap(mouseSprite->x, mouseSprite->y);
      
      if (mouseSprite->depth == 0)
	temp_depth = mouseSprite->y;
      else
	temp_depth = mouseSprite->depth;
      
      if (mouseSprite->type == 0)
	{
	  // background sprite
	  background_sprite_list[background_sprite_que_length].depth = temp_depth;
	  background_sprite_list[background_sprite_que_length].sprite = mouseSprite;
	  background_sprite_que_length++;
	}
      else
	{
	  // foreground sprite
	  sprite_list[sprite_que_length].depth = temp_depth;
	  sprite_list[sprite_que_length].sprite = mouseSprite;
	  sprite_que_length++;
	}
    }
  
  // now sort the sprites according to their depth que
  // this is a small list, use insertion sort
  int in;
  SPRITEQUE temp;
  
  // sort background sprites first
  // lowest depth ques first
  for (int i = 1; i < background_sprite_que_length; i++)
    {
      temp = background_sprite_list[i];
      for (in = i; in > 0 && background_sprite_list[in-1].depth > temp.depth; in--)
	{
	  background_sprite_list[in] = background_sprite_list[in-1];
	}
      background_sprite_list[in] = temp;
    }
  
  if (sprite_que_length > 0)
    {
      background_sprite_list[background_sprite_que_length] = sprite_list[0];
      
      // lowest depth ques first
      for (int i = 1; i < sprite_que_length; i++)
	{
	  temp = sprite_list[i];
	  for (in = i + background_sprite_que_length; in > background_sprite_que_length && background_sprite_list[in-1].depth > temp.depth; in--)
	    {
	      background_sprite_list[in] = background_sprite_list[in-1];
	    }
	  background_sprite_list[in] = temp;
	}
    }
  
  sprite_que_length += background_sprite_que_length;
  
  
  WindinkeditSprite* temp_sprite;
  WindinkeditSequence *cur_seq;
  int sprite_num;
  
  // now print out the sprites
  for (int i = 0; i < sprite_que_length; i++)
    {
      temp_sprite = background_sprite_list[i].sprite;
      sprite_num = background_sprite_list[i].sprite_num;
      cur_seq = windinkedit_dmod_get_sequence(selfp->dmod, temp_sprite->sequence);
      
      if (cur_seq != NULL)
	windinkedit_sequence_clipRender(cur_seq, temp_sprite, sprite_num, show_sprite_info, surface, offset_x, offset_y);
    }
  
  // restore old mousesprite position
  if (mouseSprite != NULL)
    {
      mouseSprite->x = mouseSprite_x_old;
      mouseSprite->y = mouseSprite_y_old;
      g_object_unref(mouseSprite);
    }
  
  return true;
}

// returns the sprite located at the x and y locations and detaches it from the screen if erase=true
WindinkeditSprite* windinkedit_screen_getSprite(WindinkeditScreen* self, int x, int y, int cur_vision, int* psprite_num)
{
  SDL_Rect bounds; 
  
  int old_area = 9999999; //silly rig
  *psprite_num = -1; 
  
  for (int i = 0; i < MAX_SPRITES; i++) 
    { 
      // check if sprite exists 
      if (self->sprite[i] != NULL && (windinkedit_sprite_getVision(self->sprite[i]) == 0 ||  
				      windinkedit_sprite_getVision(self->sprite[i]) == cur_vision))
	{ 
	  if (windinkedit_sprite_getImageBounds(self->sprite[i], &bounds) == /*TRUE*/1) 
	    { 
	      // check bounds 
	      if ((y > bounds.y) & (y < bounds.y + bounds.h) & (x > bounds.x) & (x < bounds.x + bounds.w)) 
		{ 
		  int area = bounds.h * bounds.w;
		  
		  if (area < old_area) 
		    { 
		      old_area = area; 
		      *psprite_num = i; 
		    } 
		} 
	    } 
	} 
    } 
  
  if (*psprite_num == -1)
    return NULL;
  
  return WINDINKEDIT_SPRITE(g_object_ref(self->sprite[*psprite_num]));
}

// places a sprite on the screen
int windinkedit_screen_addSprite(WindinkeditScreen* self, int sprite_num, WindinkeditSprite *new_sprite)
{
	int available_slot = sprite_num;

	for (; available_slot < MAX_SPRITES-1; available_slot++)
	{
		// check if sprite exists
		if (self->sprite[available_slot] == NULL)
		{
			break;
		}
	}

	if (available_slot == MAX_SPRITES-1)
	{
//	  ::wxMessageBox(wxT("Dink only allows 100 sprites per screen. Please delete some and try again."),
//								 wxT("Error too many sprites"));
		return false;
	}

	// make another mousesprite to add to the next screen
	WindinkeditSprite* temp = self->sprite[available_slot] = windinkedit_sprite_copy(new_sprite);

	return true;
}

// deletes a sprite from the screen
void windinkedit_screen_removeSprite(WindinkeditScreen* self, int sprite_num)
{
	if (self->sprite[sprite_num])
	{
	  g_object_unref(self->sprite[sprite_num]);
	  self->sprite[sprite_num] = NULL;
	}
}

// returns the number of the first available sprite slot
int windinkedit_screen_findFirstAvailableSpriteSlot(WindinkeditScreen* self)
{
	for (int i = 0; i < MAX_SPRITES; i++)
	{
		// check if sprite exists
		if (self->sprite[i] == NULL)
		{
			return i;
		}
	}

	return -1;
}
