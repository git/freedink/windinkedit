/**
 * 728 screens, with tiles and sprites

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "windinkedit-map.h"

#include "SDL.h"
#include "Engine.h"
#include "Undo.h"
#include "windinkedit-dmod.h"
#include "windinkedit-minimap.h"
#include "windinkedit-sequence.h"
// #include "windinkedit-sprite.h"
// #include "windinkedit-sprite-selector.h"
// #include "windinkedit-tile.h"
#include "MainFrame.h"
#include "Common.h" /* compensateScreenGap() */
#include "Colors.h" /* color_screen_selected_box, color_blank_screen_fill */
#include "SDL_port.h" /* fill_rect() */
#include "Tools.h" /* printScreen() */
#include "HardTileSelector.h"
#include "Modes.h"
#include "TileSelector.h"

#include <fstream>
using namespace std;

/* Functions not currently used by the rest of the ported code */
int windinkedit_map_placeSprite(WindinkeditMap *self, int screen, int x, int y, int stamp_p);
int windinkedit_map_pickupSprite(WindinkeditMap *self);
WindinkeditSprite* windinkedit_map_getSpriteClick(WindinkeditMap *self, int* psprite_num);
void windinkedit_map_updateHoverPosition(WindinkeditMap *self, int x, int y);
void windinkedit_map_deleteScreen(WindinkeditMap *self, int screen_num);
void windinkedit_map_newScreen(WindinkeditMap *self, int screen_num);
void windinkedit_map_pasteScreen(WindinkeditMap *self, int screen_num, WindinkeditScreen* source_screen, int midi_num, int indoor);
void windinkedit_map_changeScreenProperties(WindinkeditMap *self, int screen_num, const char* script, int midi_num, int indoor);
void windinkedit_map_changeSpriteProperties(WindinkeditMap *self, int screen_num, int sprite_num, WindinkeditSprite* new_sprite, WindinkeditSprite* old_sprite);

void windinkedit_map_undoAction(WindinkeditMap *self);
void windinkedit_map_redoAction(WindinkeditMap *self);

void windinkedit_map_trimSprite(WindinkeditMap *self, struct rect trim);
void windinkedit_map_screenshot(WindinkeditMap *self, char* filename);
static void windinkedit_map_drawScreenOnMinimap(WindinkeditMap *self, int my_screen);
void windinkedit_map_toggleAutoUpdateMinimap(WindinkeditMap *self);

// io functions
int windinkedit_map_open_dmod(WindinkeditMap *self);
int windinkedit_map_read_tile_bmps(WindinkeditMap *self);

int  windinkedit_map_deallocate_sequences(WindinkeditMap *self);
int  windinkedit_map_deallocate_map(WindinkeditMap *self);
void windinkedit_map_release_tile_bmps(WindinkeditMap *self);
// end io functions

void windinkedit_map_lock_screen(WindinkeditMap *self);
void windinkedit_map_unlock_screen(WindinkeditMap *self);


/*************/
/**  Class  **/
/**         **/
/*************/

static GObjectClass *parent_class = NULL;

enum {
  WINDINKEDIT_MAP_CONSTRUCT_DMOD = 1,
};


/** Private fields **/

#define selfp (self->_priv)

struct _WindinkeditMapPrivate {
  WindinkeditDmod* dmod;
  /* If != -1, 'locked_screen' is the screen where sprites will be
     placed (when stamping a new sprite or moving an existing one). Or
     at least that was the original intent, but due to the way keys
     events are repeted when 'shift' is left pressed, 'locked_screen'
     is always equal to the mouse hover screen, which defeats the
     point of this feature. It still cancels 'screen_match'.. */
  int locked_screen;
  /* 	DDBLTFX	blankscreen;	// used for drawing a blank screen */
  int screen_hover, x_hover, y_hover;	// x and y coordinates of mouse in relation to current screen
  int screen_x_hover, screen_y_hover;	// gives the row and column of the screen
  int screen_x_offset, screen_y_offset;	// how many pixels off the upper left screen is offset
  WindinkeditSprite* hover_sprite;
  int hover_sprite_num;
  struct rect screens_displayed;
  
  bool screenmatch;
  
  // postponement between mouse click and sprite center
  int mouseSprite_dx;
  int mouseSprite_dy;
  int cur_screen;
};


static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditMap* self = (WindinkeditMap*)instance;

  self->_priv = G_TYPE_INSTANCE_GET_PRIVATE(self, WINDINKEDIT_TYPE_MAP, WindinkeditMapPrivate);

  need_resizing = true;
  self->cur_vision = 0;
  selfp->locked_screen = -1;
  selfp->screenmatch = false;
  self->render_state = SHOW_MINIMAP;
  self->screenMode = SPRITE_MODE;
  
  selfp->hover_sprite = NULL;
  selfp->hover_sprite_num = -1;
  
  selfp->screen_x_hover = 0;
  selfp->screen_y_hover = 0;
  selfp->screen_hover = 0;
  selfp->x_hover = 0;
  selfp->y_hover = 0;
  
  self->mouseSprite = NULL;
  
  // set the map position to the upper left corner, necessary???
  self->window.left = 0;
  self->window.top = 0;
  
  memset(vision_used, 0, VISIONS_TRACKED);
  memset(&self->miniupdated, 0, sizeof(self->miniupdated));
}

/**
 * Run after properties are set
 */
static void
___constructed(GObject* object)
{
  WindinkeditMap* self = WINDINKEDIT_MAP(object);
  
}


static void
___get_property(GObject      *object,
		guint         property_id,
		GValue       *value,
		GParamSpec   *pspec)
{
  WindinkeditMap *self = (WindinkeditMap *) object;
  
  switch (property_id) {
  case WINDINKEDIT_MAP_CONSTRUCT_DMOD:
    g_value_set_object(value, selfp->dmod);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

static void
___set_property (GObject      *object,
		 guint         property_id,
		 const GValue *value,
		 GParamSpec   *pspec)
{
  WindinkeditMap *self = (WindinkeditMap *) object;
  
  switch (property_id) {
  case WINDINKEDIT_MAP_CONSTRUCT_DMOD:
    g_return_if_fail(selfp->dmod == NULL);
    selfp->dmod = WINDINKEDIT_DMOD(g_value_get_object(value));
    /* dmod is passed by reference */
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
    break;
  }
}

static void
___dispose (GObject *obj)
{
  WindinkeditMap *self = (WindinkeditMap *)obj;
  
  // mouse_sprite
  if (self->mouseSprite != NULL)
    {
      delete self->mouseSprite;
      self->mouseSprite = NULL;
    }

  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->dispose(obj);
}

static void
___finalize (GObject *obj)
{
  WindinkeditMap *self = (WindinkeditMap *)obj;
  
  windinkedit_map_deallocate_map(self);
  
  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->finalize(obj);
}


static void
___class_init (gpointer g_class,
			    gpointer g_class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(g_class);
  WindinkeditMapClass *klass = WINDINKEDIT_MAP_CLASS(g_class);
  GParamSpec *pspec;
  
  gobject_class->constructed = ___constructed;
  gobject_class->set_property = ___set_property;
  gobject_class->get_property = ___get_property;
  gobject_class->dispose = ___dispose;
  gobject_class->finalize = ___finalize;

  g_type_class_add_private(g_class, sizeof(WindinkeditMapPrivate));
  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));

  pspec = g_param_spec_object("dmod",
			      "D-Mod that contains this map",
			      "Allow to get D-Mod properties such as its path",
			      WINDINKEDIT_TYPE_DMOD /* type */,
			      (GParamFlags)(G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE));
  g_object_class_install_property(gobject_class,
				  WINDINKEDIT_MAP_CONSTRUCT_DMOD,
				  pspec);
}

GType windinkedit_map_get_type (void)
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditMapClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      (GClassInitFunc) ___class_init,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditMap),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditMapType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/

#define MAX_MAP_X (MAP_COLUMNS * (SCREEN_WIDTH + screen_gap) - screen_gap)
#define MAX_MAP_Y (MAP_ROWS * (SCREEN_HEIGHT + screen_gap) - screen_gap)

// update the coordinates of the view window
void windinkedit_map_updateMapPosition(WindinkeditMap *self, int x, int y)
{
	self->window.left = x;
	self->window.top = y;

	// verify coordinates are within bounds
	if (self->window.left < 0)
	{
		self->window.left = 0;
	}
	if (self->window.top < 0)
	{
		self->window.top = 0;
	}

	self->window.right = self->window.left + self->window_width;
	self->window.bottom = self->window.top + self->window_height;

	if (self->window.right > MAX_MAP_X)
	{
		self->window.left -=  self->window.right - MAX_MAP_X;
		self->window.right = MAX_MAP_X;
	}
	if (self->window.bottom > MAX_MAP_Y)
	{
		self->window.top -=  self->window.bottom - MAX_MAP_Y;
		self->window.bottom = MAX_MAP_Y;
	}

	selfp->screens_displayed.left = self->window.left / (SCREEN_WIDTH + screen_gap);
	selfp->screens_displayed.right = self->window.right / (SCREEN_WIDTH + screen_gap);
	selfp->screens_displayed.top = self->window.top / (SCREEN_HEIGHT + screen_gap);
	selfp->screens_displayed.bottom = self->window.bottom / (SCREEN_HEIGHT + screen_gap);

	selfp->screen_x_offset = -(self->window.left % (SCREEN_WIDTH + screen_gap));
	selfp->screen_y_offset = -(self->window.top % (SCREEN_HEIGHT + screen_gap));
}

void windinkedit_map_updateHoverPosition(WindinkeditMap *self, int x, int y)
{
	selfp->screen_x_hover = (x + self->window.left) / (SCREEN_WIDTH + screen_gap);
	selfp->screen_y_hover = (y + self->window.top) / (SCREEN_HEIGHT + screen_gap);
	selfp->screen_hover = selfp->screen_y_hover * MAP_COLUMNS + selfp->screen_x_hover;

	selfp->x_hover = (x + self->window.left) % (SCREEN_WIDTH + screen_gap);
	selfp->y_hover = (y + self->window.top) % (SCREEN_HEIGHT + screen_gap);

	// Update status bar
	mainWnd->updateMousePos(selfp->screen_hover + 1, selfp->x_hover + SIDEBAR_WIDTH, selfp->y_hover);

	WindinkeditSprite* new_hover_sprite = windinkedit_map_getSpriteClick(self, &selfp->hover_sprite_num);

	if (new_hover_sprite != selfp->hover_sprite)
	{
		selfp->hover_sprite = new_hover_sprite;
		Game_Main();
	}
}

void windinkedit_map_undoAction(WindinkeditMap *self)
{
	undo_buffer->undoAction();
}

void windinkedit_map_redoAction(WindinkeditMap *self)
{
  undo_buffer->redoAction();
}

void windinkedit_map_setMouseSprite(WindinkeditMap *self, WindinkeditSprite* sprite)
{
  // free previous sprite reference if any
  if (self->mouseSprite != NULL)
    g_object_unref(self->mouseSprite);

  if (sprite != NULL)
    {
      self->mouseSprite = WINDINKEDIT_SPRITE(g_object_ref(sprite));
    }
  else
    {
      self->mouseSprite = NULL;
    }
}

void windinkedit_map_setVision(WindinkeditMap *self, int new_vision)
{
  self->cur_vision = new_vision;
  mainWnd->updateVision(new_vision);
}

// triggered by an undo action
void windinkedit_map_placeTiles(WindinkeditMap *self, int first_x_tile, int first_y_tile, int width, int height, 
				TILEDATA tile_set[SCREEN_TILE_WIDTH * 2][SCREEN_TILE_HEIGHT * 2])
{
	int cur_screen, x_tile, y_tile;
	for (int y = first_y_tile, y_temp = 0; y_temp < height; y++, y_temp++)
	{
		for (int x = first_x_tile, x_temp = 0; x_temp < width; x++, x_temp++)
		{
			cur_screen = MAP_COLUMNS * (y / SCREEN_TILE_HEIGHT) + x / SCREEN_TILE_WIDTH;
			if (self->screen[cur_screen] == NULL)
				continue;
			x_tile = x % SCREEN_TILE_WIDTH;
			y_tile = y % SCREEN_TILE_HEIGHT;

			self->miniupdated[cur_screen] = false;

			self->screen[cur_screen]->tiles[y_tile][x_tile] = tile_set[x_temp][y_temp];
		}
	}
}

/**
 * Add a new sprite to the current screen?
 */
int windinkedit_map_placeSprite(WindinkeditMap *self, int dest_screen, int x, int y, int stamp_p)
{
	bool sprite_placed = false;

	vector<int> *screen_num = new vector<int>;
	vector<int> *sprite_num = new vector<int>;
	vector<WindinkeditSprite*> *sprite_list = new vector<WindinkeditSprite*>;

	// use screenmatch when laying down sprites
	if (selfp->screenmatch & (selfp->locked_screen == -1))
	{
	  WindinkeditSequence* cur_seq = windinkedit_dmod_get_sequence(selfp->dmod, self->mouseSprite->sequence);
	  int cur_frame = self->mouseSprite->frame - 1;

		if (cur_seq == NULL || cur_seq->frame_image[cur_frame] == NULL)
		  return 0/*false*/;

		//int center_x, center_y;
		//cur_seq->getCenter(cur_frame, center_x, center_y);

		SDL_Rect r;
		if (windinkedit_sprite_getImageBounds(self->mouseSprite, &r) == 0/*false*/)
		  return 0;
		struct rect sprite_bounds = {r.x, r.y, r.x + r.w, r.y + r.h};

		int first_x_screen = (self->window.left + sprite_bounds.left) / (SCREEN_WIDTH + screen_gap);
		int last_x_screen = (self->window.left + sprite_bounds.right) / (SCREEN_WIDTH + screen_gap);
		int first_y_screen = (self->window.top + sprite_bounds.top) / (SCREEN_HEIGHT + screen_gap);
		int last_y_screen = (self->window.top + sprite_bounds.bottom) / (SCREEN_HEIGHT + screen_gap);

		int cur_screen;
		for (int x_screen = first_x_screen; x_screen <= last_x_screen; x_screen++)
		{
			for (int y_screen = first_y_screen; y_screen <= last_y_screen; y_screen++)
			{
				cur_screen = y_screen * MAP_COLUMNS + x_screen;
				if (self->screen[cur_screen] != NULL)
				{
				  WindinkeditSprite *new_sprite = WINDINKEDIT_SPRITE(g_object_ref(self->mouseSprite));

					// add sprite to screen
					new_sprite->x = x + self->window.left - (x_screen * (SCREEN_WIDTH + screen_gap));
					new_sprite->y = y + self->window.top - (y_screen * (SCREEN_HEIGHT + screen_gap));
					compensateScreenGap(new_sprite->x, new_sprite->y);

					screen_num->push_back(cur_screen);
					sprite_num->push_back(windinkedit_screen_findFirstAvailableSpriteSlot(self->screen[cur_screen]));
					
					sprite_list->push_back(new_sprite);

					sprite_placed = true;
				}
			}
		}
	}
	else 
	{
		int cur_screen;
		// no screenmatch, just place a single sprite
		
		int x_screen, y_screen;

		if (selfp->locked_screen == -1)
		{
			x_screen = (x + self->window.left) / (SCREEN_WIDTH + screen_gap);
			y_screen = (y + self->window.top) / (SCREEN_HEIGHT + screen_gap);
			cur_screen = y_screen * MAP_COLUMNS + x_screen;
		}
		else
		{
			x_screen = selfp->locked_screen % MAP_COLUMNS;
			y_screen = selfp->locked_screen / MAP_COLUMNS;
			cur_screen = selfp->locked_screen;
		}

		if (self->screen[cur_screen] == NULL)
			return false;

		WindinkeditSprite *new_sprite = WINDINKEDIT_SPRITE(g_object_ref(self->mouseSprite));

		new_sprite->x = x + self->window.left - (x_screen * (SCREEN_WIDTH + screen_gap));
		new_sprite->y = y + self->window.top - (y_screen * (SCREEN_HEIGHT + screen_gap));

		// make sure it returns true before deleting mouseSprite
		compensateScreenGap(new_sprite->x, new_sprite->y);
		sprite_placed = true;

		screen_num->push_back(cur_screen);
		sprite_num->push_back(windinkedit_screen_findFirstAvailableSpriteSlot(self->screen[cur_screen]));
		sprite_list->push_back(new_sprite);
	}

	if (!sprite_placed)
	{
		delete screen_num;
		delete sprite_num;
		delete sprite_list;

		return false;
	}

	// create the action and then execute to actually place the sprites
	UNDO_ACTION* action = new UNDO_ACTION;
	action->type = UT_SPRITE_PLACE;
	action->sprite_place.screen_num = screen_num;
	action->sprite_place.sprite_num = sprite_num;
	action->sprite_place.sprite_list = sprite_list;
	action->sprite_place.stamp_p = stamp_p;

	undo_buffer->addUndo(action);

	return true;
}

// centers the mouse on the sprites position
// (used by Undo)
void windinkedit_map_loadMouseSprite(WindinkeditMap *self, WindinkeditSprite* sprite, int screen_num)
{
	// create a new mouse sprite if none exists already
	if (self->mouseSprite != NULL)
	{
	  g_object_unref(self->mouseSprite);
	}
	
	self->mouseSprite = WINDINKEDIT_SPRITE(g_object_ref(sprite));

// 	sprite_selector->currentSequence = self->mouseSprite->sequence;
// 	sprite_selector->currentFrame = self->mouseSprite->frame;
// 	sprite_selector->showFrames = true;

	int screen_x = screen_num % MAP_COLUMNS;
	int screen_y = screen_num / MAP_COLUMNS;
	
	int x_offset = (screen_x * (SCREEN_WIDTH + screen_gap)) - self->window.left;
	int y_offset = (screen_y * (SCREEN_HEIGHT + screen_gap)) - self->window.top;

 	self->mouseSprite->x += x_offset;
 	self->mouseSprite->y += y_offset;
// 	mouseSprite->x = x_hover;
// 	mouseSprite->y = y_hover;

	// center the mouse on the sprite
	//SetCursorPos(mouseSprite->x + mapRect.left, mouseSprite->y + mapRect.top);
}

// stores sprite in mousesprite
int windinkedit_map_pickupSprite(WindinkeditMap *self)
{
	// make sure the screen is valid
	if (self->screen[selfp->screen_hover] == NULL)
		return NULL;
	
	int sprite_num;
	WindinkeditSprite* temp_sprite;
	if ((temp_sprite = windinkedit_screen_getSprite(self->screen[selfp->screen_hover], selfp->x_hover, selfp->y_hover, self->cur_vision, &sprite_num)) == NULL)
	  return false;
	
	UNDO_ACTION* action = new UNDO_ACTION;
	action->type = UT_SPRITE_PICKUP;
	action->sprite_pickup.screen_num = selfp->screen_hover;
	action->sprite_pickup.sprite_num = sprite_num;
	action->sprite_pickup.new_mouse_sprite = temp_sprite;

	selfp->mouseSprite_dx = action->sprite_pickup.new_mouse_sprite->x - selfp->x_hover;
	selfp->mouseSprite_dy = action->sprite_pickup.new_mouse_sprite->y - selfp->y_hover;

	{
	  int seq = self->screen[selfp->screen_hover]->sprite[sprite_num]->sequence;
	  int frame = self->screen[selfp->screen_hover]->sprite[sprite_num]->frame;
	  WindinkeditSequence* seqobj = windinkedit_dmod_get_sequence(selfp->dmod, seq);
	  printf("picked up (s%d,f%d) - (%d,%d)x%dx%d - m(%d,%d) - d(%d,%d)\n",
		 seq,
		 frame,
		 self->screen[selfp->screen_hover]->sprite[sprite_num]->x,
		 self->screen[selfp->screen_hover]->sprite[sprite_num]->y,
		 seqobj->frame_width[frame-1],
		 seqobj->frame_height[frame-1],
		 selfp->x_hover, selfp->y_hover,
		 selfp->mouseSprite_dx, selfp->mouseSprite_dy);
	}

	// Don't show bounding box anymore
	selfp->hover_sprite = NULL;
	selfp->hover_sprite_num = -1;

	undo_buffer->addUndo(action);

	return true;
}

// returns pointer to sprite that was clicked on
WindinkeditSprite* windinkedit_map_getSpriteClick(WindinkeditMap* self, int* psprite_num)
{
  // make sure the screen is valid
  if (self->screen[selfp->screen_hover] == NULL)
    return NULL;
  
  return windinkedit_screen_getSprite(self->screen[selfp->screen_hover],
				      selfp->x_hover, selfp->y_hover, self->cur_vision,
				      psprite_num);
}

int windinkedit_map_drawScreens(WindinkeditMap *self, SDL_Surface* surface)
{
  // fills the back buffer with black
  SDL_FillRect(lpddsback, NULL, color_map_screen_grid);

	memset(new_vision_used, 0, VISIONS_TRACKED);

	int cur_screen;
	struct rect minimap_dest_rect, minimap_src_rect;

	double width_ratio = double(SQUARE_WIDTH) / double(SCREEN_WIDTH);
	double height_ratio = double(SQUARE_HEIGHT) / double(SCREEN_HEIGHT);

	int x_total_offset;
	int y_total_offset = selfp->screen_y_offset;

	for (int y_screen = selfp->screens_displayed.top; y_screen <= selfp->screens_displayed.bottom; y_screen++)
	{
		cur_screen = y_screen * MAP_COLUMNS + selfp->screens_displayed.left;

		minimap_dest_rect.top = y_screen * SQUARE_HEIGHT;
		minimap_dest_rect.bottom = minimap_dest_rect.top + SQUARE_HEIGHT;

		minimap_src_rect.top = y_total_offset;
		minimap_src_rect.bottom = y_total_offset + SCREEN_HEIGHT;

		if (minimap_src_rect.top < 0)
		{
			minimap_dest_rect.top -= int(double(minimap_src_rect.top) * height_ratio);
			minimap_src_rect.top = 0;
		}

		if (minimap_src_rect.bottom > self->window_height)
		{
			minimap_dest_rect.bottom -= int(double(minimap_src_rect.bottom - self->window_height) * height_ratio);
			minimap_src_rect.bottom = self->window_height;
		}

		if (minimap_src_rect.top >= minimap_src_rect.bottom)
		{
			y_total_offset += (SCREEN_HEIGHT + screen_gap);
			continue;
		}

		x_total_offset = selfp->screen_x_offset;
		for (int x_screen = selfp->screens_displayed.left; x_screen <= selfp->screens_displayed.right; x_screen++)
		{
			minimap_dest_rect.left = x_screen * SQUARE_WIDTH;
			minimap_dest_rect.right = minimap_dest_rect.left + SQUARE_WIDTH;
			
			minimap_src_rect.left = x_total_offset;
			minimap_src_rect.right = x_total_offset + SCREEN_WIDTH;

			if (minimap_src_rect.left < 0)
			{
				minimap_dest_rect.left -= int(double(minimap_src_rect.left) * width_ratio);
				minimap_src_rect.left = 0;
			}

			if (minimap_src_rect.right > self->window_width)
			{
				minimap_dest_rect.right -= int(double(minimap_src_rect.right - self->window_width) * width_ratio);
				minimap_src_rect.right = self->window_width;
			}

			if (minimap_src_rect.left >= minimap_src_rect.right)
			{
				cur_screen++;
				x_total_offset += (SCREEN_WIDTH + screen_gap);
				continue;
			}

			if (self->screen[cur_screen] != NULL)
			{
				// render tiles
//WGC			  windinkedit_screen_drawTiles(self->screen[cur_screen], x_total_offset, y_total_offset, wgc);

				// render sprites
//WGC			  windinkedit_screen_drawSprites(self->screen[cur_screen], x_total_offset, y_total_offset, &minimap_src_rect, self->cur_vision, wgc);

				// draw hardness
				if (displayhardness)
				{
				  windinkedit_screen_drawHardTiles(self->screen[cur_screen], x_total_offset, y_total_offset);
				  windinkedit_screen_drawHardSprites(self->screen[cur_screen], x_total_offset, y_total_offset, &minimap_src_rect, self->cur_vision);
				}

				// add screen to minimap
				if (update_minimap)
				{   
					windinkedit_map_drawScreenOnMinimap(self, cur_screen);
					//minimap->image->Blt(&minimap_dest_rect, lpddsback, &minimap_src_rect, DDBLT_WAIT, NULL);
				}

				// draw a box around the screen if necessary
				if (self->screenMode == SPRITE_MODE)
				{
					// "lock" the screen
					if (selfp->locked_screen == cur_screen)
					{
						struct rect dest;
						dest.left = x_total_offset;
						dest.top = y_total_offset;
						dest.right = x_total_offset + SCREEN_WIDTH;
						dest.bottom = y_total_offset + SCREEN_HEIGHT;
						
						if (selfp->locked_screen % MAP_COLUMNS != 0)
							dest.left -= screen_gap;
						if (selfp->locked_screen >= MAP_COLUMNS)
							dest.top -= screen_gap;
						if (selfp->locked_screen % MAP_COLUMNS != MAP_COLUMNS - 1)
							dest.right += screen_gap;
						if (selfp->locked_screen < NUM_SCREENS - MAP_COLUMNS)
							dest.bottom += screen_gap;

						drawBox(&dest, color_screen_selected_box, 1);
					}
				}
			}
			else
			{
				// draw a blank screen on the main screen
			  fill_rect(lpddsback, &minimap_src_rect, color_blank_screen_fill);

				// draw a blan screen on the minimap
		//		minimap->image->Blt(&minimap_dest_rect, lpddsback, NULL, DDBLT_COLORFILL | DDBLT_WAIT, &blankscreen);
			}

			cur_screen++;
			x_total_offset += (SCREEN_WIDTH + screen_gap);
		}
		y_total_offset += (SCREEN_HEIGHT + screen_gap);
	}

	selfp->hover_sprite = windinkedit_map_getSpriteClick(self, &selfp->hover_sprite_num);

	
	struct rect bounds;
	int x_offset, y_offset;

	// draw a box around the sprite the mouse is on top of
	if (self->screenMode == SPRITE_MODE && selfp->hover_sprite != NULL && self->mouseSprite == NULL)
	{
	    SDL_Rect r;
	    windinkedit_sprite_getImageBounds(selfp->hover_sprite, &r);
	    rect_set(&bounds, r.x, r.y, r.x + r.w, r.y + r.h);

		struct rect clip_box = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

		if (fixBounds(bounds, &clip_box))
		{
			x_offset = (selfp->screen_x_hover - selfp->screens_displayed.left) * 
				(SCREEN_WIDTH + screen_gap) + selfp->screen_x_offset;
			y_offset = (selfp->screen_y_hover - selfp->screens_displayed.top) * 
				(SCREEN_HEIGHT + screen_gap) + selfp->screen_y_offset;

			bounds.left += x_offset;
			bounds.right += x_offset;
			bounds.top += y_offset;
			bounds.bottom += y_offset;

			drawBox(&bounds, color_sprite_hover_box, 1);
		}	

		if (hover_sprite_info)
		{
			if (selfp->hover_sprite_num >= 0 && selfp->hover_sprite_num < 100)
			{
			  WindinkeditSequence* seq = windinkedit_dmod_get_sequence(selfp->dmod,
										   selfp->hover_sprite->sequence);
			  windinkedit_sequence_drawSpriteInfo(seq, x_offset, y_offset-20, selfp->hover_sprite, selfp->hover_sprite_num+1);
			}
		}

		if (hover_sprite_hardness)
		{
			if (selfp->hover_sprite_num >= 0 && selfp->hover_sprite_num < 100)
			{
			  //struct rect clip_box = {0, 0, current_map->window_width, current_map->window_height};
			  WindinkeditSequence* seq = windinkedit_dmod_get_sequence(selfp->dmod,
										   selfp->hover_sprite->sequence);
			  windinkedit_sequence_clipRenderHardness(seq, x_offset, y_offset, selfp->hover_sprite, &bounds);
			}
		}
	}

	// track the first 256 visions used on screen
	if (memcmp(new_vision_used, vision_used, VISIONS_TRACKED) != 0)
	{
		// update the left panel
		memcpy(vision_used, new_vision_used, VISIONS_TRACKED);
	
//		mainWnd->GetLeftPane()->UpdateVisionTree();
	}
	
	return true;
}

/**
 * Trim the mouse sprite
 */
void windinkedit_map_trimSprite(WindinkeditMap *self, struct rect trim)
{
  // Make a copy for Undo
  WindinkeditSprite *new_sprite = windinkedit_sprite_copy(self->mouseSprite);
  WindinkeditSequence *seq = windinkedit_dmod_get_sequence(selfp->dmod, new_sprite->sequence);

  if (seq == NULL)
    return;

  SDL_Rect r;
  if (windinkedit_sprite_getImageBounds(self->mouseSprite, &r) == 0/*false*/)
    return;
  struct rect image_bounds = {r.x, r.y, r.x + r.w, r.y + r.h};

  int max_x = image_bounds.right - image_bounds.left;
  int max_y = image_bounds.bottom - image_bounds.top;

  if (new_sprite->trim_right == 0)
    {
      new_sprite->trim_left = 0;
      new_sprite->trim_top = 0;
      new_sprite->trim_right = max_x;
      new_sprite->trim_bottom = max_y;
    }
  
  new_sprite->trim_left += (short)trim.left;
  new_sprite->trim_top += (short)trim.top;
  new_sprite->trim_right += (short)trim.right;
  new_sprite->trim_bottom += (short)trim.bottom;
  
  if (new_sprite->trim_left < 0)
    {
      new_sprite->trim_left = 0;
    }
  
  if (new_sprite->trim_top < 0)
    {
      new_sprite->trim_top = 0;
    }
  
  if (new_sprite->trim_right > max_x)
    {
      new_sprite->trim_right = max_x;
    }
  
  if (new_sprite->trim_bottom > max_y)
    {
		new_sprite->trim_bottom = max_y;
    }
  
  if (new_sprite->trim_left == 0 && new_sprite->trim_top == 0 
      && new_sprite->trim_right == max_x && new_sprite->trim_bottom == max_y)
    {
      // remove all trimming attributes from the sprite.
      new_sprite->trim_left = 0;
      new_sprite->trim_top = 0;
      new_sprite->trim_right = 0;
      new_sprite->trim_bottom = 0;
    }
  
  windinkedit_map_changeSpriteProperties(self, -1, -1, new_sprite, self->mouseSprite);
}

// creates a new blank screen on the map
void windinkedit_map_newScreen(WindinkeditMap *self, int screen_num)
{
	if (((screen_num >= 0) & (screen_num < NUM_SCREENS)) && (self->screen[screen_num] == NULL))
	{
		UNDO_ACTION* action = new UNDO_ACTION;
		action->type = UT_SCREEN_CREATE;
		action->screen_delete.screen_num = screen_num;

		undo_buffer->addUndo(action);
	}
}

// deletes the screen from the map
void windinkedit_map_deleteScreen(WindinkeditMap *self, int screen_num)
{
	if ((screen_num >= 0) & (screen_num < NUM_SCREENS) && (self->screen[screen_num] != NULL))
	{
		UNDO_ACTION* action = new UNDO_ACTION;
		action->type = UT_SCREEN_DELETE;
		action->screen_delete.screen_num = screen_num;
		action->screen_delete.midi_num = self->midi_num[screen_num];
		action->screen_delete.indoor = self->indoor[screen_num];
		action->screen_delete.old_screen = windinkedit_screen_copy(self->screen[screen_num]);

		undo_buffer->addUndo(action);
	}	
}

// places a copied screen onto the map
void windinkedit_map_pasteScreen(WindinkeditMap *self, int screen_num, WindinkeditScreen* source_screen, int midi_num, int indoor)
{
	if (((screen_num >= 0) & (screen_num < NUM_SCREENS)) && (self->screen[screen_num] == NULL))
	{
		UNDO_ACTION* action = new UNDO_ACTION;
		action->type = UT_SCREEN_PASTE;
		action->screen_paste.screen_num = screen_num;
		action->screen_paste.midi_num = midi_num;
		action->screen_paste.indoor = indoor;
		action->screen_paste.new_screen = windinkedit_screen_copy(source_screen);

		undo_buffer->addUndo(action);
	}
}

void windinkedit_map_changeScreenProperties(WindinkeditMap *self, int screen_num, const char* script, int midi_num, int indoor)
{
	if (((screen_num >= 0) & (screen_num < NUM_SCREENS)) && (self->screen[screen_num] != NULL))
	{
		UNDO_ACTION* action = new UNDO_ACTION;
		action->type = UT_SCREEN_PROPERTIES_CHANGE;
		action->screen_properties_change.screen_num = screen_num;

		// save new information
		strcpy(action->screen_properties_change.new_script, script);
		action->screen_properties_change.new_midi = midi_num;
		action->screen_properties_change.new_indoor = indoor;

		// save old information
		strcpy(action->screen_properties_change.old_script, self->screen[screen_num]->script);
		action->screen_properties_change.old_midi = self->midi_num[screen_num];
		action->screen_properties_change.old_indoor = self->indoor[screen_num];		

		undo_buffer->addUndo(action);
	}
}

void windinkedit_map_changeSpriteProperties(WindinkeditMap *self, int screen_num, int sprite_num, WindinkeditSprite* new_sprite, WindinkeditSprite* old_sprite)
{
	UNDO_ACTION* action = new UNDO_ACTION;
	action->type = UT_SPRITE_PROPERTIES_CHANGE;
	action->sprite_property_change.screen_num = screen_num;
	action->sprite_property_change.sprite_num = sprite_num;
	action->sprite_property_change.new_sprite = new_sprite;	// pointer was passed in
	action->sprite_property_change.old_sprite = WINDINKEDIT_SPRITE(g_object_ref(old_sprite));

	undo_buffer->addUndo(action);
}

void windinkedit_map_screenshot(WindinkeditMap *self, char* filename)
{
	FILE *output;

	// make sure we can open the file first
	if ((output = fopen(filename, "rb")) == NULL)
	{
		return;
	}
	fclose(output);

	if (self->render_state == SHOW_MINIMAP)
	  printScreen(filename, self->minimap->image);
	else
	  printScreen(filename, lpddsback);

}

void windinkedit_map_toggleAutoUpdateMinimap(WindinkeditMap *self)
{
	if (update_minimap)
	{
		update_minimap = false;
		//minimap->drawSquares();
	}
	else
	{
		update_minimap = true;
		fast_minimap_update = true;
	}
}

//function used to draw one screen on the minimap
static void windinkedit_map_drawScreenOnMinimap(WindinkeditMap *self, int cur_screen)
{
	if (self->miniupdated[cur_screen] && fast_minimap_update)
		return;

	//rects for where to draw and from where
	struct rect src_rect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT}; //set for whole buffer
	struct rect dst_rect;

	//calculate where the screen is on the minimap row and col
	int row_num = (cur_screen / MAP_COLUMNS) + 1;
	int col_num = (cur_screen % MAP_COLUMNS) + 1;

	//set the rect to the screen on the minimap
	dst_rect.left = (col_num - 1) * SQUARE_WIDTH;
	dst_rect.right = col_num * SQUARE_WIDTH;
	dst_rect.top = (row_num - 1) * SQUARE_HEIGHT;
	dst_rect.bottom = row_num * SQUARE_HEIGHT;

	//set to fill color of black
	fill_rect(lpddshidden, &src_rect, color_blank_screen_fill);

	//if the screen is acctually there, draw tiles and sprite
	if (self->screen[cur_screen] != NULL)
	{
		
//WGC		windinkedit_screen_drawTiles(self->screen[cur_screen], 0, 0, lpddshidden);
//WGC		windinkedit_screen_drawSprites(self->screen[cur_screen], 0, 0, &src_rect, self->cur_vision, lpddshidden);
	}
	self->miniupdated[cur_screen] = true;

	//blit the buffer to the minimap
	Blt(self->minimap->image, &dst_rect, lpddshidden, &src_rect);
}

WindinkeditSprite* windinkedit_map_getMouseSprite(WindinkeditMap *self)
{
  if (self->mouseSprite != NULL)
    return WINDINKEDIT_SPRITE(g_object_ref(self->mouseSprite));
  else
    return NULL;
}
  
void windinkedit_map_lock_screen(WindinkeditMap *self)
{
  if (self->screen[selfp->screen_hover] != NULL)
    selfp->locked_screen = selfp->screen_hover;
}
void windinkedit_map_unlock_screen(WindinkeditMap *self)
{
  selfp->locked_screen = -1;
}


/***********/
/**  I/O  **/
/**       **/
/***********/

// this is part of the Map class and includes the I/O functionality
#define DINK_HEADER_NAME		"Smallwood"

struct DUMMY_HARD_TILE
{
	unsigned char data[TILEWIDTH][TILEHEIGHT + 1];	// 1 extra garbage bit
};

struct DUMMY_TILE_HARDNESS
{
	int data[SCREEN_TILE_HEIGHT][SCREEN_TILE_WIDTH];
	int garbage[32];
};


int windinkedit_map_deallocate_map(WindinkeditMap *self)
{
  for (int i = 0; i < NUM_SCREENS; i++)
    {
      if (self->screen[i])
	{
	  g_object_unref(self->screen[i]);
	  self->screen[i] = NULL;
	}
    }
  return true;
}

int windinkedit_map_save_hard_dat(WindinkeditMap *self)
{
  
  gchar *filename = g_strconcat(windinkedit_dmod_get_dmoddir(selfp->dmod), "hard.dat", NULL);
	FILE *stream = stream = fopen(filename, "wb");
	g_free(filename);

	if (stream == NULL)
	{ 
	  ::wxMessageBox(wxT("Could not open hard.dat"), wxT("Error"));
		return false; 
	} 

	unsigned char garbage[58]; 
	memset(garbage, 0, 58); 
 
	DUMMY_HARD_TILE hard_tile; 
 
	for (int i = 0; i < NUM_HARD_TILES; i++) 
	{ 
		// revert to the messed up file format 
		for (int y = 0; y < TILEHEIGHT; y++)  
		{ 
			for (int x = 0; x < TILEWIDTH; x++) 
			{ 
// 				hard_tile.data[x][y] = hard_tile_selector.hardTile[i][y][x]; 
			} 
		} 
 
		fwrite(&hard_tile, sizeof(DUMMY_HARD_TILE), 1, stream); 
		
		// write out useless data 
		fwrite(garbage, 58, 1, stream); 
	} 
 
	// each screen 96 blocks (12 x 8)  
	// 128 blocks 

	DUMMY_TILE_HARDNESS hard_tile_table[MAX_TILE_BMPS]; 

	for (int i = 0; i < MAX_TILE_BMPS; i++) 
	{
	  WindinkeditTile* cur_tile = windinkedit_dmod_get_tile(selfp->dmod, i);
	  memcpy(&hard_tile_table[i], cur_tile->tile_hardness,
		 sizeof(int) * SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH); 
	} 
 
	fwrite(&hard_tile_table, sizeof(DUMMY_TILE_HARDNESS) * MAX_TILE_BMPS, 1, stream); 

	fclose(stream); 
 
	return true; 
}

int windinkedit_map_parse_hard_dat(WindinkeditMap *self)
{
	/*
	hard.dat
	file size: 2,118,400
	800 entries
	block size of each tile: 2550
	size between blocks: 58
	first block begins at beginning of file
	column major order
	each pixel separated by 50 bytes of data followed by 1 byte = 0
	z is 1, normal hardness
	a is 2, water hardness
	s is 3, ??? hardness
	*/

	//unsigned char data[TILEWIDTH][TILEHEIGHT + 1];	// 1 extra garbage bit

  gchar *filename = g_strconcat(windinkedit_dmod_get_dmoddir(selfp->dmod), "hard.dat", NULL);
	FILE *stream = stream = fopen(filename, "rb");
	g_free(filename);
	if (stream == NULL)
	{
		wxMessageBox(wxT("Could not open hard.dat"), wxT("Error"));
		return false;
	}

	DUMMY_HARD_TILE hard_tile;

	unsigned char *dest_ptr;
	
	for (int i = 0; i < NUM_HARD_TILES; i++)
	{
		if(!fread(&hard_tile, sizeof(DUMMY_HARD_TILE), 1, stream))
			return false;

// 		dest_ptr = hard_tile_selector.hardTile[i][0];

// 		// fix the messed up file format
// 		for (int y = 0; y < TILEHEIGHT; y++)
// 		{
// 			for (int x = 0; x < TILEWIDTH; x++)
// 			{
// 				*dest_ptr = hard_tile.data[x][y];
// 				dest_ptr++;
// 			}
// 		}
		fseek(stream, 58, SEEK_CUR);	// skip useless data
	}

	// each screen 96 blocks (12 x 8)
	// 128 blocks
	DUMMY_TILE_HARDNESS hard_tile_table[MAX_TILE_BMPS];

	if(!fread(&hard_tile_table, sizeof(DUMMY_TILE_HARDNESS) * MAX_TILE_BMPS, 1, stream))
		return false;

	for (int i = 0; i < MAX_TILE_BMPS; i++)
	{
	  WindinkeditTile* cur_tile = windinkedit_dmod_get_tile(selfp->dmod, i);
	  memcpy(cur_tile->tile_hardness, &hard_tile_table[i],
		 sizeof(int) * SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH);
	}

	fclose(stream);

	return true;
}

int windinkedit_map_parse_dink_dat(WindinkeditMap *self)
{
	int screen_order[NUM_SCREENS];

	char *filename = g_strconcat(windinkedit_dmod_get_dmoddir(selfp->dmod), "dink.dat", NULL);
	FILE *stream = fopen(filename, "rb");
	g_free(filename);

	if (stream == NULL)
	{
		// dink.dat not found, so clear the memory used by it
		memset(screen_order, 0, sizeof(int) * NUM_SCREENS);
		memset(self->midi_num, 0, sizeof(int) * NUM_SCREENS);
		memset(self->indoor, 0, sizeof(int) * NUM_SCREENS);

		// clear map.dat
		memset(self->screen, 0, sizeof(int) * NUM_SCREENS);

		return false;
	}

	fseek(stream, 24, SEEK_SET);

	// read in the order the screens were created
	// this lets us know where in the map.dat each screen is located
	if(!fread(screen_order, sizeof(int) * NUM_SCREENS, 1, stream))
	  {
	    perror("parse_dink_dat");
	    return false;
	  }

	fseek(stream, 4, SEEK_CUR);

	// read in midi numbers for each screen
	if (!fread(self->midi_num, sizeof(int) * NUM_SCREENS, 1, stream))
		return false;

	fseek(stream, 4, SEEK_CUR);

	// read in whether the screen is indoor or outdoor
	if(!fread(self->indoor, sizeof(int) * NUM_SCREENS, 1, stream))
		return false;

	fclose(stream);

	if (!windinkedit_map_parse_map_dat(self, screen_order))
	{
		return false;
	}

	return true;
}

int windinkedit_map_parse_map_dat(WindinkeditMap *self, int screen_order[NUM_SCREENS])
{
	FILE *stream;
	int i;

	gchar *filename;
	filename = g_strconcat(windinkedit_dmod_get_dmoddir(selfp->dmod), "map.dat", NULL);

	if ((stream = fopen(filename, "rb")) == NULL)
	{
		wxMessageBox(wxT("Could not open map.dat"), wxT("Error"));
		return false;
	}
	g_free(filename);

	int map_location;
	WindinkeditSprite* new_sprite;
	int temp;

	DUMMY_SPRITE sprite_data[MAX_SPRITES];
	DUMMY_TILE tile_data[SCREEN_TILE_HEIGHT][SCREEN_TILE_WIDTH];

	for (i = 0; i < NUM_SCREENS; i++)
	{
		// find the next screen to load
		map_location = screen_order[i] - 1;
		if (map_location == -1)
		{
			self->screen[i] = NULL;
			continue;
		}

		// go to that spot in the map file to load
		fseek(stream, map_location * SCREEN_DATA_SIZE, SEEK_SET);

		// advance to first tile
		fseek(stream, 20, SEEK_CUR);

		// read in the tiles
		self->screen[i] = windinkedit_screen_new(selfp->dmod);

		// read in the tile data
		if (!fread(tile_data, sizeof(DUMMY_TILE) * SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH, 1, stream))
			return false;
		
		for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)
		{
			for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
			{
				temp = tile_data[y][x].tile;
				// make the map format easier to read
				self->screen[i]->tiles[y][x].bmp = temp / (1 << 7);
				self->screen[i]->tiles[y][x].x = ((temp % (1 << 7)) % 12);
				self->screen[i]->tiles[y][x].y = (temp % (1 << 7)) / 12;

				self->screen[i]->tiles[y][x].alt_hardness = tile_data[y][x].alt_hardness;
			}
		}

		// advance to sprite data
		fseek(stream, 540, SEEK_CUR);

		// read in a sprite block
		if (fread(sprite_data, sizeof(DUMMY_SPRITE) * MAX_SPRITES, 1, stream) == 0)
			return false;

		//int next_sprite = 0; //cause problem with changing sprite numbers

		// read in the sprite data
		for (int cur_sprite = 0; cur_sprite < MAX_SPRITES; cur_sprite++)
		{
			// check if there's anything in the sprite block
			if (sprite_data[cur_sprite].sprite_exist == 1)
			{
			  // found a sprite, now allocate the structure for it
			  new_sprite = windinkedit_sprite_new_from(&sprite_data[cur_sprite]);

			  self->screen[i]->sprite[cur_sprite] = new_sprite;
			  //next_sprite++;
			}
		}
		if (!fread(self->screen[i]->script, BASE_SCRIPT_MAX_LENGTH, 1, stream))
		  return false;
	}

	fclose(stream);

	return true;
}

int windinkedit_map_save_dink_dat(WindinkeditMap *self)
{
	unsigned char dummy_array[2240];
	memset(dummy_array, 0, 2240);

	gchar *filename = g_strconcat(windinkedit_dmod_get_dmoddir(selfp->dmod), "dink.dat", NULL);
	FILE* stream = fopen(filename, "wb");
	g_free(filename);
	if (stream == NULL)
	  return false;

	if(!fwrite(DINK_HEADER_NAME, sizeof(DINK_HEADER_NAME), 1, stream))
		return false;

	// write out garbage
	if(!fwrite(&dummy_array, 24 - sizeof(DINK_HEADER_NAME), 1, stream))
		return false;
	
	// get the order the screens were created
	int screen_order[NUM_SCREENS];
	
	// first fix the numbers so they are ordered
	int screen_count = 1;
	for (int i = 0; i < NUM_SCREENS; i++)
	{
		if (self->screen[i] != NULL)
		{
			screen_order[i] = screen_count;
			screen_count++;
		}
		else
		{
			screen_order[i] = 0;
		}	
	}

	// write out the order the screens were created
	if(!fwrite(&screen_order, sizeof(int) * NUM_SCREENS, 1, stream))
		return false;

	// write out garbage
	if(!fwrite(&dummy_array, 4, 1, stream))
		return false;

	// write out midi numbers for each screen
	if(!fwrite(self->midi_num, sizeof(int) * NUM_SCREENS, 1, stream))
		return false;

	// write out garbage
	if(!fwrite(&dummy_array, 4, 1, stream))
		return false;

	// write out whether the screen is indoor or outdoor
	if(!fwrite(self->indoor, sizeof(int) * NUM_SCREENS, 1, stream))
		return false;

	// write out extra garbage
	if(!fwrite(&dummy_array, 2240, 1, stream))
		return false;

	// close the file
	fclose(stream);

	return true;
}

int windinkedit_map_save_map_dat(WindinkeditMap *self)
{
	DUMMY_SPRITE sprite_data;

	unsigned char dummy_array[1024];
	memset(dummy_array, 0, 1024);

	//DUMMY_SPRITE sprite_data[MAX_SPRITES];
	DUMMY_TILE tile_data[SCREEN_TILE_HEIGHT][SCREEN_TILE_WIDTH];
	memset(tile_data, 0, SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH * sizeof(DUMMY_TILE));

	// open the map file
	gchar *filename = g_strconcat(windinkedit_dmod_get_dmoddir(selfp->dmod), "map.dat", NULL);
	FILE *stream = fopen(filename, "wb");
	g_free(filename);
	if (stream == NULL)
		return false;

	for (int i = 0; i < NUM_SCREENS; i++)
	{
		if (self->screen[i] == NULL)
			continue;

		// write out garbage
		if(!fwrite(&dummy_array, 20, 1, stream))
			return false;
	
		// fill in tile output matrix
		for (int y = 0; y < SCREEN_TILE_HEIGHT; y++)
		{
			for (int x = 0; x < SCREEN_TILE_WIDTH; x++)
			{
				// tile number
			  tile_data[y][x].tile = (self->screen[i]->tiles[y][x].bmp << 7) +
			    self->screen[i]->tiles[y][x].x +
			    self->screen[i]->tiles[y][x].y * 12;

				// tile hardness
				tile_data[y][x].alt_hardness = self->screen[i]->tiles[y][x].alt_hardness;
			}
		}

		// write out the tile information for the screen
		if(!fwrite(tile_data, SCREEN_TILE_HEIGHT * SCREEN_TILE_WIDTH * sizeof(DUMMY_TILE), 1, stream))
			return false;

		// write out more garbage
		if(!fwrite(&dummy_array, 540, 1, stream))
			return false;

		// write out the sprite data
		for (int cur_sprite = 0; cur_sprite < MAX_SPRITES; cur_sprite++)
		{
			// check if there's anything in the sprite block
			if (self->screen[i]->sprite[cur_sprite] != NULL)
			{
			  windinkedit_sprite_formatOutput(self->screen[i]->sprite[cur_sprite], &sprite_data);

			  // write out a sprite block
			  if (fwrite(&sprite_data, sizeof(sprite_data), 1, stream) == 0)
			    return false;
			}
			else
			{
				// write out a sprite block
				if (fwrite(&dummy_array, sizeof(DUMMY_SPRITE), 1, stream) == 0)
					return false;
			}
		}

		if(!fwrite(self->screen[i]->script, BASE_SCRIPT_MAX_LENGTH, 1, stream))
			return false;
		
		// write out garbage
		if(!fwrite(&dummy_array, 1019, 1, stream))
			return false;
	}

	// close the map.dat file
	fclose(stream);

	return true;
}


WindinkeditScreen* windinkedit_map_get_screen(WindinkeditMap* self, int screen_maths)
{
  g_return_val_if_fail(screen_maths >= 0 && screen_maths < NUM_SCREENS, NULL);

  return self->screen[screen_maths];
}

void windinkedit_map_set_screen(WindinkeditMap* self, int screen_maths, WindinkeditScreen* new_screen)
{
  g_return_if_fail(screen_maths >= 0 && screen_maths < NUM_SCREENS);

  if (self->screen[screen_maths] != NULL)
    g_object_unref(self->screen);
  self->screen[screen_maths] = new_screen;
}

void windinkedit_map_set_graphics_size(WindinkeditMap* self, int new_width, int new_height)
{
  self->window_width = new_width;
  self->window_height = new_height;
  windinkedit_map_updateMapPosition(self, self->window.left, self->window.top);
}
