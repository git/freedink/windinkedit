#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif



#include <stdio.h>

#include "Globals.h"
#include "windinkedit-map.h"
#include "Tools.h"
/* #include <io.h> */
#include <sys/stat.h>

// prints the destination screen to the specified .bmp file in 24 bit color
int printScreen(char *filename, SDL_Surface* surface)
{
	return (SDL_SaveBMP(surface, filename) == 0);
}

struct FILENAME
{
	int file_location;
	char filename[13];
};

// compare function used for quicksort
int compare_filenames( const void *arg1, const void *arg2 )
{
   /* Compare all of both strings: */
   return strcasecmp( (*(FILENAME*)arg1).filename, (*(FILENAME*)arg2).filename );
}


void ffcreate(char* path, bool delete_file)
{
// TODO:
// 	HANDLE hFind;
// 	WIN32_FIND_DATA dataFind;
// 	/*BOOL*/int bMoreFiles = /*TRUE*/1;
// 	int i;

// 	int num_files = 0;

// 	char buffer[250];
// 	strcpy(buffer, path);
// 	strcat(buffer, "*.bmp");

// 	hFind = FindFirstFile(buffer, &dataFind);

// 	FILENAME* file_list = NULL;

// 	// get all the *.bmp files in the directory
// 	while(hFind != INVALID_HANDLE_VALUE && bMoreFiles == /*TRUE*/1)
// 	{
// 		if (strlen(dataFind.cFileName) < MAX_BMP_LENGTH)
// 		{
// 			file_list = (FILENAME*)realloc(file_list, sizeof(FILENAME) * (num_files + 1));
// 			memset(&file_list[num_files], 0 , sizeof(FILENAME));

// 			strcpy(file_list[num_files].filename, dataFind.cFileName);

// 			num_files++;
// 		}

// 		bMoreFiles = FindNextFile(hFind, &dataFind);
// 	}

// 	FindClose(hFind);

// 	strcpy(buffer, path);
// 	strcat(buffer, "*.*");

// 	hFind = FindFirstFile(buffer, &dataFind);
// 	bMoreFiles = /*TRUE*/1;

// 	// get all the directories
// 	while (hFind != INVALID_HANDLE_VALUE && bMoreFiles == /*TRUE*/1)
// 	{
// 		if ((dataFind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0 && dataFind.cFileName[0] != '.')
// 		{
// 			strcpy(buffer, path);
// 			strcat(buffer, dataFind.cFileName);
// 			strcat(buffer, "\\");

// 			ffcreate(buffer, delete_file);
// 		}

// 		bMoreFiles = FindNextFile(hFind, &dataFind);
// 	}

// 	FindClose(hFind);

// 	if (num_files == 0)
// 		return;


// 	////////////// Sort the filenames alphabetically
// 	qsort(file_list, num_files, sizeof(FILENAME), compare_filenames);


// 	FILE *output, *input;

// 	strcpy(buffer, path);
// 	strcat(buffer, "dir.ff");

// 	if ((output = fopen(buffer, "wb")) == NULL)
// 	{
// 		return;
// 	}

// 	int temp = num_files + 1;

// 	fwrite(&temp, sizeof(int), 1, output);

// 	FILENAME dummy;
// 	memset(&dummy, 0, sizeof(FILENAME));
// 	for (i = 0; i < num_files + 1; i++)
// 		fwrite(&dummy, 17, 1, output);

// 	unsigned char data_buffer[DATA_BUFFER_SIZE];

// 	int file_offset_location = 4 + (num_files + 1) * 17;

// 	for (i = 0; i < num_files; i++)
// 	{
// 		strcpy(buffer, path);
// 		strcat(buffer, file_list[i].filename);

// 		file_list[i].file_location = file_offset_location;

// 		if ((input = fopen(buffer, "rb")) == NULL)
// 		{
// 			fclose(output);
// 			return;
// 		}

// 		int amount_read;

// 		while ((amount_read = fread(data_buffer, 1, DATA_BUFFER_SIZE, input)) != 0)
// 		{
// 			file_offset_location += amount_read;
// 			fwrite(data_buffer, 1, amount_read, output);
// 		}

// 		fclose(input);

// 		// delete file if option has been selected
// 		if (delete_file)
// 		{
// 			// remove read only attribute
// 			_chmod(buffer, _S_IREAD | _S_IWRITE);
// 			remove(buffer);
// 		}
// 	}

// 	// go to fourth byte in file
// 	fseek(output, sizeof(int), SEEK_SET);

// 	for (i = 0; i < num_files; i++)
// 	{
// 		fwrite(&file_list[i], 17, 1, output);
// 	}

// 	dummy.file_location = file_offset_location;

// 	fwrite(&dummy, sizeof(FILENAME), 1, output);

// 	fclose(output);

// 	if (file_list)
// 		delete [] file_list;
}

bool compact(char *file_name, /*BOOL*/int remove_comments)
{
	int big_buffer[BIG_BUFFER_SIZE];
	int mini_buffer[MINI_BUFFER_SIZE];
	int i;

	unsigned char left[TABLE_SIZE], right[TABLE_SIZE];

// read in the file
	FILE *stream;

	if ((stream = fopen(file_name, "rb")) == NULL)
	{
		return false;
	}

	int buf_size = 0;
	unsigned char *buffer = NULL;
	int amount_read;
	int total_amount_read = 0;

	do
	{
		buf_size += MIN_BUFFER_SIZE;
		buffer = (unsigned char*)realloc(buffer, buf_size);
		amount_read = fread(buffer + total_amount_read, 1, MIN_BUFFER_SIZE, stream);
		total_amount_read += amount_read;
	} while (amount_read == MIN_BUFFER_SIZE);

	fclose(stream);

// remove all comments?
	int j = 0;

	if (remove_comments)
	{
		for (i = 0; j < total_amount_read - 1; i++)
		{
			if (buffer[i] == '/' && buffer[i + 1] == '/')
			{
				i += 2;
				total_amount_read -= 2;
				// skip the rest of the line
				while (j < total_amount_read)
				{
					if (buffer[i] == 13 || buffer[i] == '\n')
					{
						i--;
						break;
					}
					i++;
					total_amount_read--;
				}
			}
			else
			{
				buffer[j] = buffer[i];
				j++;
			}
		}
	}

	if (j < total_amount_read)
	{
		buffer[j] = buffer[i];
	}

// create the table now

	int max_value;
	int max_value_location;
	int used_pairs = 0;

	unsigned char left_char, right_char;

	int location;
	int* buf_ptr;
	unsigned char* dest_ptr, *src_ptr;
	unsigned char newchar;
	
	// clear the big buffer
	buf_ptr = big_buffer;
	for (i = 0; i < BIG_BUFFER_SIZE / 4; i++)
	{
		// unroll the loop, makes it faster, possibly at least
		*buf_ptr = 0;
		buf_ptr++;
		*buf_ptr = 0;
		buf_ptr++;
		*buf_ptr = 0;
		buf_ptr++;
		*buf_ptr = 0;
		buf_ptr++;
	}

	// clear the small buffer
	buf_ptr = mini_buffer;
	for (i = 0; i < MINI_BUFFER_SIZE / 4; i++)
	{
		*buf_ptr = 0;
		buf_ptr++;
		*buf_ptr = 0;
		buf_ptr++;
		*buf_ptr = 0;
		buf_ptr++;
		*buf_ptr = 0;
		buf_ptr++;
	}	

	// count the instances of each pair
	src_ptr = buffer;
	for (i = 0; i < total_amount_read - 1; i++)
	{
		location = ((int)*src_ptr << 8) + (int)*(src_ptr + 1);

		big_buffer[location]++;
		mini_buffer[location >> 4]++;
		src_ptr++;
	}

	// create the table and compress
	for (used_pairs = 0; used_pairs < TABLE_SIZE - 1; used_pairs++)
	{
		max_value = 0;
		buf_ptr = mini_buffer;
		
		// find the most common pair now
		for (i = 0; i < MINI_BUFFER_SIZE; i++)
		{
			if (*buf_ptr > max_value)
			{
				for (int x = 0; x < 16; x++)
				{
					location = (i << 4) + x;
					if (big_buffer[location] > max_value)
					{
						max_value = big_buffer[location];
						max_value_location = location;
					}
				}
			}
			buf_ptr++;
		}

		// add the pair to the table
		if (max_value > 2)
		{
			left_char = left[used_pairs] = max_value_location >> 8;
			right_char = right[used_pairs] = max_value_location & 0xFF;
		}
		else
			break;

		// replace all instances of that pair in the buffer
		src_ptr = buffer;
		dest_ptr = buffer;
		newchar = used_pairs + TABLE_SIZE;
		for (i = 0; i < total_amount_read - 1; i++)
		{
			if (*src_ptr == left_char && *(src_ptr + 1) == right_char)
			{
				// remove left pair
				if (i != 0)
				{
					location = (*(src_ptr - 1) << 8) + *src_ptr;
					big_buffer[location]--;
					mini_buffer[location >> 4]--;

					// add new pair
					location = (*(src_ptr - 1) << 8) + newchar;
					big_buffer[location]++;
					mini_buffer[location >> 4]++;
				}
				// remove right pair
				if (i != total_amount_read - 2)
				{
					location = (*(src_ptr + 1) << 8) + *(src_ptr + 2);
					big_buffer[location]--;
					mini_buffer[location >> 4]--;

					// add new pair
					location = (newchar << 8) + *(src_ptr + 2);
					big_buffer[location]++;
					mini_buffer[location >> 4]++;
				}

				*dest_ptr = newchar;
				src_ptr++;
				total_amount_read--;
			}
			else
			{
				*dest_ptr = *src_ptr;
			}
			src_ptr++;
			dest_ptr++;
		}

		if (i == total_amount_read - 1)
			*dest_ptr = *src_ptr;

		// remove middle pairs
		big_buffer[max_value_location] = 0;
		mini_buffer[max_value_location >> 4] -= max_value;
	}

// write out the file now
	file_name[strlen(file_name) - 1] = 'd';

	if ((stream = fopen(file_name, "wb")) == NULL)
	{
		delete [] buffer;
		return false;
	}

	unsigned char num_pairs = used_pairs + TABLE_SIZE;

	fwrite(&num_pairs, 1 , 1, stream);

	for (i = 0; i < used_pairs; i++)
	{
		fwrite(&left[i], 1 , 1, stream);
		fwrite(&right[i], 1 , 1, stream);
	}

	fwrite(buffer, 1, total_amount_read, stream);

	fclose(stream);

	if (buffer)
		delete [] buffer;

	return true;
}

void compressAll(/*BOOL*/int delete_file, /*BOOL*/int remove_comments)
{
// TODO:
// 	HANDLE hFind;
// 	WIN32_FIND_DATA dataFind;
// 	/*BOOL*/int bMoreFiles = /*TRUE*/1;

// 	char buffer[250];
// 	sprintf(buffer, "%sstory\\*.c", current_map->dmod_path);

// 	hFind = FindFirstFile(buffer, &dataFind);

// 	// get all the *.c files in the directory
// 	while(hFind != INVALID_HANDLE_VALUE && bMoreFiles == /*TRUE*/1)
// 	{
// 		sprintf(buffer, "%sstory\\%s", current_map->dmod_path, dataFind.cFileName);
// 		if (compact(buffer, remove_comments) == true)
// 		{
// 			if (delete_file)
// 			{
// 				sprintf(buffer, "%sstory\\%s", current_map->dmod_path, dataFind.cFileName);
// 				remove(buffer);
// 			}
// 		}

// 		bMoreFiles = FindNextFile(hFind, &dataFind);
// 	}

// 	FindClose(hFind);
}
