/**
 * Minimap preview

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _WINDINKEDIT_MINIMAP_H
#define _WINDINKEDIT_MINIMAP_H

#include <glib-object.h>
#include "SDL.h"

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_MINIMAP         (windinkedit_minimap_get_type ())
#define WINDINKEDIT_MINIMAP(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_MINIMAP, WindinkeditMinimap))
#define WINDINKEDIT_MINIMAP_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_MINIMAP, WindinkeditMinimapClass))
#define WINDINKEDIT_IS_MINIMAP(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_MINIMAP))
#define WINDINKEDIT_IS_MINIMAP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_MINIMAP))
#define WINDINKEDIT_MINIMAP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_MINIMAP, WindinkeditMinimapClass))

#ifndef _WINDINKEDIT_TYPEDEF_MINIMAP
#define _WINDINKEDIT_TYPEDEF_MINIMAP
typedef struct _WindinkeditMinimap WindinkeditMinimap;
#endif
typedef struct _WindinkeditMinimapClass WindinkeditMinimapClass;
typedef struct _WindinkeditMinimapPrivate WindinkeditMinimapPrivate;
  
struct _WindinkeditMinimap {
  GObject parent;
  SDL_Surface* image;
  /*< private >*/
  WindinkeditMinimapPrivate* _priv;
};

struct _WindinkeditMinimapClass {
  GObjectClass parent;
  guint progress_signal_id;
};

/* used by WINDINKEDIT_TYPE_MINIMAP */
GType windinkedit_minimap_get_type(void);


/* API. */
int windinkedit_minimap_getScreenAt(WindinkeditMinimap *self, SDL_Surface* surface, int x, int y);
void windinkedit_minimap_stopRendering(WindinkeditMinimap *self);

int  windinkedit_minimap_drawSquares(WindinkeditMinimap *self);
int  windinkedit_minimap_drawMap(WindinkeditMinimap *self, SDL_Surface* surface);
int  windinkedit_minimap_updateScreen(WindinkeditMinimap *self, int screen_idx);
int  windinkedit_minimap_refreshScreen(WindinkeditMinimap *self, int screen_idx, SDL_Surface* surface);
int  windinkedit_minimap_renderMapSquare(WindinkeditMinimap *self, int cur_screen);
int  windinkedit_minimap_renderImportMap(WindinkeditMinimap *self, int screen_order[], int midi_num[], int indoor[]);
void windinkedit_minimap_render(WindinkeditMinimap *self, SDL_Surface* surface);
void windinkedit_minimap_highlight(WindinkeditMinimap *self, int screen_idx, SDL_Surface* surface);
  
int windinkedit_minimap_loadSquare(WindinkeditMinimap *self, const char *filename, int square_num);
int windinkedit_minimap_loadSurface(WindinkeditMinimap *self);
  
G_END_DECLS

#endif
