#include <stdlib.h>
#include <gtk/gtk.h>

int main(int argc, char* argv[])
{
  gtk_init(&argc, &argv);
  GtkWidget* window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  g_signal_connect_swapped(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
  gtk_widget_show(window);
  gtk_main();
  return EXIT_SUCCESS;
}

/**
 * Local Variables:
 * compile-command: "gcc -Wall `pkg-config gtk+-2.0 --cflags --libs` main.c -o main"
 * End:
 */
