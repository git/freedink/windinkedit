/**
 * Draw from non-C languages, and with optional subsurface

 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _WINDINKEDIT_GC_H
#define _WINDINKEDIT_GC_H

#include <glib-object.h>
#include "SDL.h"
#include "rect.h"

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_GC		  (windinkedit_gc_get_type ())
#define WINDINKEDIT_GC(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_GC, WindinkeditGC))
#define WINDINKEDIT_GC_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_GC, WindinkeditGCClass))
#define WINDINKEDIT_IS_GC(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_GC))
#define WINDINKEDIT_IS_GC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_GC))
#define WINDINKEDIT_GC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_GC, WindinkeditGCClass))

#ifndef _WINDINKEDIT_TYPEDEF_GC
#define _WINDINKEDIT_TYPEDEF_GC
typedef struct _WindinkeditGC WindinkeditGC;
#endif
typedef struct _WindinkeditGCClass WindinkeditGCClass;
typedef struct _WindinkeditGCPrivate WindinkeditGCPrivate;

struct _WindinkeditGC {
  GObject parent;
  SDL_Surface *surface;
  SDL_Rect arect;

  /*< private >*/
  WindinkeditGCPrivate* _priv;
};

struct _WindinkeditGCClass {
  GObjectClass parent;
};

/* used by WINDINKEDIT_TYPE_GC */
GType windinkedit_gc_get_type (void);


/* API. */
WindinkeditGC* windinkedit_gc_new();
int windinkedit_gc_set_active_rect(WindinkeditGC* self, int x, int y, int w, int h);
int windinkedit_gc_clear_active_rect(WindinkeditGC* self);
int windinkedit_gc_flip(WindinkeditGC* self);
int windinkedit_gc_blit_surface(WindinkeditGC* self, SDL_Surface* src, struct rect* srcrect, struct rect* dstrect);
int windinkedit_gc_fill_rect(WindinkeditGC* self, int x, int y, int w, int h, int r, int g, int b);
int windinkedit_gc_get_relative_clip_rect(WindinkeditGC* self, struct rect* clip);

G_END_DECLS

#endif
