/**
 * Main D-Mod data structure

 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _WINDINKEDIT_DMOD_H
#define _WINDINKEDIT_DMOD_H

#include <glib-object.h>
#ifndef _WINDINKEDIT_TYPEDEF_MAP
#define _WINDINKEDIT_TYPEDEF_MAP
typedef struct _WindinkeditMap WindinkeditMap;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_SEQUENCE
#define _WINDINKEDIT_TYPEDEF_SEQUENCE
typedef struct _WindinkeditSequence WindinkeditSequence;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_TILE
#define _WINDINKEDIT_TYPEDEF_TILE
typedef struct _WindinkeditTile WindinkeditTile;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_MINIMAP
#define _WINDINKEDIT_TYPEDEF_MINIMAP
typedef struct _WindinkeditMinimap WindinkeditMinimap;
#endif

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_DMOD         (windinkedit_dmod_get_type ())
#define WINDINKEDIT_DMOD(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_DMOD, WindinkeditDmod))
#define WINDINKEDIT_DMOD_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_DMOD, WindinkeditDmodClass))
#define WINDINKEDIT_IS_DMOD(obj)	    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_DMOD))
#define WINDINKEDIT_IS_DMOD_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_DMOD))
#define WINDINKEDIT_DMOD_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_DMOD, WindinkeditDmodClass))

#ifndef _WINDINKEDIT_TYPEDEF_DMOD
#define _WINDINKEDIT_TYPEDEF_DMOD
typedef struct _WindinkeditDmod WindinkeditDmod;
#endif
typedef struct _WindinkeditDmodClass WindinkeditDmodClass;
typedef struct _WindinkeditDmodPrivate WindinkeditDmodPrivate;
  
struct _WindinkeditDmod {
  GObject parent;
  /*< private >*/
  WindinkeditDmodPrivate* _priv;
};

struct _WindinkeditDmodClass {
  GObjectClass parent;
  guint close_signal_id;
};

/* used by WINDINKEDIT_TYPE_DMOD */
GType windinkedit_dmod_get_type(void);


/* API. */
WindinkeditDmod* windinkedit_dmod_new();
int windinkedit_dmod_open(WindinkeditDmod* self, gchar* refdir, gchar* dmoddir);
int windinkedit_dmod_save(WindinkeditDmod* self);
const gchar* windinkedit_dmod_get_refdir(WindinkeditDmod* self);
const gchar* windinkedit_dmod_get_dmoddir(WindinkeditDmod* self);
WindinkeditMap* windinkedit_dmod_get_map(WindinkeditDmod* self);
WindinkeditMinimap* windinkedit_dmod_get_minimap(WindinkeditDmod* self);
WindinkeditSequence* windinkedit_dmod_get_sequence(WindinkeditDmod* self, int idx);
WindinkeditTile* windinkedit_dmod_get_tile(WindinkeditDmod* self, int idx);

G_END_DECLS

#endif
