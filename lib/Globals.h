/**
 * Global variables declaration

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _GLOBALS_H
#define _GLOBALS_H

#include <glib.h>
#include "SDL.h"
#include "rect.h"

/* Avoid including full .h headers just for pointers */
#ifndef _WINDINKEDIT_TYPEDEF_GRAPHICS_MANAGER
#define _WINDINKEDIT_TYPEDEF_GRAPHICS_MANAGER
typedef struct _WindinkeditGraphicsManager WindinkeditGraphicsManager;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_MAP
#define _WINDINKEDIT_TYPEDEF_MAP
typedef struct _WindinkeditMap WindinkeditMap;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_SPRITE_EDITOR
#define _WINDINKEDIT_TYPEDEF_SPRITE_EDITOR
typedef struct _WindinkeditSpriteEditor WindinkeditSpriteEditor;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_TILE
#define _WINDINKEDIT_TYPEDEF_TILE
typedef struct _WindinkeditTile WindinkeditTile;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_DMOD
#define _WINDINKEDIT_TYPEDEF_DMOD
typedef struct _WindinkeditDmod WindinkeditDmod;
#endif

// defined by the dink engine and can't be changed

#define SCREEN_WIDTH		600		// width in pixels, not memory size
#define SCREEN_HEIGHT		400

#define NUM_HARD_TILES		800

#define NUM_SCREENS			768
#define MAP_COLUMNS			32
#define MAP_ROWS			24

/* This is the size of the game side-bar (the one who gets "Screen
   Locked" written on it). Game sprite coordinates take it into
   account. In this multi-scren editor, we simplify coordinates by
   removing those extra 'x' pixels. This is still used in some
   coordinates computations. */
#define SIDEBAR_WIDTH		20

#define MAX_SPRITES			100	// only 99 used, 100 available in map.dat though

#define SCREEN_DATA_SIZE	31280

#define BASE_SCRIPT_MAX_LENGTH	21

// not defined by the dink engine

#define DEFAULT_MAX_UNDO_LEVEL	50
#define DEFAULT_SCREEN_GAP	    3
#define DEFAULT_AUTOSAVE_TIME   0

#define VISIONS_TRACKED				256

#define MOUSE_BOX_LINE_WIDTH		1
#define SPRITE_SELECTOR_BMP_GAP		4
#define TILE_SELECTOR_GAP			4

#define SHOW_SCREEN						0
#define SHOW_MINIMAP					1
#define SHOW_SPRITE_SELECTOR			2
#define SHOW_TILE_SELECTOR				3
#define SHOW_HARD_TILE_SELECTOR			4
#define SHOW_SCREEN_IMPORTER			5
#define SHOW_TILE_HARDNESS_EDITOR		6
#define SHOW_SPRITE_HARDNESS_EDITOR		7

#define SPRITE_MODE		0
#define TILE_MODE		1
#define HARDBOX_MODE	2

#define MAX_PARSE_INPUTS	10
#define MAX_LINE_LENGTH		500

#define BLACK		1
#define NOTANIM		2
#define LEFTALIGN	3

#define SPRITE_SELECTOR_BMP_WIDTH	40
#define SPRITE_SELECTOR_BMP_HEIGHT	40

#define SQUARE_WIDTH		20
#define SQUARE_HEIGHT		20

//primary colors
#define COLOR_BLACK		RGB(0,0,0)
#define COLOR_RED		RGB(255,0,0)
#define COLOR_YELLOW	RGB(255,255,0)
#define COLOR_GREEN		RGB(0,255,0)
#define COLOR_BLUE		RGB(0,0,255)
#define COLOR_WHITE		RGB(255,255,255)

//timers
#define TIMER_AUTO_SAVE 1
#define TIMER_MOUSE_CHECK 2

// GLOBALS ////////////////////////////////////////////////

/* extern HWND main_window_handle; // save the window handle */

/* extern LPDIRECTDRAW7			lpdd7;			// dd4 object */
extern SDL_Surface*		lpddsback;		// dd primary/back/flip surface
extern SDL_Surface*		lpddshidden;    // dd hidden surface
/* extern LPDIRECTDRAWCLIPPER		lpddclipper;	// dd clipper */
/* extern LPDIRECTDRAWCLIPPER		lpddclipperprimary;	// dd clipper */
/* extern DDSURFACEDESC2			ddsdback;		// a direct draw surface description struct */
/* extern DDSURFACEDESC2			ddsdhidden; */

/* extern DDBLTFX hardFill;	// used for sequences */

extern WindinkeditDmod* current_dmod;
extern WindinkeditMap* current_map;

extern WindinkeditGraphicsManager* graphics_manager;

extern unsigned char vision_used[VISIONS_TRACKED];
extern unsigned char new_vision_used[VISIONS_TRACKED];

extern int/*bool*/ need_resizing;
extern int/*bool*/ sprite_hard;
extern int/*bool*/ displayhardness;
extern int/*bool*/ show_sprite_info;
extern int/*bool*/ show_minimap_progress;
extern int/*bool*/ optimize_dink_ini;
extern int/*bool*/ fast_minimap_update;
extern int/*bool*/ hover_sprite_info;
extern int/*bool*/ hover_sprite_hardness;
extern int/*bool*/ help_text;

extern int screen_gap;
extern int max_undo_level;
extern int auto_save_time;
extern int tile_brush_size;

extern int/*bool*/ update_minimap;

extern gchar *refdir; // pathname to dink directory

#endif
