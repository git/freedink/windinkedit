/**
 * Editor initialization and mode multiplexer

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef _ENGINE_H_
#define _ENGINE_H_

extern int draw_box;
extern int mouse_x_position, mouse_y_position;
extern int mouse_x_origin, mouse_y_origin;

int  Game_Init();
void Game_Shutdown();
int  Game_Main();

int resize_map_window(int new_width, int new_height);

#endif
