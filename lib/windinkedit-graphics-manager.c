/**
 * Graphics memory management

 * Copyright (C) 2001, 2002  Gary Hertel
 * Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
 * Copyright (C) 2008  Sylvain Beucler

 * This file is part of GNU FreeDink

 * GNU FreeDink is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.

 * GNU FreeDink is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "windinkedit-graphics-manager.h"
#include "Globals.h"
#include "Colors.h"

/*************/
/**  Class  **/
/**         **/
/*************/

static GObjectClass *parent_class = NULL;


/** Private fields **/

#define selfp (self->_priv)

struct _WindinkeditGraphicsManagerPrivate {
  int isVideoCardOnlyMemory;	
  int videoMemoryUsed;
  GRAPHICQUE* head;
  GRAPHICQUE* tail;
};

static int removeGraphic();


static void
___instance_init(GTypeInstance   *instance,
		 gpointer         g_class)
{
  WindinkeditGraphicsManager* self = (WindinkeditGraphicsManager*)instance;
  self->_priv = G_TYPE_INSTANCE_GET_PRIVATE(self, WINDINKEDIT_TYPE_GRAPHICS_MANAGER, WindinkeditGraphicsManagerPrivate);

  selfp->videoMemoryUsed = 0;
  selfp->isVideoCardOnlyMemory = 0;
  
  selfp->head = g_malloc0(sizeof(GRAPHICQUE));
  selfp->tail = g_malloc0(sizeof(GRAPHICQUE));
  
  selfp->head->next = selfp->tail;
  selfp->tail->prev = selfp->head;	
}

/**
 * Clean-up object (after dispose)
 */
static void
___finalize(GObject *obj)
{
  WindinkeditGraphicsManager *self = (WindinkeditGraphicsManager *)obj;

  windinkedit_graphics_manager_empty(self);

  if (selfp->head)
    {
      g_free(selfp->head);
      selfp->head = NULL;
    }
  
  if (selfp->tail)
    {
      g_free(selfp->tail);
      selfp->tail = NULL;
    }
  
  /* Chain up to the parent class */
  G_OBJECT_CLASS(parent_class)->finalize(obj);
}


static void
___class_init(gpointer g_class,
	      gpointer g_class_data)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
  WindinkeditGraphicsManagerClass *klass = WINDINKEDIT_GRAPHICS_MANAGER_CLASS (g_class);

  gobject_class->finalize = ___finalize;

  g_type_class_add_private(g_class, sizeof(WindinkeditGraphicsManagerPrivate));
  parent_class = G_OBJECT_CLASS(g_type_class_peek_parent(klass));
}

GType windinkedit_graphics_manager_get_type()
{
  static GType type = 0;
  if (type == 0) {
    static const GTypeInfo info = {
      sizeof (WindinkeditGraphicsManagerClass),
      NULL,   /* base_init */
      NULL,   /* base_finalize */
      (GClassInitFunc) ___class_init,   /* class_init */
      NULL,   /* class_finalize */
      NULL,   /* class_data */
      sizeof (WindinkeditGraphicsManager),
      0,      /* n_preallocs */
      ___instance_init   /* instance_init */
    };
    type = g_type_register_static (G_TYPE_OBJECT,
				   "WindinkeditGraphicsManagerType",
				   &info, (GTypeFlags)0);
  }
  return type;
}


/***********/
/**  API  **/
/**       **/
/***********/

void windinkedit_graphics_manager_empty(WindinkeditGraphicsManager* self)
{
  // if empty has already been called exit
  if (selfp->head == NULL || selfp->tail == NULL)
    return;

  if (selfp->head->next != selfp->tail)
    {
      GRAPHICQUE* temp = selfp->head->next->next;

      while (temp != selfp->tail)
	{
	  g_free(temp->prev);
	  temp = temp->next;
	}
      g_free(temp->prev);
    }
  
  selfp->head->next = selfp->tail;
  selfp->tail->prev = selfp->head;
}

SDL_Surface* windinkedit_graphics_manager_allocateSurface(WindinkeditGraphicsManager* self, int width, int height)
{
  SDL_Surface *tmp = SDL_CreateRGBSurface(SDL_SWSURFACE,
					  width, height, 24,
					  0, 0, 0, 0);
  SDL_Surface *retval = SDL_DisplayFormat(tmp);
  SDL_FreeSurface(tmp);
  
  if (retval != NULL)
    // TODO: what about pitch / memory alignment?
    selfp->videoMemoryUsed += width * height * colorformat;
  return retval;
}

GRAPHICQUE* windinkedit_graphics_manager_addGraphic(WindinkeditGraphicsManager* self, SDL_Surface** surface)
{
  // fill in the new graphic que structure
  GRAPHICQUE* temp = g_malloc0(sizeof(GRAPHICQUE));
  temp->surface = surface;
  temp->prev = selfp->tail->prev;

  // set the old tails next to the new soon to be tail
  selfp->tail->prev->next = temp;

  // tail->prev should now equal the latest entry
  selfp->tail->prev = temp;
  temp->next = selfp->tail;
  
  return temp;
}

int windinkedit_graphics_manager_moveLocation(WindinkeditGraphicsManager* self, GRAPHICQUE* location)
{
  // delete old references
  location->prev->next = location->next;
  location->next->prev = location->prev;
  
  // create new references
  location->prev = selfp->tail->prev;
  location->next = selfp->tail;
  
  // set the old tails next to the new soon to be tail
  selfp->tail->prev->next = location;
  
  // tail should now equal the latest entry
  selfp->tail->prev = location;
  
  return 1/*true*/;
}

// removes all graphics that reside in video memory
int windinkedit_graphics_manager_removeAllVideoMemGraphics(WindinkeditGraphicsManager* self)
{
  GRAPHICQUE* temp = selfp->head->next;
  GRAPHICQUE* temp2;

  SDL_Surface** surface;
  
  while (temp != selfp->tail)
    {
      temp2 = temp->next;
      surface = temp->surface;
      
      if (*surface)
	{
	  selfp->videoMemoryUsed -= colorformat * (*surface)->w * (*surface)->h;
	  SDL_FreeSurface(*surface);
	  *surface = NULL;
	}
      
      // delete old references
      temp->prev->next = temp->next;
      temp->next->prev = temp->prev;
      
      g_free(temp);

      temp = temp2;
    }

  return 1/*true*/;
}

int windinkedit_graphics_manager_removeGraphics(WindinkeditGraphicsManager* self, int amount)
{
  if (!selfp->isVideoCardOnlyMemory)
    {
      int target_memory = selfp->videoMemoryUsed - amount;
      
      if (target_memory < 0)
	target_memory = 0;	// ERROR
      
      while (target_memory < selfp->videoMemoryUsed)
	{
	  if (removeGraphic() == 0/*false*/)
	    return 0/*false*/;
	}
      return 1/*true*/;
    }
  else
    {
			
// TODO: code used to check the total available video memory -
// there's no such thing in SDL, and apparently this is only
// available in DX7.
// 	int total_video_memory; 
// 	int free_video_memory; 

// 	int begin_memory = free_video_memory;

// 	while (free_video_memory < begin_memory + amount)
// 	{
// 		if (removeGraphic() == false)
// 			return false;
// 		hr = lpdd7->GetAvailableVidMem(&ddsCaps2, &total_video_memory, &free_video_memory);
// 	}

      return 1/*true*/;
    }
}

int/*bool*/ windinkedit_graphics_manager_checkMemory(WindinkeditGraphicsManager* self)
{
  if (!selfp->isVideoCardOnlyMemory)
    {
      if (selfp->videoMemoryUsed > 16000000)
	{
	  if (windinkedit_graphics_manager_removeGraphics(self, selfp->videoMemoryUsed - 16000000) == 0)
	    return 0;
	}
    }
  return 1;
}


// removes a graphic from the head of the list
static int removeGraphic(WindinkeditGraphicsManager* self)
{
  if (selfp->head->next == selfp->tail)
    return 0/*false*/;
  
  SDL_Surface** surface = selfp->head->next->surface;

  if (*surface)
    {
      selfp->videoMemoryUsed -= colorformat * (*surface)->w * (*surface)->h;
      
      SDL_FreeSurface(*surface);
      /* Set the _destination_ pointer to NULL, so the matching
	 Sequence or HardTile object detects it and reloads the
	 picture when it's needed again */
      *surface = NULL;
    }
  
  GRAPHICQUE* temp = selfp->head->next->next;
  
  temp->prev = selfp->head;
  
  // delete old graphic record
  g_free(selfp->head->next);
  selfp->head->next = temp;

  return 1/*true*/;
}
