#ifndef WINDINKEDIT_MAIN_H
#define WINDINKEDIT_MAIN_H

#include <glib-object.h>
#include <gdk/gdk.h>
#include "SDL.h"
#ifndef _WINDINKEDIT_TYPEDEF_MAP
#define _WINDINKEDIT_TYPEDEF_MAP
typedef struct _WindinkeditMap WindinkeditMap;
#endif
#ifndef _WINDINKEDIT_TYPEDEF_MINIMAP
#define _WINDINKEDIT_TYPEDEF_MINIMAP
typedef struct _WindinkeditMinimap WindinkeditMinimap;
#endif

G_BEGIN_DECLS

#define WINDINKEDIT_TYPE_MAIN		  (windinkedit_main_get_type ())
#define WINDINKEDIT_MAIN(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), WINDINKEDIT_TYPE_MAIN, WindinkeditMain))
#define WINDINKEDIT_MAIN_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), WINDINKEDIT_TYPE_MAIN, WindinkeditMainClass))
#define MAMAN_IS_BAR(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), WINDINKEDIT_TYPE_MAIN))
#define MAMAN_IS_BAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), WINDINKEDIT_TYPE_MAIN))
#define WINDINKEDIT_MAIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), WINDINKEDIT_TYPE_MAIN, WindinkeditMainClass))

typedef struct _WindinkeditMain WindinkeditMain;
typedef struct _WindinkeditMainClass WindinkeditMainClass;

struct _WindinkeditMain {
  GObject parent;
};

struct _WindinkeditMainClass {
  GObjectClass parent;
};

/* used by WINDINKEDIT_TYPE_MAIN */
GType windinkedit_main_get_type (void);

/* API. */
void windinkedit_main_rebuild_screen (WindinkeditMain *self);
void windinkedit_main_mouse_move (WindinkeditMain *self, int x, int y);
void windinkedit_main_resize (WindinkeditMain *self, int w, int h);
void windinkedit_main_mouse_left_pressed (WindinkeditMain *self, int x, int y);
/* WindinkeditMap* windinkedit_main_get_map(WindinkeditMain *self); */
/* WindinkeditMinimap* windinkedit_main_get_minimap(WindinkeditMain *self); */
void windinkedit_main_mouse_test_fill(WindinkeditMain *self);
/* void windinkedit_main_map_drawScreens(WindinkeditMain *self); */
void windinkedit_main_map_centerOnScreen(WindinkeditMain *self, WindinkeditMap *current_map, int screen_maths);
SDL_Surface* windinkedit_main_map_GetVideoSurface(WindinkeditMain* self);

G_END_DECLS

#endif
