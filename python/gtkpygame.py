#!/usr/bin/env python

# Gtk widget to embed the SDL main surface using pygame

# Copyright (C) 2008  Sylvain Beucler

# This file is part of GNU FreeDink

# GNU FreeDink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# GNU FreeDink is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import sys, os

import gobject
import gtk
from gtk import gdk

import pygame

# This is a gtk.DrawingArea subclass to include the SDL display in
# your application.

class GtkPygame(gtk.DrawingArea):
    __gsignals__ = {
        'sdl-ready' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
        }
    
    def __init__(self, width, height, bitsperpixel, sdl_flags):
        # width, height, bitsperpixel, sdl_flags: parameters for
        # 'SDL_SetVideoMode'. width/height are also the default and
        # minimum size for the widget
        gtk.Widget.__init__(self)
        
        # Prevent Gtk from overwritting us with the widget backbuffer
        self.set_double_buffered(False)

        # Set the widget size - unfortunately this becomes the minimal
        # size as well. Gtk+ apparently doesn't have a "preferred" or
        # "natural" size concept, only a "minimum size". Apparently
        # the only work-around is to change the top-level window size
        # with gtk_window_set_default_size () or gtk_window_resize()
        self.set_size_request(width, height)
        
        self.sdl_initialized = False
        self.invalidate = False
        self.bitsperpixel = bitsperpixel
        self.sdl_flags = sdl_flags
        self.connect("realize", GtkPygame.on_realize)
        self.connect("unrealize", GtkPygame.on_unrealize)

    def setvideomode(self, width, height):
        # Note: set_mode bpp and flags are swapped compared to SDL_SetVideoMode 
        pygame.display.set_mode((width, height), self.sdl_flags, self.bitsperpixel)

    # GtkWidget

    def do_map_event(self, *args):
        # A 'map-event' signal is generated when the widget is first
        # displayed.  'do_realize' is too early to init SDL, it causes a
        # X error if SDL tries to access the Window.

        # map may be called several times, for example if the widget is hidden by a maximized pane
        if self.sdl_initialized == False:
            # Perform the SDL_WINDOWID hack
            os.environ['SDL_WINDOWID'] = str(self.window.xid)
            pygame.init()
        
        self.setvideomode(self.allocation.width, self.allocation.height)
        self.connect("configure-event", GtkPygame.on_configure_event)

        if self.sdl_initialized == False:
            self.sdl_initialized = True
            self.emit('sdl-ready')

    def on_realize(self):
        # The following line prevents drawing an unnecessary
        # background; however when the program is slow on startup, or
        # crashes/freezes, this results in some kind of transparent
        # window, and this is very confusing, so let's avoid it.
        # "The windowing system will normally fill a window with its
        # background when the window is obscured then exposed, and
        # when you call gdk_window_clear()." --
        # http://library.gnome.org/devel/gdk/stable/gdk-Windows.html#gdk-window-set-back-pixmap
        #self.window.set_back_pixmap(None, False)

        # However the default white background is startling, let's use
        # a safer default
        self.window.set_background(self.get_style().black)
        
    def on_unrealize(self):
        # The do_unrealized method is responsible for freeing the GDK resources
        pygame.quit()

    def on_configure_event(self, allocation):
        # The configure event means the internal gdk.Window moved and
        # has new dimensions.  This event is here synthetized by the
        # DrawingArea, whereas GTK+ normally only sends GDK_CONFIGURE
        # to top-level windows. It doesn't really match X's
        # ConfigureNotify event (our target), as it's rather emitted
        # in DrawingArea's size_allocate.
        if (self.sdl_flags & pygame.VIDEORESIZE == pygame.VIDEORESIZE):
            self.setvideomode(self.allocation.width, self.allocation.height)

    def do_expose_event(self, event):
        # Sync to take the gdk.Window size and offscreen status into
        # account right now (so SDL won't be clipped in the previous
        # gdk.Window rectangle, or fail to update a previously
        # offscreen area). Apparently SDL xsyncs on its own and so the
        # X state isn't consistent if we don't xsync as well.
        display = self.get_display()
        display.sync()

        # Redraw the exposed portion of the window
        if (self.sdl_flags & pygame.OPENGL == pygame.OPENGL):
            pygame.display.flip()
        else:
            pygame.display.update(pygame.Rect(event.area.x, event.area.y,
                                              event.area.width, event.area.height))

gobject.type_register(GtkPygame)


if __name__ == '__main__':
    # Sample SDL app that changes the background color every 5
    # seconds.

    # It's a bare window because display bugs show up more easily
    # than when within a Box (which tries harder to refresh its
    # children than a top-level window) and hence the widget can
    # be better tested. Another good test is to instantiate a
    # GnomeApp and add it the widget using .set_contents(): the
    # status bar contains a gdk.Window that may stay on top of
    # SDL's if you display.flush() at the wrong time.

    # The SDL surface is also reconstructed every 5 seconds only -
    # it's not redrawn each time the 'init' is called. In between,
    # screen is refreshed using the SDL backbuffer, instead of
    # redrawing everything. This supports applications that redraw
    # screen as needed instead of doing so in a loop.

    # The screen is also reconstructed when the widget is
    # resized. This is mandatory to take the new size into
    # account, and because SDL clears the backbuffer anyway.

    import random
    import time
    def regen_screen():
        global color
        screen = pygame.display.get_surface()
        screen.fill(color)
        # Check that colors aren't swapped or anything
        pygame.draw.rect(screen, (255, 0, 0), (10, 10, 20, 20)) # R
        pygame.draw.rect(screen, (0, 255, 0), (30, 10, 20, 20)) # G
        pygame.draw.rect(screen, (0, 0, 255), (50, 10, 20, 20)) # B

    def on_sdl_ready(self):
        # Rebuild the screen content on resize
        def on_configure_event(self, allocation):
            regen_screen()
        self.connect('configure_event', on_configure_event)

        # Start the game loop
        def game_loop():
            global color
            global last_update
            if (pygame.time.get_ticks() - last_update) > 5000:
                last_update = pygame.time.get_ticks()
                # change color every 5 seconds
                r = random.randrange(255)
                g = random.randrange(255)
                b = random.randrange(255)
                color = (r, g, b)
                regen_screen()
                w.queue_draw()
    	#return False # only once
            time.sleep(.1)  # avoid 100% CPU utilization
            return True     # do it again asap
        # Attach game loop to Gtk
        gobject.idle_add(game_loop)

    w = GtkPygame(640, 480, 0, pygame.VIDEORESIZE)
    # Tell the engine to initialize after the SDL widget is mapped
    # on the screen (SDL screen initialized and visible).
    w.connect('sdl-ready', on_sdl_ready)
    
    last_update = -5000
    color = None

    win = gtk.Window()
    win.set_title('GtkPygame test')
    win.set_app_paintable(True)
    win.connect('delete-event', gtk.main_quit)
    win.add(w)
    win.show_all()
    gtk.main()
