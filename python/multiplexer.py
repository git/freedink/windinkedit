#!/usr/bin/env python

# Widgets that handle input depending on the mode

# Copyright (C) 2001, 2002  Gary Hertel
# Copyright (C) 2002, 2003  Michael S. Braecklein Jr.
# Copyright (C) 2008  Sylvain Beucler

# This file is part of GNU FreeDink

# GNU FreeDink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# GNU FreeDink is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.


from gettext import gettext as _

import gtk.gdk
import pygame

from gtkpygame import GtkPygame
import windinkedit

import time

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 400
MAP_COLS=32
MAP_ROWS=24

class Multiplexer(GtkPygame):
    def __init__(self, status_bar):
        GtkPygame.__init__(self, 640, 480, 0, pygame.VIDEORESIZE)
        self.game_initialized = False
        self.status = status_bar
        self.engine = windinkedit.Main()
        self.busy = False
        # SDL init
        self.connect_after('sdl-ready', Multiplexer.on_sdl_ready)
        # Proxy events
        self.connect('motion-notify-event', Multiplexer.on_motion_notify_event)
        self.connect('button-press-event',  Multiplexer.on_button_press_event)
        self.connect('key-press-event',     Multiplexer.on_key_press_event)
        self.connect('key-release-event',   Multiplexer.on_key_release_event)
        self.connect('expose-event', Multiplexer.on_expose_event)
        self.connect('focus-in-event',  Multiplexer.on_focus_in_event)
        self.connect('focus-out-event', Multiplexer.on_focus_out_event)
        # Enable mouse events
        self.set_events(self.get_events()
                        #| gtk.gdk.KEY_PRESS_MASK  # default
                        | gtk.gdk.POINTER_MOTION_MASK
                        | gtk.gdk.BUTTON_PRESS_MASK | gtk.gdk.BUTTON_RELEASE_MASK
                        | gtk.gdk.POINTER_MOTION_HINT_MASK)
        # Tell Gtk that we can grab the keyboard focus (and events)
        self.set_flags(self.flags() | gtk.CAN_FOCUS)

    def switch_mode(self, mode):
        self.mode = mode
        self.redraw()
        

    # Start SDL when we're ready to draw
    def on_sdl_ready(self):
        windinkedit.init()
        self.dmod = windinkedit.Dmod()
        retval = self.dmod.open("/downloads/dink/dink", "/downloads/dink/dink")
        if retval != 1:
            print "Cannot open DMod"
            sys.exit
        self.game_initialized = True
        self.map = self.dmod.get_map()
        self.minimap = self.dmod.get_minimap()
        self.map.set_graphics_size(640, 480);
        def callback_progress(minimap, cur_screen):
            self.status.set_progress_percentage(1.0 * cur_screen / 768)
            # Force GUI update
            while gtk.events_pending():
                gtk.main_iteration()
        self.minimap.connect('progress', callback_progress)
        self.connect('size-allocate', Multiplexer.on_configure_event)

        self.surface = pygame.display.get_surface()
        self.sdl_surface = windinkedit.pygame2sdl(self.surface)
        self.screen_mode = ScreenMode(self.map, self)
        self.minimap_mode = MinimapMode(self.minimap, self)
        self.switch_mode(self.minimap_mode)
        self.engine.rebuild_screen()
        # Get keyboard focus (instead of confusingly setting it to the
        # menu bar by default)
        self.grab_focus()

        from gtkimagesdl import GtkImageSDL
        win = gtk.Window()
        win.set_app_paintable(True)
        g = GtkImageSDL(640, 480)
        g.set_events(self.get_events()
                     | gtk.gdk.KEY_PRESS_MASK  # default
                     | gtk.gdk.POINTER_MOTION_MASK
                     | gtk.gdk.BUTTON_PRESS_MASK | gtk.gdk.BUTTON_RELEASE_MASK
                     | gtk.gdk.POINTER_MOTION_HINT_MASK)
        g.set_flags(g.flags() | gtk.CAN_FOCUS)
        g.status = self.status
        gscreen_mode = ScreenMode(self.map, g)
        def cb_expose(self, event):
            #print "cb_expose"
            gscreen_mode.redraw()
            return False
        g.connect("expose-event", cb_expose)
        def cb_key(self, event):
            return gscreen_mode.on_key_press_event(event)
        g.connect("key-press-event", cb_key)
        def cb_motion(self, event):
            self.grab_focus()
        g.connect("motion-notify-event", cb_motion)

        win.add(g)
        win.show_all()
        

    def on_focus_in_event(self, event):
        return True # don't redraw the whole area
    def on_focus_out_event(self, event):
        return True # don't redraw the whole area

    # Proxy event handlers
    def on_motion_notify_event(self, motion_event):
        self.grab_focus() # grab keyboard focus on mouseover
        self.mode.on_motion_notify_event(motion_event)
    def on_button_press_event(self, event):
        self.grab_focus() # grab keyboard focus on click
        self.mode.on_button_press_event(event)
    def on_key_press_event(self, event):
        return self.mode.on_key_press_event(event)
    def on_key_release_event(self, event):
        return self.mode.on_key_release_event(event)
    def on_configure_event(self, allocation):
        # Replicate resize on all modes even if not currently active
        self.minimap_mode.on_configure_event(allocation)
        self.screen_mode.on_configure_event(allocation)
	#windinkedit_sprite_selector_resizeScreen(sprite_selector);
 	#current_map->hard_tile_selector.resizeScreen();
        self.redraw()

    def on_expose_event(self, event):
        self.mode.redraw()
        # TODO: passe the area to redraw

    def redraw(self):
        self.mode.redraw()
        # Generic refresh
        self.queue_draw()
	#graphics_manager.checkMemory();



class Mode:
    def on_motion_notify_event(self, motion_event):
        pass
    
    def on_button_press_event(self, event):
        pass
    
    def on_key_press_event(self, event):
        pass

    def on_key_release_event(self, event):
        pass
    
    def on_configure_event(self, allocation):
        pass
    
    def redraw(self):
        pass

class MinimapMode(Mode):
    def __init__(self, minimap, widget):
        self.minimap = minimap
        self.widget = widget
        self.last_x = 0
        self.last_y = 0
        self.last_screen = 0

    def on_motion_notify_event(self, motion_event):
        #print "on_motion_notify_event", motion_event.x, motion_event.y, motion_event.is_hint
        x = int(motion_event.x)
        y = int(motion_event.y)
        if (motion_event.is_hint):
            #state = motion_event.window.get_pointer()
            #x = state[0]
            #y = state[1]
            pass
        self.widget.engine.mouse_move(x, y)
        # The last motion we get may be different than the final
        # mouse position, in which case the display processing
        # does not match the mouse position at the end of the
        # series of mouse motions, which confuses the
        # user. event_request_motions should fix this, by
        # requesting more mouse events once we're done with the
        # processing, but apparently it does not work if the mouse
        # stopped moving when that function is called. So instead
        # we use the gtk.window.get_pointer() trick above, which
        # is not recommended by the documentation.

        #widget.status.set_status("screen: %.3d, x: %.3d, y: %.3d" % (hover_screen, 0, 0))
        cur_screen = self.minimap.getScreenAt(self.widget.sdl_surface, x, y)
        self.widget.status.set_status("screen: %.3d, x: %.3d, y: %.3d" % (cur_screen, 0, 0))

        self.minimap.refreshScreen(self.last_screen, self.widget.sdl_surface)
        self.widget.queue_draw_area(self.last_x/20*20, self.last_y/20*20, 20, 20)

        self.minimap.highlight(cur_screen, self.widget.sdl_surface)
        self.widget.queue_draw_area(x/20*20, y/20*20, 20, 20)

        gtk.gdk.event_request_motions(motion_event)
        self.last_screen = cur_screen
        self.last_x = int(x)
        self.last_y = int(y)

    def on_button_press_event(self, event):
        self.widget.engine.mouse_left_pressed(int(event.x), int(event.y))
        # int x_location = selfp->hover_x_screen * (SCREEN_WIDTH + screen_gap) + SCREEN_WIDTH / 2 - (selfp->map->window_width / 2);
        # int y_location = selfp->hover_y_screen * (SCREEN_HEIGHT + screen_gap) + SCREEN_HEIGHT / 2 - (selfp->map->window_height / 2);

        screen_maths = self.minimap.getScreenAt(self.widget.sdl_surface, int(event.x), int(event.y)) - 1
        self.widget.engine.map_centerOnScreen(self.widget.map, screen_maths)
        self.widget.screen_mode.center_on_screen(screen_maths)
        self.widget.switch_mode(self.widget.screen_mode)
        return False

    def on_key_press_event(self, event):
        # Space - compute minimap
        if (event.keyval == gtk.gdk.keyval_from_name('space')):
            self.widget.busy = True
            self.widget.status.set_status(_("Minimap detailing in progress (ESC to stop)"))
            # compute minimap
            complete = self.minimap.drawMap(self.widget.sdl_surface)
            self.widget.status.set_progress_percentage(0)
            if (complete < 0):
                self.widget.status.set_status(_("Aborted minimap detailing"))
            else:
                self.widget.status.set_status(_("Finished minimap detailing"))
            self.widget.busy = False
        elif (event.keyval == gtk.gdk.keyval_from_name('Escape')):
            self.minimap.stopRendering()
        print gtk.gdk.keyval_name(event.keyval)

    def on_configure_event(self, allocation):
	#windinkedit_minimap_resizeScreen(current_map->minimap);
        pass

    def redraw(self):
        # Specific
        self.minimap.render(self.widget.sdl_surface)

        #ScreenText("Press F12 to Toggle this text, and F5 to change it's color.", 70);
        #ScreenText("Press Spacebar to detail minimap. Click a square to view it.", 50);
        #ScreenText("Right click on any map square to select it's details.", 30);


class ScreenMode(Mode):
    def __init__(self, map, widget):
        self.map = map
        self.widget = widget
 
        self.cur_vision = 0
        self.view_x = 0
        self.view_y = 0
        self.gap = 1 # grid width
        self.move_step = 10

    def update_status(self, x , y):
        coords = self.get_screen_pos(x, y)
        if (coords[1] >= SCREEN_WIDTH or coords[2] >= SCREEN_HEIGHT):
            coords = (0, 0, 0)
        self.widget.status.set_status("screen: %.3d, x: %.3d, y: %.3d" % coords)

    def on_motion_notify_event(self, motion_event):
        self.update_status(motion_event.x, motion_event.y)

    def on_button_press_event(self, event):
        pass

    def on_key_press_event(self, event):
        # Tab - back to minimap
        if (event.keyval == gtk.gdk.keyval_from_name('Tab')
            or event.keyval == gtk.gdk.keyval_from_name('Escape')):
            self.widget.switch_mode(self.widget.minimap_mode)
            return True # don't shift focus
        # Arrow keys - move around the map
        elif (event.keyval == gtk.gdk.keyval_from_name('Left')):
            self.set_view_x(self.get_view_x() - self.move_step)
            return True
        elif (event.keyval == gtk.gdk.keyval_from_name('Right')):
            self.set_view_x(self.get_view_x() + self.move_step)
            return True
        elif (event.keyval == gtk.gdk.keyval_from_name('Up')):
            self.set_view_y(self.get_view_y() - self.move_step)
            return True
        elif (event.keyval == gtk.gdk.keyval_from_name('Down')):
            self.set_view_y(self.get_view_y() + self.move_step)
            return True
        # Shift, accelerate navigation
        elif (event.keyval == gtk.gdk.keyval_from_name('Shift_L')
              or event.keyval == gtk.gdk.keyval_from_name('Shift_R')):
            self.move_step *= 2
            return True

    def on_key_release_event(self, event):
        # Shift, stop accelerating navigation
        if (event.keyval == gtk.gdk.keyval_from_name('Shift_L')
            or event.keyval == gtk.gdk.keyval_from_name('Shift_R')):
            self.move_step /= 2
            return True

    def on_configure_event(self, allocation):
        self.map.set_graphics_size(allocation.width, allocation.height)
        # Enforce view_x/view_y consistency rules
        self.set_view_x(self.view_x)
        self.set_view_y(self.view_y)

    def redraw(self):
        """Draw an array of screens from the current view position"""
        surface = self.widget.surface
        sdl_surface = self.widget.sdl_surface
        full_width  = SCREEN_WIDTH  + self.gap
        full_height = SCREEN_HEIGHT + self.gap
        screen_left = int(1.0 * self.view_x / full_width)
        screen_top  = int(1.0 * self.view_y / full_height)
        screen_right  = min(int(1.0 * (self.view_x + self.widget.allocation.width  - 1) / full_width),  MAP_COLS)
        screen_bottom = min(int(1.0 * (self.view_y + self.widget.allocation.height - 1) / full_height), MAP_ROWS)

        rect = pygame.Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)
        offset_y = 0 - (self.view_y % full_height)
        for screen_y in range(screen_top, screen_bottom + 1):
            offset_x = -(self.view_x % full_width)
            for screen_x in range(screen_left, screen_right + 1):
                cur_screen_number = screen_y * MAP_COLS + screen_x
                cur_screen = self.map.get_screen(cur_screen_number)
                rect.topleft = (offset_x, offset_y)
                surface.set_clip(rect)
                if (cur_screen is not None):
                    cur_screen.drawTiles(sdl_surface, offset_x, offset_y)
                    cur_screen.drawSprites(self.cur_vision, sdl_surface, offset_x, offset_y)
                else:
                    self.widget.surface.fill((50,50,255), pygame.Rect(offset_x, offset_y, SCREEN_WIDTH, SCREEN_HEIGHT))
                surface.set_clip()
                offset_x += full_width
            offset_y += full_height

        # Draw grid
        grid_x = 0 - (self.view_x % full_width)
        for screen_x in range(screen_left, screen_right):
            grid_x += SCREEN_WIDTH
            self.widget.surface.fill((0,0,255), pygame.Rect(grid_x, 0, self.gap, self.widget.allocation.height))
            grid_x += self.gap

        grid_y = 0 - (self.view_y % full_height)
        for screen_y in range(screen_top, screen_bottom):
            grid_y += SCREEN_HEIGHT
            self.widget.surface.fill((0,0,255), pygame.Rect(0, grid_y, self.widget.allocation.width, self.gap))
            grid_y += self.gap
        
        pos = self.widget.window.get_pointer()
        self.update_status(pos[0], pos[1])

    def center_on_screen(self, screen_maths):
        screen_x = screen_maths % MAP_COLS
        screen_y = screen_maths / MAP_COLS
        self.set_view_x(screen_x * (SCREEN_WIDTH  + self.gap) - ((self.widget.allocation.width  - SCREEN_WIDTH)  / 2))
        self.set_view_y(screen_y * (SCREEN_HEIGHT + self.gap) - ((self.widget.allocation.height - SCREEN_HEIGHT) / 2))

    def get_view_x(self):
        return self.view_x
    def set_view_x(self, view_x):
        old_view_x = self.view_x
        self.view_x = max(0, min(view_x, MAP_COLS * (SCREEN_WIDTH + self.gap) - self.gap - self.widget.allocation.width))
        if (self.view_x != old_view_x):
            # When too many events are piling up, Gtk skips screen
            # refreshes (expose events). Thus it's better to redraw
            # the screen during an expose event (which can be skipped)
            # rather than in the keyboard event (which is always
            # processed).

            # self.redraw()
            #pygame.display.flip()
            self.widget.window.invalidate_rect(None, False) # refresh all screen

    def get_view_y(self):
        return self.view_y
    def set_view_y(self, view_y):
        old_view_y = self.view_y
        self.view_y = max(0, min(view_y, MAP_ROWS * (SCREEN_HEIGHT + self.gap) - self.gap - self.widget.allocation.height))
        if (self.view_y != old_view_y):
            self.widget.window.invalidate_rect(None, False) # refresh all screen

    def get_screen_pos(self, x, y):
        full_width  = SCREEN_WIDTH  + self.gap
        full_height = SCREEN_HEIGHT + self.gap
        offset_x = 0 - (self.view_x % full_width)
        offset_y = 0 - (self.view_y % full_height)
        screen_left = int(1.0 * self.view_x / full_width)
        screen_top  = int(1.0 * self.view_y / full_height)

        # First, which screen are we on?
        delta_x = int(x) - offset_x
        delta_y = int(y) - offset_y
        delta_screen_x = (delta_x / full_width)
        delta_screen_y = (delta_y / full_height)
        screen_x = screen_left + delta_screen_x
        screen_y = screen_top  + delta_screen_y
        screen_maths = (screen_y * MAP_COLS + screen_x)

        # Then, the screen coordinates
        resx = int(x) - delta_screen_x * full_width  - offset_x
        resy = int(y) - delta_screen_y * full_height - offset_y
        return (screen_maths + 1, resx, resy)
