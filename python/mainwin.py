#!/usr/bin/env python

# WinDinkedit GUI main window

# Copyright (C) 2003-2006 Rubens Ramos (GnoCHM)
# Copyright (C) 2008  Sylvain Beucler

# This file is part of GNU FreeDink

# GNU FreeDink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# GNU FreeDink is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.


import os
from gettext import gettext as _

import gtk
import gnome
import gnome.ui
import gtk.glade

import app
from multiplexer import Multiplexer
from ipython_view import *

class MainWin:
    class AboutBox:
        def __init__(self, widget):
            self.widget = widget
            widget.connect('delete_event', self.delete_event)

        # User clicked on "Close" button, or otherwise closed the
        # window
        def on_response(self, dialog, response, *args):
            print dialog
            self.widget.hide()

        # Window is closed but we don't destroy it so we can show it
        # again later on
        def delete_event(self, *args):
            self.widget.hide()
            return True # don't destroy the window

    # Main editor window
    def __init__(self):
        self.xml = gtk.glade.XML(os.path.join(app.PKGDATADIR, 'windinkedit.glade'),
                                 domain=app.ac_PACKAGE)

        self.widget = self.xml.get_widget('wde_mainwin')
	#self.win = gtk.Window()
        if (self.widget is None):
            print "Cannot load main window user interface."
            sys.exit(1)
        self.xml.signal_autoconnect(self)
        self.widget.connect("destroy", gtk.main_quit)

	self.aboutbox = self.xml.get_widget('wde_aboutbox')
        tmp = self.AboutBox(self.aboutbox)
        self.xml.signal_autoconnect(tmp)

        self.status = self.xml.get_widget('wde_appbar')

        self.multiplexer = Multiplexer(self.status)

        #self.win.set_contents(custom)
        hpaned1 = self.xml.get_widget('wde_pane')
        hpaned1.pack2(self.multiplexer, resize=True, shrink=False)

        self.treestore = gtk.TreeStore(str)
        visions = self.treestore.append(None, [_("Visions In Use")])
        self.treestore.append(visions, ["0"])
        self.treestore.append(None, [_("Scripts")])
        self.treestore.append(None, [_("Sprite Library")])

        self.treeview = self.xml.get_widget('wde_treeview')
        self.treeview.set_model(self.treestore)
        self.treeview.insert_column_with_attributes(0, 'Col0', gtk.CellRendererText(), text=0)
        self.treeview.set_headers_visible(False)
   
        self.status.set_status(_("Welcome to WinDinkedit!"))
        self.multiplexer.show()
        self.w = None
        
    def on_about_activate(self, *args):
        # Help->About menu entry
        self.aboutbox.show()
        
    def on_ipython_activate(self, *args):
        # Cf. http://ipython.scipy.org/moin/Cookbook/EmbeddingInGTK
        W = gtk.Window()
        W.set_size_request(750,550)
        W.set_resizable(True)
        S = gtk.ScrolledWindow()
        S.set_policy(gtk.POLICY_AUTOMATIC,gtk.POLICY_AUTOMATIC)
        V = IPythonView()
        V.set_wrap_mode(gtk.WRAP_CHAR)
        V.show()
        S.add(V)
        S.show()
        W.add(S)
        W.show()
        W.connect('delete_event',lambda x,y:False)
        #W.connect('destroy',lambda x:gtk.main_quit())
        V.updateNamespace({'mainwin': self})
        V.updateNamespace({'map': self.multiplexer.map})

    def on_sdl2gtk_activate(self, *args):
        mainwin = self
        def on_expose_event(self, event):
            mainwin.multiplexer.engine.test_RGB(self.window)
            #gc = self.window.new_gc()
            #self.window.draw_rectangle(gc, True, 10, 10, 200, 200)
        if (self.w is None):
            self.w = gtk.Window()
            self.w.set_double_buffered(False)
            self.w.set_app_paintable(True)
        self.w.connect("expose-event", on_expose_event)
        self.w.show()
