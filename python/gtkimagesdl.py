#!/usr/bin/env python

# Access a SDL_Surface that can be displayed as a GdkImage

# Copyright (C) 2008  Sylvain Beucler

# This file is part of GNU FreeDink

# GNU FreeDink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# GNU FreeDink is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import sys, os

import gobject
import gtk
import windinkedit  # gdkimage2sdl

class GtkImageSDL(gtk.DrawingArea):
    def __init__(self, width, height):
        # width, height, bitsperpixel, sdl_flags: parameters for
        # 'SDL_SetVideoMode'. width/height are also the default and
        # minimum size for the widget
        gtk.Widget.__init__(self)
        
        # Set the widget size - unfortunately this becomes the minimal
        # size as well. Gtk+ apparently doesn't have a "preferred" or
        # "natural" size concept, only a "minimum size". Apparently
        # the only work-around is to change the top-level window size
        # with gtk_window_set_default_size () or gtk_window_resize()
        self.set_size_request(width, height)
        self.set_double_buffered(False)
        self.connect("realize", GtkImageSDL.on_realize)
        self.connect("configure-event", GtkImageSDL.on_configure_event)
        self.create_image()

    def create_image(self):
        # TODO: fix memory leak (C SDL_Surface)
        self.image = gtk.gdk.Image(gtk.gdk.IMAGE_FASTEST, gtk.gdk.visual_get_system(),
                                   self.allocation.width, self.allocation.height)
        self.sdl_surface = windinkedit.gdkimage2sdl(self.image)
        self.surface = windinkedit.sdl2pygame(self.sdl_surface)

    def on_realize(self):
        # The default white background is startling, let's use a safer
        # default
        self.window.set_background(self.get_style().black)
        
    def on_configure_event(self, event):
        self.create_image()

    def do_expose_event(self, event):
        gc = self.get_style().black_gc  # or any valid GC
        self.window.draw_image(gc, self.image, event.area.x, event.area.y,
                               event.area.x, event.area.y,
                               event.area.width, event.area.height)
        # If we don't sync, it seems that the X server can use the
        # content of the image _after_ the call to draw_image - most
        # probably when the application is busy drawing the next
        # frame. This happens a lot when the application is producing
        # text output in a slow terminal such as gnome-terminal.
        display = self.get_display()
        display.sync()

gobject.type_register(GtkImageSDL)



if __name__ == '__main__':
    import pygame
    import random
    import time

    # Sample SDL app that changes the background color every 5
    # seconds.

    # The SDL surface is also reconstructed every 5 seconds only -
    # it's not redrawn each time the 'init' is called. In between,
    # screen is refreshed using the SDL backbuffer, instead of
    # redrawing everything. This supports applications that redraw
    # screen as needed instead of doing so in a loop.
    
    # The screen is also reconstructed when the widget is
    # resized. This is mandatory to take the new size into
    # account, and because the backbuffer is recreated.
    
    def regen_screen():
        global color
        # Beware that the surface changes everytime the widget is
        # resized!
        screen = w.surface
        screen.fill(color)
        # Check that colors aren't swapped or anything
        pygame.draw.rect(screen, (255, 0, 0), (10, 10, 20, 20)) # R
        pygame.draw.rect(screen, (0, 255, 0), (30, 10, 20, 20)) # G
        pygame.draw.rect(screen, (0, 0, 255), (50, 10, 20, 20)) # B

    # Start the game loop
    def game_loop():
        global color
        global last_update
        if (pygame.time.get_ticks() - last_update) > 5000:
            last_update = pygame.time.get_ticks()
            # change color every 5 seconds
            r = random.randrange(255)
            g = random.randrange(255)
            b = random.randrange(255)
            color = (r, g, b)
            regen_screen()
            w.queue_draw()
        #return False # only once
        time.sleep(.1)  # avoid 100% CPU utilization
        return True     # do it again asap

    # Show the widget before to initialize the game (otherwise the
    # surface won't be ready)
    w = GtkImageSDL(640, 480)

    # Initialize game
    pygame.init()
    last_update = -5000
    color = None
    game_loop()

    # Don't run this before the game is initialized
    def on_configure_event(self, allocation):
        """Rebuild the screen content on resize"""
        regen_screen()
    w.connect('configure_event', on_configure_event)

    # Attach game loop to Gtk
    gobject.idle_add(game_loop)
    win = gtk.Window()
    win.set_title('GtkImageSDL test')
    win.set_app_paintable(True)
    win.connect('delete-event', gtk.main_quit)
    win.add(w)
    win.show_all()
    gtk.main()


#if __name__ == '__main__':
if False:
    # gtk.Image already exists but:
    # - doesn't handle resize events
    # - needs to be embedded in an EventBox
    import gtk
    win = gtk.Window()
    image = gtk.gdk.Image(gtk.gdk.IMAGE_FASTEST, gtk.gdk.visual_get_system(), 640, 480)
    wimage = gtk.Image()
    wimage.set_from_image(image, None)
    win.add(wimage)

    sdl_surface = windinkedit.gdkimage2sdl(image)
    surf = windinkedit.sdl2pygame(sdl_surface)
    surf.fill((50,50,200), (10, 10, 100, 100))
    win.connect('delete-event', gtk.main_quit)
    win.show_all()
    # Test if updates to the Image are taken into account:
    surf.fill((200,50,50), (40, 40, 10, 10))
    gtk.main()
