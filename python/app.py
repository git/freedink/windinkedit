#!/usr/bin/env python

# WinDinkedit startup

# Copyright (C) 2008  Sylvain Beucler

# This file is part of GNU FreeDink

# GNU FreeDink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# GNU FreeDink is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.


import os

import gtk
import gnome
import gnome.ui

import pygame
import random
import time
import gobject

import mainwin

# TODO: move to __init__.py ?
# TODO: or move to another library to make things clearer
import windinkedit._pywindinkedit
from windinkedit._pywindinkedit import *

# When we autoconfiscate later, rename this file to .in and add it to
# configure.ac's AC_CONFIG_FILES
#version = '@VERSION@'
#appdatadir = '@APPDATADIR@'
ac_PACKAGE = 'windinkedit'
ac_PACKAGE_VERSION='1.4.1'

# Relocatable program tips
PKGDATADIR=None

def main(prefix):
    global PKGDATADIR
    PKGDATADIR=os.path.join(prefix, 'share', ac_PACKAGE)
    gnome_program = gnome.init('windinkedit', ac_PACKAGE_VERSION)
    inst = mainwin.MainWin()
    inst.widget.show()
    gtk.main()

if __name__ == "__main__":
    main("/usr")
