- Gnome + Python + libglade quickstart code:

 - libglade (s/autoconnect/signal_autoconnect/): http://www.jamesh.id.au/software/libglade/

 - GnomeApp tutorial: http://laguna.fmedic.unam.mx/~daniel/pygtutorial/pygtutorial/

 - Official PyGtk tutorial, which is actually a python-aware version
   of the Gtk+ tutorial:
   http://www.moeraki.com/pygtktutorial/pygtk2tutorial/

 - Drawing with GTK tutorial: tips about hinted mouse motion events:
   http://www.gtk.org/~otaylor/gtk/tutorial/drawing_tut-1.html

- Differences between GtkBuilder and libglade:

 - libglade is older and apparently less maintained

 - gtkbuilder is integrated in Gtk+ 2.12 (2007)

 - gtkbuilder uses a different file format than libglade, but there's
   a converter called 'gt-builder-convert'

 - gtkbuilder has a few regressions over libglade that needs fixing:
   http://library.gnome.org/devel/gtk/2.12/gtk-migrating-GtkBuilder.html
   Not everybody is concerned by those features though.

 - glade support for the gtkbuilder format is planned but not ready
   yet

 - So gtkbuilder is likely to takeover libglade in the future but is
   still recent and cutting edge
