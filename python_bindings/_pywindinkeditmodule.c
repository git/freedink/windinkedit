#include <pygobject.h>
 
void _pywindinkedit_register_classes (PyObject *d); 
extern PyMethodDef _pywindinkedit_functions[];
 
DL_EXPORT(void)
init_pywindinkedit(void)
{
    PyObject *m, *d;
 
    init_pygobject ();
 
    m = Py_InitModule ("_pywindinkedit", _pywindinkedit_functions);
    d = PyModule_GetDict (m);
 
    _pywindinkedit_register_classes (d);
 
    if (PyErr_Occurred ()) {
        Py_FatalError ("can't initialise module _pywindinkedit");
    }
}
