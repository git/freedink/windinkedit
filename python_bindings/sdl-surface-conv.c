#include <glib-object.h>
#include "SDL.h"

/**
 * Register pointer type (you'll use the WINDINKEDIT_TYPE_SDL_SURFACE
 * macro to call this function)
 */
GType
windinkedit_sdl_psurface_get_type (void)
{
  static GType our_type = 0;
  
  if (our_type == 0)
    our_type = g_pointer_type_register_static ("pSDL_Surface");

  return our_type;
}

/**
 * Register boxed type
 */
static SDL_Rect* sdl_rect_copy (const SDL_Rect *rect)
{
  SDL_Rect* result = g_new(SDL_Rect, 1);
  *result = *rect;
  return result;
}

GType
windinkedit_sdl_rect_get_type (void)
{
  static GType our_type = 0;
  
  if (our_type == 0)
    our_type = g_boxed_type_register_static(g_intern_static_string ("SDL_Rect"),
					    (GBoxedCopyFunc)sdl_rect_copy,
					    (GBoxedFreeFunc)g_free);

  return our_type;
}
