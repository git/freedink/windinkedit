#ifndef _WINDINKEDIT_SDL_SURFACE_CONV_H
#define _WINDINKEDIT_SDL_SURFACE_CONV_H

#define WINDINKEDIT_TYPE_SDL_SURFACE (windinkedit_sdl_psurface_get_type())
#define WINDINKEDIT_TYPE_SDL_RECT (windinkedit_sdl_rect_get_type())

#endif
